﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Security;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace ProtocolTester
{
    [Serializable]
    public class Carel
    {

        [NonSerialized]
        private SerialPort port;

        public SerialPort SerialPort { get { return port; } set { port = value; } }

        private static List<object[]> writeNewVal = new List<object[]>();

        private int selectAdress;
        private string carelStatus;
        private int selectedIndexes = 9999;

      //  public event EventHandler<ExtendedEventArgs> UpdateStatisticsEvent;
     //   public event EventHandler<EventArgs> UpdateLogEvent;
     //   public event EventHandler<ExtendedEventArgs> AddGoodUnitEvent;
        // public event EventHandler<EventArgs> UpdateFunctionScanListViewEvent;
    //    public event EventHandler<ExtendedEventArgs> UpdateStatisticListViewEvent;
            //public event EventHandler<EventArgs> CreateBmcEntry;

        public static List<CarelUnit> carelUnits = new List<CarelUnit>();

        public int SelectedIndexes
        {
            set { selectedIndexes = value; }
        }

        public string CarelStatus
        {
            get { return carelStatus; }
        }

        private int requestResponseTimeout = 200;

        public int SelectAdress
        {
            get { return selectAdress; }
            set { selectAdress = value; }
        }


        public bool GetValuesIndicator { get; set; }

        public int RequestResponseTimeout
        {
            get { return requestResponseTimeout; }
            set { requestResponseTimeout = value; }
        }

        public Carel(SerialPort _serialPort)
        {
            SerialPort = _serialPort;
        }

        public static void GetAllCarelValues(CarelUnit unit, int requestResponseTimeout, Action RefreshListView, Action<Device> UpdateStatistic)
        {
            try
            {
                if (!unit.SerialPort.IsOpen)
                    unit.SerialPort.Open();
            }
            catch
            {
                unit.СomPortError++;
                unit.Timer.Stop();
                return;
            }

            byte[] response = new byte[] { };

           
            byte[] message = new byte[] { 85 };

            message = Carel.GetCRC(new byte[] { 2, (byte)unit.Address, 63, 3 });

            CheckRequestResult(unit, SendRequest(unit, message, "GetAllCarelValues.1"));
            UpdateStatistic(unit);

            Thread.Sleep(100);

            if (!GetResponse(unit, ref response, requestResponseTimeout))
            {
                unit.ReadError++;
            }
            else
                if (response.Length > 0) unit.GoodAnswer++;

            if (response.Length > 0)
            {
                if (response[0] == 7)
                {
                    CheckRequestResult(unit, SendRequest(unit, message, "GetAllCarelValues.2"));

                    Thread.Sleep(100);

                    if (!GetResponse(unit, ref response, requestResponseTimeout))
                    {
                        unit.ReadError++;
                    }
                    else
                        unit.GoodAnswer++;

                    UpdateStatistic(unit);
                }
            }

            Thread.Sleep(100);
            message = Carel.GetCRC(new byte[] { 2, (byte)unit.Address, 70, 49, 3 });

            CheckRequestResult(unit, SendRequest(unit, message, "GetAllCarelValues.3"));

            Thread.Sleep(100);
            if (!GetResponse(unit, ref response, requestResponseTimeout))
            {
                unit.ReadError++;
            }
            else
            {
                if (response.Length > 0) 
                unit.GoodAnswer++;
            }
            UpdateStatistic(unit);

            List<byte[]> arrayList = new List<byte[]>();
            byte[] tempArray;

            int count = 0;
            while (true)
            {
                if (count > 20)
                {
                    
                    break;
                }

                message = new byte[] { 5, (byte)unit.Address };

                CheckRequestResult(unit, SendRequest(unit, message, "GetAllCarelValues.4"));
                

                Thread.Sleep(100);
                if (!GetResponse(unit, ref response, requestResponseTimeout))
                {
                    unit.ReadError++;
                }
                else
                {
                    if (response.Length > 0) 
                    unit.GoodAnswer++;
                }

                UpdateStatistic(unit);

                if (response.Length > 0)
                {
                    List<byte> respList = new List<byte>();

                    int allBytesCount = response.Length;
                    bool flag = false;


                    while (true)
                    {
                        if (response.Length > 3)//если в ответе больше 3-х байт
                        {
                            bool tempFlag = true;
                            for (int i = 0; i < response.Length; i++)
                            {
                                if (response[i] != (byte)3)
                                    respList.Add(response[i]);
                                else
                                {
                                    if (i + 2 < response.Length)
                                    {
                                        respList.Add(response[i]);
                                        respList.Add(response[i + 1]);
                                        respList.Add(response[i + 2]);
                                    }


                                    response = new byte[] { 1 };
                                    tempFlag = false;
                                    break;
                                }
                            }

                            if (tempFlag)
                            {
                                Thread.Sleep(100);
                                if (!GetResponse(unit, ref response, requestResponseTimeout))
                                {
                                    unit.ReadError++;
                                }
                                else
                                {
                                    if (response.Length > 0) 
                                    unit.GoodAnswer++;
                                }
                            }
                        }
                        else
                        {
                            if (response.Length > 0)//если в ответе хотя бы один байт
                            {
                                if (response[0] == 0)//и он равен 0, то считаем что чтение массива завершилось, устанавливаем флаг, чтобы выйти из внешнего цикла
                                {
                                    flag = true;
                                    unit.EmptyAnswer++;
                                    break;
                                }
                                else
                                {
                                    if (response[0] != 1)
                                        unit.ReadError++;
                                    break;
                                }
                            }
                            else//если массив пустой, выходим и шлем запрос на получение данных.
                            {
                                break;
                            }
                        }

                    }


                    if (flag)
                    {
                        break;
                    }

                    tempArray = new byte[respList.Count]; //переопределяем временный массив 

                    for (int i = 0; i < respList.Count; i++)//записываем в него все что получили от устройства
                    {
                        tempArray[i] = respList.ElementAt(i);
                    }

                    if (CheckCRC(tempArray))//проверяем CRC
                    {
                        arrayList.Add(tempArray);//если все нормально добавляем массив в список результат
                        message = new byte[] { 6 }; //шлем подтверждение успешного завершения операции

                        CheckRequestResult(unit, SendRequest(unit, message, "GetAllCarelValues.5"));

                        //            if (UpdateStatisticListViewEvent != null)
                        //                UpdateStatisticListViewEvent(new RequestResult(true, sw.Elapsed), null);
                    }
                    else
                    {
                        unit.CrcError++;
                        message = new byte[] { 7 };//иначе, если контрольная сумма не правильная, шлем ошибку и ждем повторной порции данных
                        CheckRequestResult(unit, SendRequest(unit, message, "GetAllCarelValues.6"));
                        count++;
                    }
                }
                else
                {
                    count++;
                    unit.NotAnswer++;
                }

               // Thread.Sleep(requestResponseTimeout);
            }

//            if (unit._SerialPort.IsOpen)
//                unit._SerialPort.Close();

            //Делим полученные данные на 3 соответствующих списка
            List<byte[]> realList = arrayList.Where((x) => x[2] == 83).ToList();
            List<byte[]> intList = arrayList.Where((x) => x[2] == 85).ToList();
            List<byte[]> boolList = arrayList.Where((x) => x[2] == 66).ToList();

            Task CarelTask = Task.Factory.StartNew(() =>
            {
                RecogniseAnswer(unit, realList);
                RecogniseAnswer(unit, intList);
                RecogniseAnswer(unit, boolList);

                RefreshListView();



            });

            

        }

        private static void CheckRequestResult(CarelUnit unit, string requestResult)
        {
            if (requestResult != "")
            {
                if (requestResult.Length > 4)
                {
                    switch (requestResult.Substring(0, 5))
                    {
                        case "Error":
                            unit.WriteError++;
                            break;
                        case "Seria":
                            unit.СomPortError++;
                            break;
                    }
                }
            }
        }

        public static Device GetNewCarelValues(CarelUnit unit, int requestResponseTimeout, List<Device> devices, BMCSettings _bMCSettings)
        {
            byte[] response = new byte[] { };
            byte[] message = new byte[] { 5, (byte)unit.Address };

            lock (writeNewVal)
            while (true)//отправка запроса на изменение значания переменной
            {
                
                if (writeNewVal.Count > 0)
                {
                        //MessageBox.Show((((Device)(writeNewVal[0][1])).Address - 48).ToString() + " " + (byte[])(writeNewVal[0][0]));
                        //    Thread.Sleep(200);
                        if (((CarelUnit)(writeNewVal[0][1])).SerialPort.PortName != unit.SerialPort.PortName)
                            break;

                    CheckRequestResult((CarelUnit)(writeNewVal[0][1]), SendRequest((CarelUnit)(writeNewVal[0][1]), (byte[])(writeNewVal[0][0]), "GetNewCarelValues.2"));

                    Thread.Sleep(requestResponseTimeout);

                    if (!GetResponse(unit, ref response, requestResponseTimeout))
                        unit.ReadError++;
              //      else
                    {
                        writeNewVal.RemoveAt(0);
                    }
                }
                else
                    break;
            }
            return GetNewValue(unit, requestResponseTimeout, devices, _bMCSettings);
        }

        private static Device GetNewValue(CarelUnit unit, int requestResponseTimeout, List<Device> devices, BMCSettings _bMCSettings)
        {
            while (true)
            {
                byte[] response = new byte[] { };
                byte[] message = new byte[] { 5, (byte)unit.Address };

                int cicleCount = 0;

                while (true)
                {
                    CheckRequestResult(unit, SendRequest(unit, message, "GetNewCarelValues.2"));

                    Thread.Sleep(requestResponseTimeout);

                    if (!GetResponse(unit, ref response, requestResponseTimeout))
                    {
                        unit.ReadError++;
                        return unit;
                    }

                    if (response.Length > 0)
                        break;
                    else
                    {
                        if (cicleCount > 1)
                            break;
                        else
                            cicleCount++;
                    }
                }

               // response = new byte[] { 0xE0, 0x80, 0x23, 0x05, 0x80, 0xFA, 0xFF, 0xFF, 0x03, 0x38, 0x3B };

                if (response.Length > 0)
                {
                    if (response[0] == 7 & response.Length == 1)
                    {
                        unit.UnitAnswerError++;
                        break;
                        //return new RequestResult(false, unit, 5);//ошибка обработки контроллером
                    }
                    else
                    {
                        if (response[0] != 0)
                        {
                            if (response[0] != 2 & response[0] != 6 & response[0] != 7)
                                unit.IncorrectAnswer++;

                            if (RecogniseAnswerValue(unit, response, devices))
                            {
                                message = new byte[] { 6 }; //шлем подтверждение успешного завершения операции
                                SendRequest(unit, message, "GetNewCarelValues.3");

                                unit.GoodAnswer++;

                                if (unit.Quality == true)
                                {
                                    if (unit.NotAnswerCountForQuality > 6)
                                    {
                                        ErrorEventLog.LogError(": GetNewValue.1 - Связь с контроллером, адрес: " + (unit.Address - 48).ToString() + " восстановлена.", Model.GetProgrammDir() + "\\Logs\\Carel.txt");
                                    }
                                    unit.NotAnswerCountForQuality = 0;
                                }
                            }
                            else
                            {
                                unit.CrcError++;
                                message = new byte[] { 7 }; //шлем подтверждение неуспешного завершения операции
                                SendRequest(unit, message, "GetNewCarelValues.4");
                                return unit;
                            }
                        }
                        else
                        {
                            unit.EmptyAnswer++;
                            if (unit.Quality == true)
                            {
                                if (unit.NotAnswerCountForQuality > 6)
                                {
                                    ErrorEventLog.LogError(": GetNewValue.1 - Связь с контроллером, адрес: " + (unit.Address - 48).ToString() + " восстановлена.", Model.GetProgrammDir() + "\\Logs\\Carel.txt");
                                }
                                unit.NotAnswerCountForQuality = 0;
                            }
                            break;
                        }
                    }
                }
                else
                {
                    unit.NotAnswer++;

                    if (unit.NotAnswerCountForQuality > 6)
                        if (unit.Quality)
                        {
                            unit.Quality = false;
                           // unit.NotAnswerCountForQuality = 0;

                            ErrorEventLog.LogError(": GetNewValue.2 - Связь с контроллером, адрес: " + (unit.Address - 48).ToString() + " потеряна.", Model.GetProgrammDir() + "\\Logs\\Carel.txt");

                            foreach (var v in unit.DataItems)
                            {
                                v.NeedSend = true;
                            }

                            if (_bMCSettings != null)
                                CreateEntry(unit, _bMCSettings);
                        }

                    if (unit.NotAnswerCountForQuality < 7)
                        unit.NotAnswerCountForQuality++;

                    //if (unit.NotAnswerCountForQuality > 100000)
                    //    unit.NotAnswerCountForQuality = 6;
                    break;
                    //return new RequestResult(false, unit, 6);
                }

                //unit.EmptyAnswer++;
                //break;
            }
            return unit;
        }

        private static void CreateEntry(Device unit, BMCSettings _bMCSettings)
        {
            if (_bMCSettings.FunctionActivated)//Если в настройках функция активирована, то создаем инцидент
            {
                if (((Device)unit).bMCRemedy == null)
                {
                    Task carelTask = Task.Factory.StartNew(() =>
                    {
                        unit.bMCRemedy = new BMCRemedy(_bMCSettings.BMCServer, _bMCSettings);
                    //_bMCSettings.Description = "Нет связи с контроллером, адресс " + (unit is CarelUnit ? unit.Address - 48 : unit.Address).ToString();

                    Thread.Sleep(_bMCSettings.Timeout * 60000);

                        if (unit.NotAnswerCountForQuality < 7)
                            return;
                        else
                        {
                            unit.bMCRemedy._BMCServer = unit.bMCRemedy._BMCServer ?? new BMC.ARSystem.Server();

                            lock (_bMCSettings)
                            {
                                unit.bMCRemedy.CreateIncidentBMC(unit.bMCRemedy, unit);
                            }
                        }
                    });
                }
            }
        }

        private static void RecogniseAnswer(CarelUnit unit, List<byte[]> response)
        {
            byte lastNumber = 0;
            int arrayIndex = 0;

            string dtNow = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss.fff");

            foreach (byte[] byteArray in response)
            {
                if (byteArray[2] != (byte)66)//Если у нас не булы
                {
                    if (byteArray[2] == (byte)83)//Если у нас риалы
                    {
                        if (lastNumber != byteArray[3])
                        {
                            int i = 1;

                            while (true)
                            {
                                i = i + 4;

                                if (i + 4 > byteArray.Length)
                                    break;

                                int v1, v2, v3, v4;

                                v1 = byteArray[i] > 64 ? byteArray[i] - 49 : byteArray[i] - 48;
                                v2 = byteArray[i + 1] > 64 ? byteArray[i + 1] - 49 : byteArray[i + 1] - 48;
                                v3 = byteArray[i + 2] > 64 ? byteArray[i + 2] - 49 : byteArray[i + 2] - 48;
                                v4 = byteArray[i + 3] > 64 ? byteArray[i + 3] - 49 : byteArray[i + 3] - 48;

                                if (v1 != 0 | v2 != 0 | v3 != 0 | v4 != 0)
                                {

                                }

                                string hex1 = v1 >= 0 ? int.Parse(v1.ToString("X")).ToString("X") : "0";
                                string hex2 = v2 >= 0 ? int.Parse(v2.ToString("X")).ToString("X") : "0";
                                string hex3 = v3 >= 0 ? int.Parse(v3.ToString("X")).ToString("X") : "0";
                                string hex4 = v4 >= 0 ? int.Parse(v4.ToString("X")).ToString("X") : "0";

                                int tempVal = int.Parse((hex1 + hex2 + hex3 + hex4), System.Globalization.NumberStyles.AllowHexSpecifier);

                                double d = tempVal < 32768 ? (double)tempVal / 10 : ((double)tempVal - 65536) / 10;

                                unit.DataItems[arrayIndex].Value = d.ToString();
                                unit.DataItems[arrayIndex].TimeStamp = dtNow;
                                unit.DataItems[arrayIndex].Com = unit.SerialPort.PortName;
                                //unit.DataItems[arrayIndex].Address = unit.Address - 48;
                                unit.DataItems[arrayIndex].Variable = arrayIndex + 1;
                                unit.DataItems[arrayIndex].DataType_ = DataItem.DataType.REAL;

                                if (!unit.Quality)
                                {
                                    unit.Quality = true;
                                    for (int j = 0; j < unit.DataItems.Count; j++)
                                        unit.DataItems[j].NeedSend = true;
                                }

                                arrayIndex++;
                            }

                            lastNumber = byteArray[3];
                        }
                    }

                    else //у нас инты
                    {
                        if (lastNumber != byteArray[3])
                        {
                            int i = 1;//адресация начинается с нуля, 
                            arrayIndex = arrayIndex == 0 ? 207 : arrayIndex;
                            while (true)
                            {
                                i = i + 4;

                                if (i + 4 > byteArray.Length)
                                    break;

                                int v1, v2, v3, v4;

                                v1 = byteArray[i] > 64 ? byteArray[i] - 49 : byteArray[i] - 48;
                                v2 = byteArray[i + 1] > 64 ? byteArray[i + 1] - 49 : byteArray[i + 1] - 48;
                                v3 = byteArray[i + 2] > 64 ? byteArray[i + 2] - 49 : byteArray[i + 2] - 48;
                                v4 = byteArray[i + 3] > 64 ? byteArray[i + 3] - 49 : byteArray[i + 3] - 48;


                                string hex1 = int.Parse(v1.ToString("X")).ToString("X");
                                string hex2 = int.Parse(v2.ToString("X")).ToString("X");
                                string hex3 = int.Parse(v3.ToString("X")).ToString("X");
                                string hex4 = int.Parse(v4.ToString("X")).ToString("X");

                                int tempVal = int.Parse((hex1 + hex2 + hex3 + hex4), System.Globalization.NumberStyles.AllowHexSpecifier);

                                int d = tempVal < 32768 ? tempVal : (tempVal - 65536);


                                unit.DataItems[arrayIndex].Value = d.ToString();
                                unit.DataItems[arrayIndex].TimeStamp = dtNow;
                                unit.DataItems[arrayIndex].Com = unit.SerialPort.PortName;
                                //unit.DataItems[arrayIndex].Address = unit.Address - 48;
                                unit.DataItems[arrayIndex].Variable = arrayIndex - 206;
                                unit.DataItems[arrayIndex].DataType_ = DataItem.DataType.INT;

                                if (!unit.Quality)
                                {
                                    unit.Quality = true;
                                    for (int j = 0; j < unit.DataItems.Count; j++)
                                        unit.DataItems[j].NeedSend = true;
                                }

                                arrayIndex++;
                            }

                            lastNumber = byteArray[3];
                        }
                    }
                }
                else//у нас булы
                {
                    int i = 1;//адресация начинается с нуля, 

                    arrayIndex = 413;

                    while (true)
                    {
                        i = i + 4;

                        if (i > byteArray.Length - 4)
                            break;

                        int v1, v2, v3, v4;

                        //делаем преобразование согласно протоколу Carel
                        v1 = byteArray[i] > 64 ? byteArray[i] - 49 : byteArray[i] - 48;


                        v2 = byteArray[i + 1] > 64 ? byteArray[i + 1] - 49 : byteArray[i + 1] - 48;
                        v3 = byteArray[i + 2] > 64 ? byteArray[i + 2] - 49 : byteArray[i + 2] - 48;
                        v4 = byteArray[i + 3] > 64 ? byteArray[i + 3] - 49 : byteArray[i + 3] - 48;


                        byte hex1 = byte.Parse(v1 < 0 ? "0" : v1.ToString("X"));//получаем шестнадцатеричное представление 
                        byte hex2 = byte.Parse(v2 < 0 ? "0" : v2.ToString("X"));
                        byte hex3 = byte.Parse(v3 < 0 ? "0" : v3.ToString("X"));
                        byte hex4 = byte.Parse(v4 < 0 ? "0" : v4.ToString("X"));

                        for (int j = 1; j < (arrayIndex == 605 ? 16 : 17); j++)
                        {
                           // unit.DataItems[j + arrayIndex].Value = d.ToString();
                            unit.DataItems[j + arrayIndex].TimeStamp = dtNow;
                            unit.DataItems[j + arrayIndex].Com = unit.SerialPort.PortName;
                            //unit.DataItems[arrayIndex].Address = unit.Address - 48;
                            unit.DataItems[j + arrayIndex].Variable = j + arrayIndex - 413;
                            unit.DataItems[j + arrayIndex].DataType_ = DataItem.DataType.BOOL;

                            if (!unit.Quality)
                            {
                                unit.Quality = true;
                                for (int c = 0; c < unit.DataItems.Count; c++)
                                    unit.DataItems[c].NeedSend = true;
                            }
                        }


                        unit.DataItems[1 + arrayIndex].Value = ModBusRTU.GetBit(hex2, 0) == 1 ? true.ToString() : false.ToString();
                        unit.DataItems[2 + arrayIndex].Value = ModBusRTU.GetBit(hex2, 1) == 1 ? true.ToString() : false.ToString();
                        unit.DataItems[3 + arrayIndex].Value = ModBusRTU.GetBit(hex2, 2) == 1 ? true.ToString() : false.ToString();
                        unit.DataItems[4 + arrayIndex].Value = ModBusRTU.GetBit(hex2, 3) == 1 ? true.ToString() : false.ToString();

                        unit.DataItems[5 + arrayIndex].Value = ModBusRTU.GetBit(hex1, 0) == 1 ? true.ToString() : false.ToString();
                        unit.DataItems[6 + arrayIndex].Value = ModBusRTU.GetBit(hex1, 1) == 1 ? true.ToString() : false.ToString();
                        unit.DataItems[7 + arrayIndex].Value = ModBusRTU.GetBit(hex1, 2) == 1 ? true.ToString() : false.ToString();
                        unit.DataItems[8 + arrayIndex].Value = ModBusRTU.GetBit(hex1, 3) == 1 ? true.ToString() : false.ToString();

                        unit.DataItems[9 + arrayIndex].Value = ModBusRTU.GetBit(hex4, 0) == 1 ? true.ToString() : false.ToString();
                        unit.DataItems[10 + arrayIndex].Value = ModBusRTU.GetBit(hex4, 1) == 1 ? true.ToString() : false.ToString();
                        unit.DataItems[11 + arrayIndex].Value = ModBusRTU.GetBit(hex4, 2) == 1 ? true.ToString() : false.ToString();
                        unit.DataItems[12 + arrayIndex].Value = ModBusRTU.GetBit(hex4, 3) == 1 ? true.ToString() : false.ToString();

                        unit.DataItems[13 + arrayIndex].Value = ModBusRTU.GetBit(hex3, 0) == 1 ? true.ToString() : false.ToString();
                        unit.DataItems[14 + arrayIndex].Value = ModBusRTU.GetBit(hex3, 1) == 1 ? true.ToString() : false.ToString();
                        unit.DataItems[15 + arrayIndex].Value = ModBusRTU.GetBit(hex3, 2) == 1 ? true.ToString() : false.ToString();

                        if (arrayIndex < 605)
                            unit.DataItems[16 + arrayIndex].Value = ModBusRTU.GetBit(hex3, 3) == 1 ? true.ToString() : false.ToString();




                        arrayIndex = arrayIndex + 16;
                    }

                    lastNumber = byteArray[3];

                }

            }

        }


        public static bool RecogniseAnswerValue(CarelUnit unit, byte[] response, List<Device> devices)
        {
           // response = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x38, 0x3A };

            List<byte[]> resp = new List<byte[]>();
            if (response.Length == 11 | response.Length == 8)
            {
                resp.Add(response);
            }
            else
            {
                int startArr = 0, endArr = 0;
                for (int i = 0; i < response.Length; i++)//Если в ответе пришло несколько изменившихся переменных в цикле происходит их разбор
                {
                    if (response[i] == 2)
                        startArr = i;
                    else
                        if (response[i] == 3)
                        {
                            endArr = i;

                            if (response.Length < i + 3)
                            {
                                break;
                            }
                            else
                            {
                                byte[] tempArray = new byte[endArr - startArr + 3];
                                Array.Copy(response, startArr, tempArray, 0, endArr - startArr + 3);
                                resp.Add(tempArray);
                            }
                        }
                }
            }

            for (int i = 0; i < resp.Count; i++)
            {
                unit = null;
                foreach (Device d in devices)
                    if (d.Address == resp[i][1])
                        unit = (CarelUnit)d;
                response = resp[i];

                if (unit != null)
                {
                    if (CheckCRC(response))
                    {
                        string dtNow = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss.fff");
                        int unitAddress = response[1];
                        int dataType = response[2];
                        int varAdress = response[3] - 48;

                        if (response.Length == 11)
                        {


                            int v1, v2, v3, v4;

                            v1 = response[4] > 64 ? response[4] - 49 : response[4] - 48;
                            v2 = response[5] > 64 ? response[5] - 49 : response[5] - 48;
                            v3 = response[6] > 64 ? response[6] - 49 : response[6] - 48;
                            v4 = response[7] > 64 ? response[7] - 49 : response[7] - 48;


                            string hex1 = int.Parse(v1.ToString("X")).ToString("X");
                            string hex2 = int.Parse(v2.ToString("X")).ToString("X");
                            string hex3 = int.Parse(v3.ToString("X")).ToString("X");
                            string hex4 = int.Parse(v4.ToString("X")).ToString("X");

                            int tempVal = int.Parse((hex1 + hex2 + hex3 + hex4), System.Globalization.NumberStyles.AllowHexSpecifier);

                            string value = "0";

                            if (dataType == 65)
                            {
                                double d = tempVal < 32768 ? (double)tempVal / 10 : ((double)tempVal - 65536) / 10;
                                value = d.ToString();
                                varAdress = response[3] - 49;
                            }
                            else
                                if (dataType == 73)
                            {
                                int d = tempVal < 32768 ? tempVal : (tempVal - 65536);
                                value = d.ToString();
                                varAdress = response[3] - 49 + 207;
                            }


                            if (unit.DataItems[varAdress].OldValue != unit.DataItems[varAdress].Value)
                            {
                                unit.DataItems[varAdress].OldValue = unit.DataItems[varAdress].Value;
                                unit.DataItems[varAdress].OldTimeStamp = unit.DataItems[varAdress].TimeStamp;
                                // UpdateListView();
                            }

                            unit.DataItems[varAdress].NeedSend = true;
                            unit.DataItems[varAdress].Value = value;
                            unit.DataItems[varAdress].TimeStamp = dtNow;

                            if (!unit.Quality)
                            {
                                unit.Quality = true;
                                for (int j = 0; j < unit.DataItems.Count; j++)
                                    unit.DataItems[j].NeedSend = true;
                            }



                        }
                        else
                            if (response.Length == 8)
                        {
                            varAdress = response[3] - 49 + 414;
                            int v1;

                            v1 = response[4] > 64 ? response[4] - 49 : response[4] - 48;

                            byte hex1 = byte.Parse(v1.ToString("X"));

                            if (unit.DataItems[varAdress].OldValue != unit.DataItems[varAdress].Value)
                            {
                                unit.DataItems[varAdress].OldValue = unit.DataItems[varAdress].Value;
                                unit.DataItems[varAdress].OldTimeStamp = unit.DataItems[varAdress].TimeStamp;
                                // UpdateListView();
                            }

                            unit.DataItems[varAdress].Value = hex1 == 1 ? true.ToString() : false.ToString();
                            unit.DataItems[varAdress].TimeStamp = dtNow;
                            unit.DataItems[varAdress].NeedSend = true;

                            if (!unit.Quality)
                            {
                                unit.Quality = true;
                                for (int j = 0; j < unit.DataItems.Count; j++)
                                    unit.DataItems[j].NeedSend = true;
                            }

                        }

                        //return true;
                    }
                    else
                        return false;//ошибка контрольной суммы
                }
                else
                    return false;//ошибка контрольной суммы
            }
            return true;
        }

        public static void WriteNewValue(short val, int unitAddress, DataItem.DataType dataType, int valAddress, Device dev)
        {
            
            byte[] value = dataType == DataItem.DataType.INT ? GetByteArrayFromIntValue(val) :
                dataType == DataItem.DataType.REAL ? GetByteArrayFromRealValue(val) : GetByteArrayFromBoolValue(val);

            byte[] message = dataType != DataItem.DataType.BOOL ? GetCRC(new byte[] { 2, (byte)unitAddress, (byte)(dataType == DataItem.DataType.INT ? 73 : dataType == DataItem.DataType.REAL ? 65 : 68), (byte)valAddress, value[0], value[1], value[2], value[3], 3 }) :
                GetCRC(new byte[] { 2, (byte)unitAddress, (byte)(dataType == DataItem.DataType.INT ? 73 : dataType == DataItem.DataType.REAL ? 65 : 68), (byte)valAddress, value[0], 3 });

            writeNewVal.Add(new object[] {message, dev});
        }

        public static byte[] GetByteArrayFromIntValue(short val)
        {
            byte[] res = new byte[4];



            string hexString = val.ToString("X");
            string[] tempArray = new string[4];

            int charCount = hexString.Length;

            if (charCount > 0)
            {
                for (int i = 0; i < charCount; i++)
                {
                    tempArray[4 - charCount + i] = hexString.Substring(i, 1);
                }
            }


            for (int i = 0; i < 4; i++)
            {
                int tVal = System.Int32.Parse(tempArray[i] == null ? "0" : tempArray[i], System.Globalization.NumberStyles.AllowHexSpecifier);
                res[i] = (byte)(tVal < 10 ? tVal + 48 : tVal + 55);
            }

            return res;
        }

        public static byte[] GetByteArrayFromRealValue(short val)
        {
            byte[] res = new byte[4];



            string hexString = val.ToString("X");
            string[] tempArray = new string[4];

            int charCount = hexString.Length;

            if (charCount > 0)
            {
                for (int i = 0; i < charCount; i++)
                {
                    tempArray[4 - charCount + i] = hexString.Substring(i, 1);
                }
            }


            for (int i = 0; i < 4; i++)
            {
                int tVal = System.Int32.Parse(tempArray[i] == null ? "0" : tempArray[i], System.Globalization.NumberStyles.AllowHexSpecifier);
                res[i] = (byte)(tVal < 10 ? tVal + 48 : tVal + 55);
            }

            return res;
        }

        public static byte[] GetByteArrayFromBoolValue(int val)
        {
            return new byte[] { val == 0 ? (byte)48 : (byte)49 };
        }

        private static bool CheckCRC(byte[] array)
        {
            try
            {
                if (array.Length == 0)
                    return false;

                int sum = 0;

                for (int i = 0; i < array.Length - 2; i++)
                {
                    sum = sum + array[i];
                    byte t = array[i];
                }

                int crcH = array[array.Length - 2];
                int crcL = array[array.Length - 1];

                string hex = sum.ToString("X");

                if (hex.Length > 1)
                {
                    string hexCrcH = "3" + hex.Substring(hex.Length - 2, 1);
                    string hexCrcL = "3" + hex.Substring(hex.Length - 1, 1);

                    var t1 = crcL.ToString("X");
                    var t2 = crcH.ToString("X");

                    if (hexCrcH == crcH.ToString("X") & hexCrcL == crcL.ToString("X"))
                        return true;
                }

              //  if (UpdateStatisticListViewEvent != null)
              //      UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), null);
                ErrorEventLog.LogError(": (CheckCRC.1) CRC ERROR", Model.GetProgrammDir() + "\\Logs\\Carel.txt");
            }
            catch (Exception ex)
            {
              //  if (UpdateStatisticListViewEvent != null)
              //      UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), null);
                ErrorEventLog.LogError(": (CheckCRC.2) CRC ERROR " + ex.Message, Model.GetProgrammDir() + "\\Logs\\Carel.txt");
            }
            return false;
        }

        /// <summary>
        /// Подготовка и отправка команды в порт
        /// </summary>
        /// <param name="sp">Порт</param>
        /// <param name="address">Адрес устройства в сети</param>
        /// <param name="allOrOnlyNewData">true - запрос всего массива, false - запрос измененных данных</param>
        /// <returns></returns>
        public string SendRequest(SerialPort sp, byte address, bool allOrOnlyNewData, object sender)
        {
            string res = "";

          //  Stopwatch sw = new Stopwatch();
            //Ensure port is open:
            if (sp.IsOpen)
            {
                byte[] message = new byte[0];

                //sw.Start();

                if (!allOrOnlyNewData)
                {
                    message = new byte[] { 5, address };
                    //message = GetCRC(new byte[] { 2, address, 70, 49, 3 });
                }

                try
                {
                    sp.Write(message, 0, message.Length);//отправка сообщения
                }
                catch (Exception err)
                {
             //       if (UpdateStatisticListViewEvent != null)
             //           UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), null);
                    return "Error in read event: " + err.Message;
                }
            }
            else
            {
             //   if (UpdateStatisticListViewEvent != null)
             //       UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), null);
                return "Serial port not open";
            }

            return res;
        }


        /// <summary>
        /// Функция отправки сообщения в порт
        /// </summary>
        /// <param name="unit">экземпляр класса описывающий свойства через которые осуществляется доступ к конкретному контроллеру и массивы с переменными выгружеными из контроллера</param>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        /// <returns></returns>
        public static string SendRequest(CarelUnit unit, byte[] message, string method)
        {
            SerialPort sp = unit.SerialPort;
        //    unit.AddLog("req - ", message);

            unit.AddLog("req - " + (unit.Address - 48).ToString() + " " + unit.SerialPort.PortName, message);

                if (sp.IsOpen)
                {
                    try
                    {
                        sp.Write(message, 0, message.Length);//отправка сообщения
                    }
                    catch (Exception err)
                    {
                        ErrorEventLog.LogError("(" + method + ") Error in read event: " + err.Message, Model.GetProgrammDir() + "\\Logs\\Carel.txt");
                        //if (UpdateStatisticListViewEvent != null)
                        //    UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), null);
        //                Model.LogList.Insert(0, "Error in read event: " + err.Message);
                        unit.AddLog("Error in read event: " + err.Message, message);
                        return "Error in read event: " + err.Message;
                    }
                }
                else
                {
                    ErrorEventLog.LogError("(" + method + ") Serial port not open ", Model.GetProgrammDir() + Model.GetProgrammDir() + "\\Logs\\Carel.txt");
                    // if (UpdateStatisticListViewEvent != null)
                    //     UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), null);
                    unit.AddLog("Serial port not open", message);
                    return "Serial port not open";
                }

            return "";
        }

 

        public static bool GetResponse(CarelUnit unit, ref byte[] response, int requestResponseTimeout)
        {
            try
            {
                SerialPort sp = unit.SerialPort;
                if (sp.BytesToRead == 0)
                {
                    Thread.Sleep(requestResponseTimeout);
                }
                if (sp.BytesToRead == 0)
                {
                    response = new byte[0];
                    return true;
                }

                response = new byte[sp.BytesToRead];

                sp.Read(response, 0, response.Length);

                unit.AddLog("resp - " + (unit.Address - 48).ToString() + " " + unit.SerialPort.PortName, response);

                return true;
            }
            catch (Exception ex)
            {
                unit.AddLog("Ошибка при получении ответа. " + ex.Message, response);
                return false;
            }
        }
        

        public static byte[] GetCRC(byte[] message)
        {
            byte[] res = new byte[message.Length + 2];

            int sum = 0;

            for (int i = 0; i < message.Length; i++)
            {
                res[i] = message[i];

                sum = sum + message[i];
            }

            string hexstr = String.Format("{0:X}", sum);

            if (hexstr.Length > 1)
            {
                string firstB, lastB;

                lastB = hexstr.Substring(hexstr.Length - 1, 1);
                firstB = hexstr.Substring(hexstr.Length - 2, 1);

                int lastBint, firstBint;

                lastBint = Convert.ToInt32(lastB, 16) + 48;
                firstBint = Convert.ToInt32(firstB, 16) + 48;

                res[res.Length - 2] = (byte)firstBint;
                res[res.Length - 1] = (byte)lastBint;
            }
            else
                return message;

            return res;
        }



    }


    

}
