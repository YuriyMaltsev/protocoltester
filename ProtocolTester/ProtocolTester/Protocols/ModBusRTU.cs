﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Threading;

namespace ProtocolTester
{
    [Serializable]
    public class ModBusRTU
    {
        private static List<object[]> writeNewVal = new List<object[]>();

        private string _portName = "";
        private List<int> _dataBits;
        private List<int> _speeds;
        private List<Parity> _paritys;
        private List<StopBits> _stopBits;
        private List<int> _address;

        [NonSerialized]
        private SerialPort comPort;
        private bool autoModeIndicator;

        private int selectAdress;
        private string modbusStatus;
        private int selectedIndexes = 9999;

        public event EventHandler<ExtendedEventArgs> UpdateStatisticsEvent;
        public event EventHandler<EventArgs> UpdateLogEvent;
        public event EventHandler<ExtendedEventArgs> AddGoodUnitEvent;
        public event EventHandler<EventArgs> UpdateFunctionScanListViewEvent;
        public event EventHandler<ExtendedEventArgs> UpdateStatisticListViewEvent;

        public int SelectedIndexes
        {
            set { selectedIndexes = value; }
        }

        public string ModbusStatus
        {
            get { return modbusStatus; }
        }

        public int requestResponseTimeout = 200;

        public int SelectAdress
        {
            get { return selectAdress; }
            set { selectAdress = value; }
        }

        public string PortName
        {
            get { return _portName; }
            set { _portName = value; }
        }

        public SerialPort ComPort
        {
            get { return comPort; }
        }

        public bool AutoModeIndicator
        {
            get { return autoModeIndicator; }
            set { autoModeIndicator = value; }
        }

        public bool GetValuesIndicator { get; set; }

        private int status = 0;

        public ModBusRTU(List<int> dataBits, List<int> speeds, List<Parity> paritys, List<StopBits> stopBits, List<int> address)
        {
            _dataBits = dataBits;
            _speeds = speeds;
            _paritys = paritys;
            _stopBits = stopBits;
            _address = address;

            //comPort = new SerialPort(_portName);
        }


        public ModBusRTU()
        {

        }
        

        public static void UpdateValues(ModBusRTUUnit device, int requestResponseTimeout)
        {
            List<ushort[]> arrayList = new List<ushort[]>();
            

            ushort[] arr = new ushort[7];

            //цикл подготовки данных для запросов
            for (int i = 0; i < device.DataItems.Count; i++)
            {
                if (arr[0] == 0)
                {
                    arr[0] = (ushort)device.DataItems[i].Group;//группа
                    arr[1] = (ushort)device.DataItems[i].Function;//функция
                    arr[2] = (ushort)device.DataItems[i].Variable;//начальный регистр
                    arr[3] = 1;//кол-во регистров
                    arr[4] = (ushort)i;//стартовый индекс группы в listView
                    arr[5] = (ushort)(i + 1);//конечный индекс группы в listView
                    arr[6] = (ushort)device.Address;//адрес устройства в сети

                    arrayList.Add(arr);
                }
                else
                {
                    if (arrayList[arrayList.Count - 1][0] == (ushort)device.DataItems[i].Group)
                    {
                        arrayList[arrayList.Count - 1][3]++;
                        arrayList[arrayList.Count - 1][5]++;
                    }
                    else
                    {
                        arr = new ushort[7];
                        arr[0] = (ushort)device.DataItems[i].Group;//группа
                        arr[1] = (ushort)device.DataItems[i].Function;//функция
                        arr[2] = (ushort)device.DataItems[i].Variable;//начальный регистр
                        arr[3] = 1;//кол-во регистров
                        arr[4] = (ushort)i;//стартовый индекс группы в listView
                        arr[5] = (ushort)(i + 1);//конечный индекс группы в listView
                        arr[6] = (ushort)device.Address;
                        arrayList.Add(arr);
                    }
                }
            }

            short[] values;
            int valuesIndex = 0;

            if (!device.SerialPort.IsOpen)
                device.SerialPort.Open();

            if (arrayList.Count == 0 & writeNewVal.Count > 0)
                WriteValue(device, requestResponseTimeout);
            //цикл запросов с обновлением данных
            foreach (ushort[] v in arrayList)
            {
                if (writeNewVal.Count > 0)
                    WriteValue(device, requestResponseTimeout);

                values = ModBusRTU.UpdateValue(device, Convert.ToByte(v[6]), v[1], v[2], v[3], requestResponseTimeout);

                if (values == null)
                {
                //    lastError = DateTime.Now.ToString("HH:mm:ss") + ": COM - " + _port.PortName + ", Address - " + v[6] + ", Group - " + v[0].ToString() + ", Error - " + (_model.MbRtu.ModbusStatus == "Read successful" ? "null value" : _model.MbRtu.ModbusStatus);
                //    _iView.UpdateLog(lastError);
                //    errorList.Add(new Errors(DateTime.Now, lastError));
                }

                valuesIndex = 0;
                for (int j = v[4]; j < v[5]; j++)
                {
                    if (device.DataItems[j].Function == v[1])
                    {
                        device.DataItems[j].Value = values != null ? (device.DataItems[j].DataType_ == DataItem.DataType.BOOL? (values[valuesIndex] > 0 ? "1" : "0") :
                            (device.DataItems[j].DataType_ == DataItem.DataType.INT ? (values[valuesIndex].ToString()) :
                            (device.DataItems[j].DataType_ == DataItem.DataType.REAL ? ((double)values[valuesIndex] / 10).ToString() : values[valuesIndex].ToString()))) :
                             device.DataItems[j].Value;

                        device.DataItems[j].TimeStamp = DateTime.Now.ToString();
                        device.DataItems[j].Quality = "Good";                                       /////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        if (device.DataItems[j].OldValue != device.DataItems[j].Value)
                        {
                            device.DataItems[j].OldTimeStamp = device.DataItems[j].TimeStamp;
                            device.DataItems[j].OldValue = device.DataItems[j].Value;
                            device.DataItems[j].NeedSend = true;
                        }

                        valuesIndex++;
                    }
                }
            }
        }

        private static void WriteValue(Device dev, int requestResponseTimeout)
        {
            while (true)
            {
                lock(writeNewVal)
                {
                    if (writeNewVal.Count > 0)
                    {
                        if (((Device)writeNewVal[0][1]).SerialPort.PortName != dev.SerialPort.PortName)
                            break;

                        ((Device)writeNewVal[0][1]).SerialPort.Write((byte[])(writeNewVal[0])[0], 0, ((byte[])(writeNewVal[0])[0]).Length);

                        Thread.Sleep(requestResponseTimeout);

                        byte[] response = null;

                        GetResponse(((Device)(writeNewVal[0])[1]).SerialPort, ref response);

                        if (((byte[])(writeNewVal[0])[0]).SequenceEqual(response))
                        {
                            ((Device)(writeNewVal[0])[1]).GoodAnswer++;
                            if (response[1] == 5)
                            {
                                var variable = ToShort(response[3], response[2]);
                                var item = ((Device)(writeNewVal[0])[1]).DataItems.Select((v) => v).Where((v2) => v2.Variable == variable & v2.DataType_ == DataItem.DataType.BOOL).ToArray();

                                if (item.Length > 0)
                                {
                                    item[0].OldValue = item[0].Value;
                                    item[0].OldTimeStamp = item[0].TimeStamp;
                                    item[0].Value = response[4] == 0xFF ? "1" : "0";
                                    item[0].TimeStamp = DateTime.Now.ToString();
                                    item[0].NeedSend = true;
                                }
                            }
                            else
                            {
                                if (response[1] == 6)
                                {
                                    var variable = ToShort(response[3], response[2]);
                                    var item = ((Device)(writeNewVal[0])[1]).DataItems.Select((v) => v).Where((v2) => v2.Variable == variable & v2.DataType_ != DataItem.DataType.BOOL).ToArray();

                                    if (item.Length > 0)
                                    {
                                        item[0].OldValue = item[0].Value;
                                        item[0].OldTimeStamp = item[0].TimeStamp;
                                        item[0].Value = item[0].DataType_ == DataItem.DataType.REAL ? (((double)ToShort(response[5], response[4])) / 10 > 3276.8 ?
                                            ((double)ToShort(response[5], response[4])) / 10 - 6553.6 : ((double)ToShort(response[5], response[4])) / 10).ToString("0.#") :
                                            (ToShort(response[5], response[4]) > 32767 ? ToShort(response[5], response[4]) - 65536 : ToShort(response[5], response[4])).ToString();
                                        item[0].TimeStamp = DateTime.Now.ToString();
                                        item[0].NeedSend = true;
                                    }
                                }
                            }
                            writeNewVal.RemoveAt(0);
                        }
                        else
                            ((Device)(writeNewVal[0])[1]).WriteError++;
                    }
                    else
                        break;
                }
            }
        }

        private static short[] UpdateValue(Device dev, byte adress, int function, ushort startAdress, ushort countRegisters, int requestResponseTimeout)
        {
            short[] values = new short[countRegisters];
            string status;

            switch (function)
            {
                case 1:
                    status = ModBusRTU.SendFc1(dev, adress, startAdress, countRegisters, requestResponseTimeout, ref values);
                    if (status == "Good")
                    {
                        if (!dev.Quality)
                        {
                            dev.NotAnswerCountForQuality = 0;
                            dev.Quality = true;
                            foreach (var v in dev.DataItems)
                            {
                                v.Quality = "Good";
                                v.NeedSend = true;
                            }
                            ErrorEventLog.LogError("GetNewValue.1 - Связь с контроллером, адрес: " + dev.Address.ToString() + " восстановлена.", Model.GetProgrammDir() + "\\Logs\\ModBusTRU.txt");
                        }
                        return values;
                    }
                    break;

                case 2:
                    status = ModBusRTU.SendFc2(dev, adress, startAdress, countRegisters, requestResponseTimeout, ref values);
                    if (status == "Good")
                    {
                        if (!dev.Quality)
                        {
                            dev.NotAnswerCountForQuality = 0;
                            dev.Quality = true;
                            foreach (var v in dev.DataItems)
                            {
                                v.Quality = "Good";
                                v.NeedSend = true;
                            }
                            ErrorEventLog.LogError("GetNewValue.1 - Связь с контроллером, адрес: " + dev.Address.ToString() + " восстановлена.", Model.GetProgrammDir() + "\\Logs\\ModBusTRU.txt");
                        }
                        return values;
                    }
                    break;

                case 3:
                    status = ModBusRTU.SendFc3(dev, adress, startAdress, countRegisters, requestResponseTimeout, ref values);
                    if (status == "Good")
                    {
                        if (!dev.Quality)
                        {
                            dev.NotAnswerCountForQuality = 0;
                            dev.Quality = true;
                            foreach (var v in dev.DataItems)
                            {
                                v.Quality = "Good";
                                v.NeedSend = true;
                            }
                            ErrorEventLog.LogError( "GetNewValue.1 - Связь с контроллером, адрес: " + dev.Address.ToString() + " восстановлена.", Model.GetProgrammDir() + "\\Logs\\ModBusTRU.txt");
                        }
                        return values;
                    }
                    break;

                case 4:
                    status = ModBusRTU.SendFc4(dev, adress, startAdress, countRegisters, requestResponseTimeout, ref values);
                    if (status == "Good")
                    {
                        if (!dev.Quality)
                        {
                            dev.NotAnswerCountForQuality = 0;
                            dev.Quality = true;
                            foreach (var v in dev.DataItems)
                            {
                                v.Quality = "Good";
                                v.NeedSend = true;
                            }
                            ErrorEventLog.LogError( "GetNewValue.1 - Связь с контроллером, адрес: " + dev.Address.ToString() + " восстановлена.", Model.GetProgrammDir() + "\\Logs\\ModBusTRU.txt");
                        }
                        return values;
                    }
                    break;
            }

            dev.NotAnswerCountForQuality++;

            if (dev.NotAnswerCountForQuality > 6)
                if (dev.Quality)
                {
                    dev.Quality = false;
                    dev.NotAnswerCountForQuality = 0;

                    foreach (var v in dev.DataItems)
                    {
                        v.NeedSend = true;
                    }


                    ErrorEventLog.LogError("UpdateValue.1 - Связь с контроллером, адрес: " + dev.Address.ToString() + " потеряна." , Model.GetProgrammDir() + "\\Logs\\ModBusTRU.txt");
                }

            return null;
        }

     

        public static int SetBit(int dw, int nBitNumber, int nBitValue)
        {
            //dw - целое, в котором задаем бит
            //nBitNumber - номер бита, который задаем (0..31)
            int dwMask = 1 << nBitNumber;// 0000100000....

            //nBitValue (0|1)
            if (nBitValue == 0)
            {//задаем 0
                dwMask = ~dwMask;// 1111011111....
                dw = dw & dwMask; // 0 & x = 0
            }
            else
            {//задаем 1
                dw = dw | dwMask;// 1 | x = 1
            }

            return dw;
        }

        public static int GetBit(int dw, int nBitNumber)
        {
            //dw - целое в котором узнаем бит
            //nBitNumber - номер бита, который узнаем (0..31)
            int dwMask = 1 << nBitNumber;// 0000100000....
            dw = dwMask & dw;
            if (dw != 0)
                return 1;
            return 0;
        }


        #region CRC Computation
        private static void GetCRC(byte[] message, ref byte[] CRC)
        {
            //Function expects a modbus message of any length as well as a 2 byte CRC array in which to 
            //return the CRC values:

            ushort CRCFull = 0xFFFF;
            byte CRCHigh = 0xFF, CRCLow = 0xFF;
            char CRCLSB;

            for (int i = 0; i < (message.Length) - 2; i++)
            {
                CRCFull = (ushort)(CRCFull ^ message[i]);

                for (int j = 0; j < 8; j++)
                {
                    CRCLSB = (char)(CRCFull & 0x0001);
                    CRCFull = (ushort)((CRCFull >> 1) & 0x7FFF);

                    if (CRCLSB == 1)
                        CRCFull = (ushort)(CRCFull ^ 0xA001);
                }
            }
            CRC[1] = CRCHigh = (byte)((CRCFull >> 8) & 0xFF);
            CRC[0] = CRCLow = (byte)(CRCFull & 0xFF);
        }
        #endregion

        #region Build Message
        private static void BuildMessage(byte address, byte type, ushort start, ushort registers, ref byte[] message)
        {
            //Array to receive CRC bytes:
            byte[] CRC = new byte[2];

            message[0] = address;
            message[1] = type;
            message[2] = (byte)(start >> 8);
            message[3] = (byte)start;
            message[4] = (byte)(registers >> 8);
            message[5] = (byte)registers;

            GetCRC(message, ref CRC);
            message[message.Length - 2] = CRC[0];
            message[message.Length - 1] = CRC[1];
        }
        #endregion

        #region Check Response
        private static bool CheckResponse(byte[] response)
        {
            try
            {
                //Perform a basic CRC check:
                byte[] CRC = new byte[2];
                GetCRC(response, ref CRC);
                if (CRC[0] == response[response.Length - 2] && CRC[1] == response[response.Length - 1])
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError("CheckResponse.1 - " + ex.Message, Model.GetProgrammDir() + "\\Logs\\ModBusTRU.txt");
                //MessageBox.Show(ex.Message);
                return false;
            }
        }
        #endregion

        #region Get Response
        private static void GetResponse(SerialPort sp, ref byte[] response)
        {
            if (sp.BytesToRead == 0)
                return;

            int readBytes = sp.BytesToRead;
            response = new byte[readBytes];
            sp.Read(response, 0, readBytes);
            //There is a bug in .Net 2.0 DataReceived Event that prevents people from using this
            //event as an interrupt to handle data (it doesn't fire all of the time).  Therefore
            //we have to use the ReadByte command for a fixed length as it's been shown to be reliable.
            //for (int i = 0; i < readBytes; i++)//response.Length; i++)
            //{
            //    response[i] = (byte)(sp.ReadByte());
            //}
        }
        #endregion

        #region Get Response
        private static void GetResponseBool(SerialPort sp, ref byte[] response)
        {
            if (sp.BytesToRead == 0)
                return;

            int readBytes = sp.BytesToRead;
            response = new byte[readBytes];
            sp.Read(response, 0, readBytes);
            //There is a bug in .Net 2.0 DataReceived Event that prevents people from using this
            //event as an interrupt to handle data (it doesn't fire all of the time).  Therefore
            //we have to use the ReadByte command for a fixed length as it's been shown to be reliable.
            //for (int i = 0; i < readBytes; i++)//response.Length; i++)
            //{
            //    response[i] = (byte)(sp.ReadByte());
            //}
        }
        #endregion


        /// <summary>
        /// Read Coils
        /// </summary>
        /// <param name="sp">Выбранный com порт</param>
        /// <param name="address">Адрес устройства в сети</param>
        /// <param name="startCoil">Адрес начальной ячейки</param>
        /// <param name="coilsCount">Количество читаемых ячеек</param>
        /// <param name="values">Переменная массив -результат</param>
        /// <returns>true в случае безошибочного считывания, false в случае возникновения ошибок, при этом в переменную modbusStatus сохраняется информация об ошибке</returns>
        public static string SendFc1(Device dev, byte address, ushort startCoil, ushort coilsCount, int requestResponseTimeout, ref short[] values, bool read = true)
        {
            //Stopwatch sw = new Stopwatch();
            //Ensure port is open:
            if (dev.SerialPort.IsOpen)
            {
                //Clear in/out buffers:
                dev.SerialPort.DiscardOutBuffer();
                dev.SerialPort.DiscardInBuffer();

                //Function 1 request is always 8 bytes:
                // 1 байт - адрес устройства
                // 2 байт - код функции
                // 3 байт - начальный адрес ячейки, старший байт
                // 4 байт - начальный адрес ячейки, младший байт (от 0х0000 до 0хFFFF)
                // 5 байт - количество считываемых ячеек, старший байт
                // 6 байт - количество считываемых ячеек, младший байт (от 1 до 2000)
                // 7 и 8 байты - CRC
                byte[] message = new byte[8];

                int bitCount = 0;
                for (int i = 0; i < 25; i = i + 8)
                {
                    bitCount++;
                }

                //Function 1 response buffer:
                byte[] response = new byte[bitCount];//предварительная инициализация, чтоб не было ошибки. Переинициализация будет произведена в методе GetResponse

                //Build outgoing modbus message:
                BuildMessage(address, (byte)1, startCoil, coilsCount, ref message);
                //Send modbus message to Serial Port:



                try
                {
                    //sw.Start();
                    dev.SerialPort.Write(message, 0, message.Length);//отправка сообщения
                    System.Threading.Thread.Sleep(requestResponseTimeout);

                    if (read)
                        GetResponseBool(dev.SerialPort, ref response);//в response будет помещен результат, при чем кол-во байт будет переопределено

                  //  response = new byte[] { 0x40, 0x86, 0x6A, 0x06, 0x80, 0xFA, 0xFF, 0xFF, 0x00, 0x00, 0x2C, 0x61 };
                    //sw.Stop();
                }
                catch (Exception err)
                {
    //                if (UpdateStatisticListViewEvent != null)
    //                    UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                    return "Error in read event: " + err.Message;
                }

                if (read)
                //Evaluate message:
                if (CheckResponse(response))
                {
                    if (response.Length < 6)//при возникновении ошибки, согласно спецификации размер массива будет меньше 6 байт
                    {
                            dev.ReadError++;
 //                       if (UpdateStatisticListViewEvent != null)
 //                           UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                        return "Error code - " + response[2].ToString();
                    }
                    else
                    {
                        int arrayIndex = 0;
                        //Return requested register values:
                        for (int i = 3; i < 3 + response[2]; i++)
                        {
                            //values[i - 3] = response[i];

                            for (int j = 1; j < 9; j++)
                            {
                                values[arrayIndex] = (response[i] & (1 << j - 1)) != 0 ? (short)1 : (short)0;
                                arrayIndex++;

                                if (arrayIndex == coilsCount)
                                    break;
                            }
                        }
                            //                      if (UpdateStatisticListViewEvent != null)
                            //                          UpdateStatisticListViewEvent(new RequestResult(true, sw.Elapsed), (new ExtendedEventArgs(null)));
                            dev.GoodAnswer++;
                        return "Good";
                    }
                }
                else
                {
                        dev.CrcError++;
  //                  if (UpdateStatisticListViewEvent != null)
  //                      UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                    return "CRC error";
                }
                return "";
            }
            else
            {
                dev.СomPortError++;
  //              if (UpdateStatisticListViewEvent != null)
  //                  UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                return "Serial port not open";
            }

        }

        /// <summary>
        /// Read Discrete Inputs
        /// </summary>
        /// <param name="sp">Выбранный com порт</param>
        /// <param name="address">Адрес устройства в сети</param>
        /// <param name="startCoil">Адрес начальной ячейки</param>
        /// <param name="coilsCount">Количество читаемых ячеек</param>
        /// <param name="values">Переменная массив -результат</param>
        /// <returns>true в случае безошибочного считывания, false в случае возникновения ошибок, при этом в переменную modbusStatus сохраняется информация об ошибке</returns>
        public static string SendFc2(Device dev, byte address, ushort startCoil, ushort coilsCount, int requestResponseTimeout, ref short[] values, bool read = true)
        {
  //          Stopwatch sw = new Stopwatch();
            //Ensure port is open:
            if (dev.SerialPort.IsOpen)
            {
                //Clear in/out buffers:
                dev.SerialPort.DiscardOutBuffer();
                dev.SerialPort.DiscardInBuffer();

                //Function 2 request is always 8 bytes:
                // 1 байт - адрес устройства
                // 2 байт - код функции
                // 3 байт - начальный адрес ячейки, старший байт
                // 4 байт - начальный адрес ячейки, младший байт (от 0х0000 до 0хFFFF)
                // 5 байт - количество считываемых ячеек, старший байт
                // 6 байт - количество считываемых ячеек, младший байт (от 1 до 2000)
                // 7 и 8 байты - CRC
                byte[] message = new byte[8];

                int bitCount = 0;
                for (int i = 0; i < 25; i = i + 8)
                {
                    bitCount++;
                }

                //Function 3 response buffer:
                byte[] response = new byte[bitCount];//предварительная инициализация, чтоб не было ошибки. Переинициализация будет произведена в методе GetResponse

                //Build outgoing modbus message:
                BuildMessage(address, (byte)2, startCoil, coilsCount, ref message);



                //Send modbus message to Serial Port:
                try
                {
                    //                sw.Start();
                    dev.SerialPort.Write(message, 0, message.Length);//отправка сообщения
                    System.Threading.Thread.Sleep(requestResponseTimeout);

                    if (read)
                    GetResponse(dev.SerialPort, ref response);//в response будет помещен результат, при чем кол-во байт будет переобпределено
    //                sw.Stop();
                }
                catch (Exception err)
                {
    //                if (UpdateStatisticListViewEvent != null)
    //                    UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                    return "Error in read event: " + err.Message;
                }
                if (read)
                //Evaluate message:
                if (CheckResponse(response))
                {
                    if (response.Length < 6)//при возникновении ошибки, согласно спецификации размер массива будет меньше 6 байт
                    {
                            dev.ReadError++;
                            //                    if (UpdateStatisticListViewEvent != null)
                            //                        UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                            return "Error code - " + response[2].ToString();
                    }
                    else
                    {
                        int arrayIndex = 0;
                        //Return requested register values:
                        for (int i = 3; i < 3 + response[2]; i++)
                        {
                            //values[i - 3] = response[i];

                            for (int j = 1; j < 9; j++)
                            {
                                values[arrayIndex] = (response[i] & (1 << j - 1)) != 0 ? (short)1 : (short)0;
                                arrayIndex++;

                                if (arrayIndex == coilsCount)
                                    break;
                            }
                        }
                            //                    if (UpdateStatisticListViewEvent != null)
                            //                        UpdateStatisticListViewEvent(new RequestResult(true, sw.Elapsed), (new ExtendedEventArgs(null)));
                            dev.GoodAnswer++;
                            return "Good";
                    }
                }
                else
                {
                        dev.CrcError++;
                        //                if (UpdateStatisticListViewEvent != null)
                        //                    UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                        return "CRC error";
                }

                return "";
            }
            else
            {
                dev.СomPortError++;
                //            if (UpdateStatisticListViewEvent != null)
                //                UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                return "Serial port not open";
            }

        }

        #region Function 3 - Read Registers
        public static string SendFc3(Device dev, byte address, ushort start, ushort registers, int requestResponseTimeout, ref short[] values, bool read = true)
        {
  //          Stopwatch sw = new Stopwatch();
            //Ensure port is open:
            if (dev.SerialPort.IsOpen)
            {
                //Clear in/out buffers:
                dev.SerialPort.DiscardOutBuffer();
                dev.SerialPort.DiscardInBuffer();
                //Function 3 request is always 8 bytes:
                byte[] message = new byte[8];
                //Function 3 response buffer:
                byte[] response = new byte[5 + 2 * registers];
                //Build outgoing modbus message:
                BuildMessage(address, (byte)3, start, registers, ref message);


                //Send modbus message to Serial Port:
                try
                {
                    //                sw.Start();
                    dev.SerialPort.Write(message, 0, message.Length);
                    System.Threading.Thread.Sleep(requestResponseTimeout);
                    if (read)
                        GetResponse(dev.SerialPort, ref response);
                       
    //                sw.Stop();
                }
                catch (Exception err)
                {
    //                if (UpdateStatisticListViewEvent != null)
    //                    UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                    return "Error in read event: " + err.Message;
                }
                if (read)
                //Evaluate message:
                if (CheckResponse(response))
                {
                    if (response.Length < 6)
                    {
                            dev.ReadError++;
                            //                   if (UpdateStatisticListViewEvent != null)
                            //                       UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                            return "Error code - " + response[2].ToString();
                    }
                    else
                    {
                        int bytesCount = (response.Length - 5) / 2;
                        //Return requested register values:
                        for (int i = 0; i < bytesCount; i++)
                        {
                            values[i] = response[2 * i + 3];
                            values[i] <<= 8;
                            values[i] += response[2 * i + 4];
                        }

                            dev.GoodAnswer++;
                            //                    if (UpdateStatisticListViewEvent != null)
                            //                        UpdateStatisticListViewEvent(new RequestResult(true, sw.Elapsed), (new ExtendedEventArgs(null)));
                            return "Good";
                    }
                }
                else
                {
                        dev.CrcError++;
                        //               if (UpdateStatisticListViewEvent != null)
                        //                   UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                        return "CRC error";
                }
                return "";
            }
            else
            {
                dev.СomPortError++;
                //             if (UpdateStatisticListViewEvent != null)
                //                 UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                return "Serial port not open";
            }

        }
        #endregion

        #region Function 4 - Read Registers
        public static string SendFc4(Device dev, byte address, ushort start, ushort registers, int requestResponseTimeout, ref short[] values, bool read = true)
        {
   //         Stopwatch sw = new Stopwatch();

            //Ensure port is open:
            if (dev.SerialPort.IsOpen)
            {
                //Clear in/out buffers:
                dev.SerialPort.DiscardOutBuffer();
                dev.SerialPort.DiscardInBuffer();
                //Function 3 request is always 8 bytes:
                byte[] message = new byte[8];
                //Function 3 response buffer:
                byte[] response = new byte[5 + 2 * registers];
                //Build outgoing modbus message:
                BuildMessage(address, (byte)4, start, registers, ref message);


                //Send modbus message to Serial Port:
                try
                {
                    //                 sw.Start();
                    dev.SerialPort.Write(message, 0, message.Length);
                    System.Threading.Thread.Sleep(requestResponseTimeout);
                    if (read)
                        GetResponse(dev.SerialPort, ref response);
   //                 sw.Stop();
                }
                catch (Exception err)
                {
    //                if (UpdateStatisticListViewEvent != null)
    //                    UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                    return "Error in read event: " + err.Message;
                }
                if (read)
                //Evaluate message:
                if (CheckResponse(response))
                {
                    if (response.Length < 6)
                    {
                            dev.ReadError++;
                            //                   if (UpdateStatisticListViewEvent != null)
                            //                       UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                            return "Error code - " + response[2].ToString();
                    }
                    else
                    {
                        int bytesCount = (response.Length - 5) / 2;
                        //Return requested register values:
                        for (int i = 0; i < bytesCount; i++)
                        {
                            values[i] = response[2 * i + 3];
                            values[i] <<= 8;
                            values[i] += response[2 * i + 4];
                        }

                            dev.GoodAnswer++;
                            //                  if (UpdateStatisticListViewEvent != null)
                            //                      UpdateStatisticListViewEvent(new RequestResult(true, sw.Elapsed), (new ExtendedEventArgs(null)));
                            return "Good";
                    }
                }
                else
                {
                        dev.CrcError++;
                        //              if (UpdateStatisticListViewEvent != null)
                        //                  UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                        return "CRC error";
                }
                return "";
            }
            else
            {
                dev.СomPortError++;
                //           if (UpdateStatisticListViewEvent != null)
                //               UpdateStatisticListViewEvent(new RequestResult(false, sw.Elapsed), (new ExtendedEventArgs(null)));
                return "Serial port not open";
            }

        }
        #endregion

        public static void WriteSingleRegister(Device dev, ushort address, int value)
        {
                byte[] message = new byte[8];
         
                byte[] response = new byte[8];
         
                byte byte2;
                byte byte1;
                byte byte4;
                byte byte3;

                FromShort(address, out byte1, out byte2);
                FromInt(value, out byte3, out byte4);

                message[0] = (byte)dev.Address;
                message[1] = 0x06;
                message[2] = byte2;
                message[3] = byte1;
                message[4] = byte4;
                message[5] = byte3;

                byte[] CRC = new byte[2];

                GetCRC(message, ref CRC);
                message[message.Length - 2] = CRC[0];
                message[message.Length - 1] = CRC[1];

            ModBusRTU.writeNewVal.Add(new object[] { message, dev, value });
        }

        public static void WriteSingleCoil(Device dev, ushort address, short value)
        {
          //  dev.serialPort.DiscardOutBuffer();
          //  dev.serialPort.DiscardInBuffer();

            byte[] message = new byte[8];

            byte byte2;
            byte byte1;
            byte byte4;
            byte byte3;

            FromShort(address, out byte1, out byte2);
            FromInt(value, out byte3, out byte4);

            message[0] = (byte)dev.Address;
            message[1] = 0x05;
            message[2] = byte2;
            message[3] = byte1;
            message[4] = value != 0 ? (byte)0xFF : (byte)0x0;
            message[5] = 0;

            byte[] CRC = new byte[2];

            GetCRC(message, ref CRC);
            message[message.Length - 2] = CRC[0];
            message[message.Length - 1] = CRC[1];

            ModBusRTU.writeNewVal.Add(new object[] { message, dev });
            
        }

        public static object[] FromString(string s)
        {
            short res = 0;
            bool needDelOn10 = false;
            bool er = false;

            try
            {
                double f = double.Parse(s);
                double f2 = f > 0 ? f : f * -1;
                if (f2 - (int)f2 > 0)
                {
                    if (f > 3276.7 | f < -3276.8)
                        er = true;

                    res = (short)(f * 10);
                    needDelOn10 = true;
                }
                else
                {
                    if (f > 32767 | f < -32768)
                        er = true;

                    res = (short)f;
                }
            }
            catch
            {
                er = true;
            }

            
            return new object[] { res, needDelOn10, er };
        }

        static int ToShort(byte byte1, byte byte2)
        {
            int s = (int)((byte2 << 8) | (byte1 << 0));
            return s;
        }

        static void FromShort(short number, out byte byte1, out byte byte2)
        {
            byte2 = (byte)(number >> 8);
            byte1 = (byte)(number & 255);
        }

        static void FromInt(int number, out byte byte1, out byte byte2)
        {

            byte2 = (byte)(number >> 8);
            byte1 = (byte)(number & 255);
        }

        static void FromShort(ushort number, out byte byte1, out byte byte2)
        {
            byte2 = (byte)(number >> 8);
            byte1 = (byte)(number & 255);
        }
    }
}
