﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Xml.Serialization;

namespace ProtocolTester
{
    [Serializable]
    public class ConnectionLine485
    {
        private static ulong crcError;
        private static ulong unitAnswerError;
        private static ulong notAnswer;
        private static ulong emptyAnswer;
        private static ulong writeError;
        private static ulong comPortError;
        private static ulong readError;
        private static ulong goodAnswer;
        private static ulong allRequest;
        private static ulong incorrectAnswer;

        /// <summary>
        /// Всего запросов
        /// </summary>
        [XmlIgnore]
        public static ulong AllRequest
        {
            get { return allRequest; }
            set { allRequest = value; }
        }

        /// <summary>
        /// Ошибки чтения из порта
        /// </summary>
        [XmlIgnore]
        public static ulong IncorrectAnswer
        {
            get { return incorrectAnswer; }
            set { incorrectAnswer = value == 0 ? value : (incorrectAnswer + 1 > 18446744073709551610 ? 0 : incorrectAnswer + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; }
        }

        /// <summary>
        /// Ошибки чтения из порта
        /// </summary>
        [XmlIgnore]
        public static ulong GoodAnswer
        {
            get { return goodAnswer; }
            set { goodAnswer = value == 0 ? value : (goodAnswer + 1 > 18446744073709551610 ? 0 : goodAnswer + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; }
        }

        /// <summary>
        /// Ошибки чтения из порта
        /// </summary>
        [XmlIgnore]
        public static ulong ReadError
        {
            get { return readError; }
            set { readError = value == 0 ? value : (readError + 1 > 18446744073709551610 ? 0 : readError + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; }
        }

        /// <summary>
        /// Ошибки ком порта (порт не открыт, порт занят)
        /// </summary>
        [XmlIgnore]
        public static ulong СomPortError
        {
            get { return comPortError; }
            set { comPortError = value == 0 ? value : (comPortError + 1 > 18446744073709551610 ? 0 : comPortError + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; }
        }

        /// <summary>
        /// Ошибки записи в порт
        /// </summary>
        [XmlIgnore]
        public static ulong WriteError
        {
            get { return writeError; }
            set { writeError = value == 0 ? value : (writeError + 1 > 18446744073709551610 ? 0 : writeError + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; }
        }

        /// <summary>
        /// С момента последнего запроса изменения не произошли
        /// </summary>
        [XmlIgnore]
        public static ulong EmptyAnswer
        {
            get { return emptyAnswer; }
            set { emptyAnswer = value == 0 ? value : (emptyAnswer + 1 > 18446744073709551610 ? 0 : emptyAnswer + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; }
        }

        /// <summary>
        /// Нет ответа от устройства
        /// </summary>
        [XmlIgnore]
        public static ulong NotAnswer
        {
            get { return notAnswer; }
            set { notAnswer = value == 0 ? value : (notAnswer + 1 > 18446744073709551610 ? 0 : notAnswer + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; }
        }

        /// <summary>
        /// Ошибки ответа контроллера
        /// </summary>
        [XmlIgnore]
        public static ulong UnitAnswerError
        {
            get { return unitAnswerError; }
            set { unitAnswerError = value == 0 ? value : (unitAnswerError + 1 > 18446744073709551610 ? 0 : unitAnswerError + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; }
        }

        /// <summary>
        /// Ошибки контрольной суммы
        /// </summary>
        [XmlIgnore]
        public static ulong CrcError
        {
            get { return crcError; }
            set { crcError = value == 0 ? value : (crcError + 1 > 18446744073709551610 ? 0 : crcError + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; }
        }

        public event EventHandler<ExtendedEventArgs> ChangeMode;


        private Model.Mode mode;

        public int RWTimeout { get; set; }

        public int UpdateInterval { get; set; }

        [NonSerialized]
        private SerialPort port;
        [XmlIgnore]
        public SerialPort Port { get { return port; } set { port = value; } }

        public string PortName { get; set; }
        public int BaudRate { get; set; }
        public Parity _Parity { get; set; }
        public int DataBits { get; set; }
        public StopBits _StopBits { get; set; }

        //[XmlIgnore]
        public Model.Mode Mode 
        {
            get { return mode; }
            set
            {
                if (ChangeMode != null)
                    ChangeMode(null, new ExtendedEventArgs(value));
                mode = value;
            }
        }

        [XmlIgnore]
        private List<Device> device = new List<Device>();

        [XmlArray("DeviceCollection"), XmlArrayItem("Device")]
        //[XmlIgnore]
        public List<Device> _Device
        {
            get { return device; }
            set { device = value; }
        }

        public ConnectionLine485()
        {
            Port = new SerialPort();
            RWTimeout = -1;
            UpdateInterval = -1;
        }

        public ConnectionLine485(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits, int deviceAddress, Model.Mode _mode)
        {
            Port = (SerialPort)(new SerialPort(portName, baudRate, parity, dataBits, stopBits));

            PortName = portName;
            BaudRate = baudRate;
            _Parity = parity;
            DataBits = dataBits;
            _StopBits = stopBits;
           
            mode = _mode;

            Device _device = new Device();

            deviceAddress = mode == Model.Mode.Carel ? deviceAddress + 48 : deviceAddress;

            switch (mode)
            {
                case Model.Mode.ModBusRTU:
                    _device = new ModBusRTUUnit(Port, Port.PortName, deviceAddress);
                    break;
                case  Model.Mode.Carel:
                    _device = new CarelUnit(Port, Port.PortName, deviceAddress);
                    break;
            }

            device.Add(_device);
        }

        public static void ResetStatistic()
        {
            crcError = 0;
            unitAnswerError = 0;
            notAnswer = 0;
            emptyAnswer = 0;
            writeError = 0;
            comPortError = 0;
            readError = 0;
            goodAnswer = 0;
            allRequest = 0;
            incorrectAnswer = 0;
        }
    }

}
