﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMC.ARSystem;
using ProtocolTester.SettingsFormNamespace;

namespace ProtocolTester
{
    public class BMCRemedy
    {
        public Server _BMCServer { get; set; }
        private BMCSettings _BMCSettings;

        private string atachFile;
        private string atachComment;

        /// <summary>
        /// Комментарий к вложению
        /// </summary>
        public string AtachComment
        {
            get { return atachComment; }
            set { atachComment = value; }
        }

        /// <summary>
        /// Файл вложения
        /// </summary>
        public string AtachFile
        {
            get { return atachFile; }
            set { atachFile = value; }
        }

        

        public BMCRemedy(Server server, BMCSettings bmcSettings)
        {
            _BMCServer = server;
            _BMCSettings = bmcSettings;
        }

        /// <summary>
        /// Сознание инцидента в системе BMC
        /// </summary>
        /// <param name="bmcPat">Ссылка на экземпляр класса шаблона</param>
        public void CreateIncidentBMC(BMCRemedy bmcPat, Device unit)
        {

            try
            {
                var v = _BMCServer._Context;

                if (_BMCServer._Context == null)
                    _BMCServer.Login(_BMCSettings.ServerAddress, _BMCSettings.UserName, _BMCSettings.Password, "", _BMCSettings.Port);
                else
                    if (_BMCServer._Context.ToString() == "0")
                        _BMCServer.Login(_BMCSettings.ServerAddress, _BMCSettings.UserName, _BMCSettings.Password, "", _BMCSettings.Port);

            
                //Подготовка переменных полей для создания инцидента
                string createFormName = "HPD:IncidentInterface_Create";
                FieldValueList valList = new FieldValueList();
                
                valList.Add(1000000076, "CREATE");
                valList.Add(1000000018, bmcPat._BMCSettings.LastName);//"Мальцев");
                valList.Add(1000000019, bmcPat._BMCSettings.FirstName);//"Юрий");
                valList.Add(1000000020, bmcPat._BMCSettings.MiddleName);//"Юрьевич");
                valList.Add(7, "New");
                valList.Add(1000000099, "User Service Request");
                valList.Add(1000000163, (bmcPat._BMCSettings.Impact + 1) * 1000);//Влияние
                valList.Add(1000000164, 3);   //Приоритет
                valList.Add(1000000162, (bmcPat._BMCSettings.Urgency + 1) * 1000);//Срочность
                //Тема
                valList.Add(1000000000, bmcPat._BMCSettings.ObjectName + " " + bmcPat._BMCSettings.ShortDescription);
                //Детальное описание
                valList.Add(1000000151, "Нет связи с контроллером, адрес " + (unit is CarelUnit ? unit.Address - 48 : unit.Address).ToString());
                valList.Add(1000000215, 11000);
                valList.Add(1000000001, "АО Тандер Головная компания");
                valList.Add(1000000082, "АО Тандер Головная компания");
                //valList.Add(1000005661, bmcPat.FinishDate);
                //Сервис
                valList.Add(303497300, bmcPat._BMCSettings.Service);


                //Получение доп полей///////////////////////////////////////////////////////////////////////////////////////////////////////
                string sql = "('200000020' = \"" + bmcPat._BMCSettings.Service + "\" and '200000005' != null)";
                int[] entryFields = { 400129200, 200000020, 200000003, 200000004, 200000005 };
                EntryFieldValueList entryWithFields = _BMCServer.GetListEntryWithFields("AST:BusinessService", sql, entryFields, Convert.ToUInt32(0), Convert.ToUInt32(1));

                foreach (EntryFieldValue entry in entryWithFields)
                {
                    if (entry.FieldValues.ContainsKey(Convert.ToUInt32(200000003)))
                        valList.Add(200000003, entry.FieldValues[200000003].ToString());
                    if (entry.FieldValues.ContainsKey(Convert.ToUInt32(200000004)))
                        valList.Add(200000004, entry.FieldValues[200000004].ToString());
                    if (entry.FieldValues.ContainsKey(Convert.ToUInt32(200000005)))
                        valList.Add(200000005, entry.FieldValues[200000005].ToString());
                    if (entry.FieldValues.ContainsKey(Convert.ToUInt32(400129200)))
                        valList.Add(303519300, entry.FieldValues[400129200].ToString());
                    break;

                }
                

                valList.Add(230000000, "AST:BusinessService");
                valList.Add(230000009, "BMC_BUSINESSSERVICE");
                valList.Add(1000000560, DateTime.Now.AddHours(-3).ToString());
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                valList.Add(1000000217, bmcPat._BMCSettings.WorkGroup);

                sql = "'1000000015' = \"" + bmcPat._BMCSettings.WorkGroup + "\"";
                entryFields = new int[] { 1000000001, 1000000014, 1000000015, 1 };
                entryWithFields = _BMCServer.GetListEntryWithFields("CTM:Support Group", sql, entryFields, Convert.ToUInt32(0), Convert.ToUInt32(1));

                foreach (EntryFieldValue entry in entryWithFields)
                {
                    if (entry.FieldValues.ContainsKey(Convert.ToUInt32(1)))
                    {
                        valList.Add(1000000079, entry.FieldValues[1].ToString());
                        valList.Add(1000000014, entry.FieldValues[1000000014].ToString());
                        valList.Add(1000000251, entry.FieldValues[1000000001].ToString());
                        break;
                    }
                }

                valList.Add(1000000022, 2000);
                valList.Add(1000000026, 2000);
                valList.Add(1000000169, 0);

                try
                {
                    string res = _BMCServer.CreateEntry(createFormName, valList);

                    if (res != "")
                    {
                        string res2 = GetINCfromREQ(res);

                        if (bmcPat.AtachComment != null & bmcPat.AtachFile != null)
                            if (bmcPat.AtachComment != "" & bmcPat.AtachFile != "")
                                AddComment(bmcPat.AtachComment, res2, bmcPat.AtachFile);//@"Y:\_Departments\Эксплуатация ГМ, РЦ, АТП\15_Диспетчер ДЭ\акт проверки ДГУ образец заполнения.odt");

                        ErrorEventLog.LogError("CreateIncidentBMC: создан инцидент BMC - " + res2, Model.GetProgrammDir() + "\\Logs\\BMC.txt");
                    }
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError("CreateIncidentBMC_2: " + ex.Message, Model.GetProgrammDir() + "\\Logs\\BMC.txt");
                }

                _BMCServer.Logout();
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError("CreateIncidentBMC_3: " + ex.Message, Model.GetProgrammDir() + "\\Logs\\BMC.txt");
            }

            unit.bMCRemedy = null;
        }

        /// <summary>
        /// Получение номера инцидента из id
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        private string GetINCfromREQ(string req)
        {
            string inc = "";
            string sql = "'1' = \"" + req + "\"";
            int[] entryFields = { 1000000161 };

            var entryWithFields = _BMCServer.GetListEntryWithFields("HPD:IncidentInterface_Create", sql, entryFields, Convert.ToUInt32(0), Convert.ToUInt32(1));

            FieldValueList valList = new FieldValueList(); ;

            foreach (EntryFieldValue entry in entryWithFields)
            {
                if (entry.FieldValues.ContainsKey(Convert.ToUInt32(1000000161)))
                {
                    inc = entry.FieldValues[1000000161].ToString();
                    break;
                }
            }

            return inc;
        }

        //Добаление комментария к инциденту
        private string AddComment(string comment, string inc, string filename = "")
        {
            try
            {
                Attachment at = new Attachment();

                if (filename != "")
                {
                    string[] strarr = filename.Split('\\');

                    at.SetValue(strarr[strarr.Length - 1], filename);
                }
                string formName = "HPD:WorkLog";

                FieldValueList fieldValues = new FieldValueList();
                fieldValues[1000000000] = comment;
                fieldValues[1000000151] = comment;
                fieldValues[1000000161] = inc;
                fieldValues[1000000170] = "8000";

                if (filename != "")
                {
                    fieldValues[1000000351] = at;
                }

                string entryId = _BMCServer.CreateEntry(formName, fieldValues);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "";
        }

    }
}
