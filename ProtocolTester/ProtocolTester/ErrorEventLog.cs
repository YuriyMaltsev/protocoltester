﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ProtocolTester
{
    //Класс реализующий интерфейс IErrorEventLog, методы которого сохраняют логи в текстовые файлы
    public static class ErrorEventLog
    {
        public static void LogError(string errorText, string fileName)
        {
            using (StreamWriter sw = File.AppendText(fileName))
            {
                lock(sw)
                    sw.WriteLine(DateTime.Now.ToString() + ": " + errorText);
            }
        }

        public static void LogEvent(string eventText)
        {
            using (StreamWriter sw = File.AppendText("Eventlog.txt"))
            {
                sw.WriteLine(DateTime.Now.ToString() + ": " + eventText);
            }
        }
    }
}
