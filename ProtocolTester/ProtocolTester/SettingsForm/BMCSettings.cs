﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMC.ARSystem;
using System.Xml.Serialization;

namespace ProtocolTester{
    [Serializable]
    public class BMCSettings
    {
        public string ObjectName { get; set; }
        public string ShortDescription { get; set; }
        public string Service { get; set; }
        public string WorkGroup { get; set; }
        public int Timeout { get; set; }
        public bool FunctionActivated { get; set; }
        public int Impact { get; set; }
        public int Urgency { get; set; }
        public string UserName { get; set; }

        public string FirstName { get; set; }//Имя
        public string LastName { get; set; }//Фамилия
        public string MiddleName { get; set; }//Отчество
                                              //Пользователь BMC от имени которого будут создаваться инциденты.

        [XmlIgnore]
        public string Description { get; set; }

        [XmlIgnore]
        public string Password { get; set; }

        [XmlIgnore]
        public Server BMCServer { get; set; }

        public string ServerAddress { get; set; }
        public int Port { get; set; } 

        public SerializableDictionary<string, ServiceRows> ServiceTable = new SerializableDictionary<string, ServiceRows>();//Список сервисов с доп полями
        public SerializableDictionary<string, WorkGroupRows> WorkGroupTable = new SerializableDictionary<string, WorkGroupRows>();//Список Рабочих групп 

        public BMCSettings()
        {

        }
    }

    [Serializable]
    public class ServiceRows
    {

        public ServiceRows()
        {

        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="service_name">Имя сервиса</param>
        /// <param name="tier1_param">Доп. поле Product Categorization Tier 1</param>
        /// <param name="tier2_param">Доп. поле Product Categorization Tier 2</param>
        /// <param name="tier3_param">Доп. поле Product Categorization Tier 3</param>
        /// <param name="service_id">Уникальный ID сервиса в таблице БД BMC</param>
        /// <param name="stateSel">Статус выбран ли данный сервис или нет</param>
        public ServiceRows(string service_name, string tier1_param, string tier2_param, string tier3_param, string service_id, bool stateSel)
        {
            serviceName = service_name;
            tier1 = tier1_param;
            tier2 = tier2_param;
            tier3 = tier3_param;
            serviceID = service_id;
            stateSelect = stateSel;
        }

        private string serviceName;
        private string tier1;
        private string tier2;
        private string tier3;
        private string serviceID;
        private bool stateSelect;

        /// <summary>
        /// Имя сервиса
        /// </summary>
        public string ServiceName
        {
            get { return serviceName; }
            set { serviceName = value; }
        }

        /// <summary>
        /// Доп. поле Product Categorization Tier 1
        /// </summary>
        public string Tier1
        {
            get { return tier1; }
            set { tier1 = value; }
        }

        /// <summary>
        /// Доп. поле Product Categorization Tier 2
        /// </summary>
        public string Tier2
        {
            get { return tier2; }
            set { tier2 = value; }
        }

        /// <summary>
        /// Доп. поле Product Categorization Tier 3
        /// </summary>
        public string Tier3
        {
            get { return tier3; }
            set { tier3 = value; }
        }

        /// <summary>
        /// Уникальный ID сервиса в таблице БД BMC
        /// </summary>
        public string ServiceID
        {
            get { return serviceID; }
            set { serviceID = value; }
        }

        /// <summary>
        /// Статус выбран ли данный сервис или нет
        /// </summary>
        public bool StateSelect
        {
            get { return stateSelect; }
            set { stateSelect = value; }
        }
    }

    [Serializable]
    public class WorkGroupRows
    {
        public WorkGroupRows()
        {

        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="workGroup_name">Наименование РГ</param>
        /// <param name="support_Organization">Наименование обслуживающей организации</param>
        /// <param name="company_name">Наименование компании</param>
        /// <param name="workGroup_id">Уникальный ID РГ в таблице БД BMC</param>
        /// <param name="stateSel">Статус выбрана ли данная РГ или нет</param>
        public WorkGroupRows(string workGroup_name, string support_Organization, string company_name, string workGroup_id, bool stateSel)
        {
            workGroupName = workGroup_name;
            supportOrganization = support_Organization;
            companyName = company_name;
            workGroupID = workGroup_id;
            stateSelect = stateSel;
        }

        private string workGroupName;
        private string supportOrganization;
        private string companyName;
        private string workGroupID;
        private bool stateSelect;

        /// <summary>
        /// Наименование РГ
        /// </summary>
        public string WorkGroupName
        {
            get { return workGroupName; }
            set { workGroupName = value; }
        }

        /// <summary>
        /// Наименование обслуживающей организации
        /// </summary>
        public string SupportOrganization
        {
            get { return supportOrganization; }
            set { supportOrganization = value; }
        }

        /// <summary>
        /// Наименование компании
        /// </summary>
        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        /// <summary>
        /// Уникальный ID РГ в таблице БД BMC
        /// </summary>
        public string WorkGroupID
        {
            get { return workGroupID; }
            set { workGroupID = value; }
        }

        /// <summary>
        /// Статус выбрана ли данная РГ или нет
        /// </summary>
        public bool StateSelect
        {
            get { return stateSelect; }
            set { stateSelect = value; }
        }
    }
}
