﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProtocolTester.SettingsFormNamespace
{
    public partial class SettingsForm : Form, ISettingsForm
    {
        string path;

        public event EventHandler<EventArgs> GetPathButtonsClick;
        public event EventHandler<EventArgs> checkBoxAutoStartStateChange;
        public event EventHandler<EventArgs> FormLoad;
        public event EventHandler<EventArgs> ButtonsClick;

        public SettingsForm(string path)
        {
            InitializeComponent();
            this.path = path;
        }

        public string TextBoxPath
        {
            set { textBoxPath.Text = value; }
        }
        

        public string textBox_Object
        {
            get { return textBoxObject.Text; }
            set { textBoxObject.Text = value; }
        }

        public string textBox_ShortDescription
        {
            get { return textBoxShortDescription.Text; }
            set { textBoxShortDescription.Text = value; }
        }

        public string textBox_Service
        {
            get { return textBoxService.Text; }
            set { textBoxService.Text = value; }
        }

        public string textBox_WorkGroup
        {
            get { return textBoxWorkGroup.Text; }
            set { textBoxWorkGroup.Text = value; }
        }

        public string textBox_Timeout
        {
            get { return textBoxTimeout.Text; }
            set { textBoxTimeout.Text = value; }
        }

        public int textBox_Impact
        {
            get { return comboBoxImpact.SelectedIndex ; }
            set { comboBoxImpact.SelectedIndex = value; }
        }

        public int textBox_Urgency
        {
            get { return comboBoxUrgency.SelectedIndex; }
            set { comboBoxUrgency.SelectedIndex = value; }
        }

        public string textBox_UserName
        {
            get { return textBoxUserName.Text; }
            set { textBoxUserName.Text = value; }
        }

        public string textBox_Password
        {
            get { return textBoxPassword.Text; }
            set { textBoxPassword.Text = value; }
        }

        public string textBox_Address
        {
            get { return textBoxServer.Text; }
            set { textBoxServer.Text = value; }
        }

        public string textBox_Port
        {
            get { return textBoxPort.Text; }
            set { textBoxPort.Text = value; }
        }

        public bool checkBox_FunctionActivated
        {
            get { return checkBoxActivation.Checked; }
            set { checkBoxActivation.Checked = value; }
        }

        public bool checkBoxAutoStartState
        {
            get { return checkBoxAutoStart.Checked; }
            set { checkBoxAutoStart.Checked = value; }
        }

        TextBox ISettingsForm.textBoxService
        {
            get { return textBoxService; }
            set { textBoxService = value; }
        }

        TextBox ISettingsForm.textBoxWorkGroup
        {
            get { return textBoxWorkGroup; }
            set { textBoxWorkGroup = value; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (GetPathButtonsClick != null)
                GetPathButtonsClick(openFileDialogSelectPath, e);
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            textBoxPath.Text = path;

            if (FormLoad != null)
                FormLoad(sender, e);
        }

        private void checkBoxAutoStart_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxAutoStartStateChange != null)
                checkBoxAutoStartStateChange(sender, e);
            
        }

        private void buttonAccept_Click(object sender, EventArgs e)
        {
            if (ButtonsClick != null)
                ButtonsClick(sender, e);
            if (((Button)sender) == buttonAccept)
                Close();
        }


        public string textBox_FirstName
        {
            get { return textBoxI.Text; }
            set { textBoxI.Text = value; }
        }

        public string textBox_LastName
        {
            get { return textBoxF.Text; }
            set { textBoxF.Text = value; }
        }

        public string textBox_MiddleName
        {
            get { return textBoxO.Text; }
            set { textBoxO.Text = value; }
        }
    }
}
