﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProtocolTester.SettingsFormNamespace
{
    public class MSettingsForm
    {
//public BMCSettings _bMCSettings { get; set; }

        public string GetPath(object sender)
        {
            string res = "";

            OpenFileDialog ofd = (OpenFileDialog)sender;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                res = ofd.FileName;

                for (int i = res.Length - 1; i > 0; i--)
                {
                    if (res[i] != '\\')
                    {
                        res = res.Substring(0, i);
                    }
                    else
                    {
                        res = res.Substring(0, i);
                        break;
                    }
                }
            }
            ofd.Dispose();
            return res;
        }
    }
}
