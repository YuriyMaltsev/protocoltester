﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using ProtocolTester.SettingsFormNamespace;
using System.Threading.Tasks;

namespace ProtocolTester.SettingsFormBMCNamespace
{
    public partial class SettingsForm : Form
    {
        PSettingsForm _pSettingsForm;
        ISettingsForm _sf;
        delegate void Delegate();

        public SettingsForm(PSettingsForm pSettingsForm, ISettingsForm sf)
        {
            InitializeComponent();
            _pSettingsForm = pSettingsForm;
            _sf = sf;
        }


        private void button5_Click(object sender, EventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BMCSettings));
            using (var fStream = new FileStream("BMCData.xml", FileMode.Create, FileAccess.Write, FileShare.None))
            {
                serializer.Serialize(fStream, _pSettingsForm._presenter._Model._bMCSettings);
            }
            
            button5.Enabled = false;
        }

        private void listView3_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            string hash = e.Item.Text;
            bool state = e.Item.Checked;

            IEnumerable<ListViewItem> lv = ((ListView)sender).Items.Cast<ListViewItem>();
            var items = from item in lv where item.Checked select item;
            var itemsArr = items.ToArray();

            

            ((ListView)sender).ItemChecked -= listView3_ItemChecked;
            if (((ListView)sender).Name == "listView1")
            {
                for (int i = 0; i < itemsArr.Length; i++)
                {
                    itemsArr[i].Checked = false;
                    _pSettingsForm._presenter._Model._bMCSettings.ServiceTable[itemsArr[i].Text].StateSelect = false;
                }

                if (hash != "")
                    if (_pSettingsForm._presenter._Model._bMCSettings.ServiceTable.ContainsKey(hash))
                    {
                        _pSettingsForm._presenter._Model._bMCSettings.ServiceTable[hash].StateSelect = state;

                        if (state)
                        {
                            _sf.textBoxService.Text = _pSettingsForm._presenter._Model._bMCSettings.ServiceTable[hash].ServiceName;
                            _sf.textBoxService.Tag = _pSettingsForm._presenter._Model._bMCSettings.ServiceTable[hash];
                        }
                    }
            }
            else
            {
                if (((ListView)sender).Name == "listView2")
                {
                    for (int i = 0; i < itemsArr.Length; i++)
                    {
                        itemsArr[i].Checked = false;
                        _pSettingsForm._presenter._Model._bMCSettings.WorkGroupTable[itemsArr[i].Text].StateSelect = false;
                    }

                    if (hash != "")
                        if (_pSettingsForm._presenter._Model._bMCSettings.WorkGroupTable.ContainsKey(hash))
                        {
                            _pSettingsForm._presenter._Model._bMCSettings.WorkGroupTable[hash].StateSelect = state;

                            if (state)
                            {
                                _sf.textBoxWorkGroup.Text = _pSettingsForm._presenter._Model._bMCSettings.WorkGroupTable[hash].WorkGroupName;
                                _sf.textBoxWorkGroup.Tag = _pSettingsForm._presenter._Model._bMCSettings.WorkGroupTable[hash];
                            }
                        }
                }
            }

            ((ListView)sender).Items[e.Item.Index].Checked = state;
            button5.Enabled = true;

            ((ListView)sender).ItemChecked += listView3_ItemChecked;
        }

        private void listView3_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ((ListView)sender).ListViewItemSorter = new ListViewItemComparer(e.Column);
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            if (_pSettingsForm._presenter._Model._bMCSettings.ServiceTable.Count == 0 & _pSettingsForm._presenter._Model._bMCSettings.WorkGroupTable.Count == 0)
            {
                if (_pSettingsForm._presenter._Model._bMCSettings.UserName != "" | _pSettingsForm._presenter._Model._bMCSettings.Password != "")
                {
                    _pSettingsForm._presenter._Model._bMCSettings.BMCServer = _pSettingsForm._presenter._Model._bMCSettings.BMCServer ?? new BMC.ARSystem.Server();

                    pictureBox1.Visible = true;
                    pictureBox2.Visible = true;
                    Delegate del = new Delegate(Autorization);
                    Task newTask = Task.Factory.StartNew(() => del.BeginInvoke(new AsyncCallback(AutorizationCallBack), null));
                }
                else
                    MessageBox.Show("Имя или пароль указаны не верно.");
            }
            else
            {
                ShowWorkGroup();

                pictureBox1.Visible = false;
                pictureBox2.Visible = false;
            }

            listView2.ItemChecked += listView3_ItemChecked;
            listView1.ItemChecked += listView3_ItemChecked;
        }

        private void AutorizationCallBack(IAsyncResult aRes)
        {
            if (pictureBox2.InvokeRequired)
                pictureBox2.Invoke(new System.Action(() =>
                {
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = false;
                }));

            
        }

        private void Autorization()
        {
            try
            {
                _pSettingsForm._presenter._Model._bMCSettings.BMCServer.Login(_pSettingsForm._presenter._Model._bMCSettings.ServerAddress,
                                                                                        _pSettingsForm._presenter._Model._bMCSettings.UserName,
                                                                                        _pSettingsForm._presenter._Model._bMCSettings.Password,
                                                                                        "",
                                                                                        _pSettingsForm._presenter._Model._bMCSettings.Port);

                string sql = "1 = 1";
                int[] entryFields = { 400129200, 200000020, 200000003, 200000004, 200000005 };
                BMC.ARSystem.EntryFieldValueList entryWithFields = _pSettingsForm._presenter._Model._bMCSettings.BMCServer.GetListEntryWithFields("AST:BusinessService", sql, entryFields, Convert.ToUInt32(0), Convert.ToUInt32(1000));

                foreach (BMC.ARSystem.EntryFieldValue entry in entryWithFields)
                {
                    if (entry.FieldValues[400129200].ToString() != "0")
                        if (!_pSettingsForm._presenter._Model._bMCSettings.ServiceTable.ContainsKey(entry.FieldValues[400129200].ToString()))
                            _pSettingsForm._presenter._Model._bMCSettings.ServiceTable.Add(entry.FieldValues[400129200].ToString(), new ServiceRows(entry.FieldValues[200000020].ToString(), entry.FieldValues[200000003].ToString(), entry.FieldValues[200000004].ToString(), entry.FieldValues[200000005].ToString(), entry.FieldValues[400129200].ToString(), false));
                }

                entryFields = new int[] { 1000000015, 1000000014, 1000000001, 1 };
                entryWithFields = _pSettingsForm._presenter._Model._bMCSettings.BMCServer.GetListEntryWithFields("CTM:Support Group", sql, entryFields, Convert.ToUInt32(0), Convert.ToUInt32(1000));

                foreach (BMC.ARSystem.EntryFieldValue entry in entryWithFields)
                {
                    _pSettingsForm._presenter._Model._bMCSettings.WorkGroupTable.Add(entry.FieldValues[1].ToString(), new WorkGroupRows(entry.FieldValues[1000000015].ToString(), entry.FieldValues[1000000014].ToString(), entry.FieldValues[1000000001].ToString(), entry.FieldValues[1].ToString(), false));
                }

                _pSettingsForm._presenter._Model._bMCSettings.BMCServer.Logout();
                _pSettingsForm._presenter._Model._bMCSettings.BMCServer = null;

                listView1.Invoke(new Action(() =>
                {
                    ShowWorkGroup();
                }));
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError("Autorization: " + ex.Message, Model.GetProgrammDir() + "\\Logs\\BMC.txt");
            }
        }

        public void ShowWorkGroup()
        {
            listView1.Clear();
            listView1.Columns.Add(new ColumnHeader());
            listView1.Columns[0].Text = "Сервис ID";
            listView1.Columns[0].Width = 100;
            listView1.Columns.Add(new ColumnHeader());
            listView1.Columns[1].Text = "200000003";
            listView1.Columns[1].Width = 100;
            listView1.Columns.Add(new ColumnHeader());
            listView1.Columns[2].Text = "200000004";
            listView1.Columns[2].Width = 100;
            listView1.Columns.Add(new ColumnHeader());
            listView1.Columns[3].Text = "200000005";
            listView1.Columns[3].Width = 100;
            listView1.Columns.Add(new ColumnHeader());
            listView1.Columns[4].Text = "Имя Сервиса";
            listView1.Columns[4].Width = 100;

            listView2.Clear();
            listView2.Columns.Add(new ColumnHeader());
            listView2.Columns[0].Text = "РГ ID";
            listView2.Columns[0].Width = 100;
            listView2.Columns.Add(new ColumnHeader());
            listView2.Columns[1].Text = "1000000014";
            listView2.Columns[1].Width = 100;
            listView2.Columns.Add(new ColumnHeader());
            listView2.Columns[2].Text = "1000000001";
            listView2.Columns[2].Width = 100;
            listView2.Columns.Add(new ColumnHeader());
            listView2.Columns[3].Text = "Имя РГ";
            listView2.Columns[3].Width = 100;


            label7.Text = "Service (" + _pSettingsForm._presenter._Model._bMCSettings.ServiceTable.Count.ToString() + "): ";
            foreach (var ff in _pSettingsForm._presenter._Model._bMCSettings.ServiceTable)
            {
                ListViewItem lvItem = new ListViewItem();
                lvItem.Text = ((ServiceRows)ff.Value).ServiceID;
                lvItem.SubItems.Add(((ServiceRows)ff.Value).Tier1);
                lvItem.SubItems.Add(((ServiceRows)ff.Value).Tier2);
                lvItem.SubItems.Add(((ServiceRows)ff.Value).Tier3);
                lvItem.SubItems.Add(((ServiceRows)ff.Value).ServiceName);
                if (((ServiceRows)ff.Value).StateSelect)
                    lvItem.Checked = true;
                listView1.Items.Add(lvItem);
            }

            label8.Text = "Work Group (" + _pSettingsForm._presenter._Model._bMCSettings.WorkGroupTable.Count.ToString() + "): ";
            foreach (var ff in _pSettingsForm._presenter._Model._bMCSettings.WorkGroupTable)
            {
                ListViewItem lvItem = new ListViewItem();
                lvItem.Text = ((WorkGroupRows)ff.Value).WorkGroupID;
                lvItem.SubItems.Add(((WorkGroupRows)ff.Value).SupportOrganization);
                lvItem.SubItems.Add(((WorkGroupRows)ff.Value).CompanyName);
                lvItem.SubItems.Add(((WorkGroupRows)ff.Value).WorkGroupName);
                if (((WorkGroupRows)ff.Value).StateSelect)
                    lvItem.Checked = true;
                listView2.Items.Add(lvItem);
            }
        }

        private void SettingsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            button5.PerformClick();
            Close();
        }
    }

    //Класс на основе которого реализованна сортировка в ListView
    class ListViewItemComparer : System.Collections.IComparer
    {
        private int col;
        public ListViewItemComparer()
        {
            col = 0;
        }
        public ListViewItemComparer(int column)
        {
            col = column;
        }
        public int Compare(object x, object y)
        {
            return String.Compare(((System.Windows.Forms.ListViewItem)x).SubItems[col].Text, ((System.Windows.Forms.ListViewItem)y).SubItems[col].Text);
        }
    }

}
