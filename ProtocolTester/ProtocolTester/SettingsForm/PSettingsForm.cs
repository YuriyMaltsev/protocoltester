﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using ProtocolTester.SettingsFormBMCNamespace;
using System.Security.Cryptography;

namespace ProtocolTester.SettingsFormNamespace
{
    public class PSettingsForm
    {
        public Presenter _presenter;
        ISettingsForm _settingsForm;
        public MSettingsForm mSettingsForm;

        Settings _settings;

        public PSettingsForm(Settings settings, ISettingsForm settingsForm, Presenter presenter)
        {
            _settingsForm = settingsForm;
            _presenter = presenter;
            _settings = settings;
            mSettingsForm = new MSettingsForm();
            _settingsForm.GetPathButtonsClick += new EventHandler<EventArgs>(_settingsForm_GetPathButtonsClick);
            _settingsForm.checkBoxAutoStartStateChange += _settingsForm_checkBoxAutoStartStateChange;
            _settingsForm.ButtonsClick += _settingsForm_ButtonsClick;
            _settingsForm.FormLoad += _settingsForm_FormLoad;
        }

        private void _settingsForm_ButtonsClick(object sender, EventArgs e)
        {
            if (((Button)sender).Name == "buttonSelectionFromBMC")
            {
                SettingsFormBMCNamespace.SettingsForm sf = new SettingsFormBMCNamespace.SettingsForm(this, _settingsForm);
                sf.ShowDialog();
            }
            else
            {

                if (((Button)sender).Name == "buttonAccept")
                {
                    if (_presenter._Model._bMCSettings == null)
                        _presenter._Model._bMCSettings = new BMCSettings();

                    _presenter._Model._bMCSettings.FunctionActivated = _settingsForm.checkBox_FunctionActivated;
                    _presenter._Model._bMCSettings.Impact = _settingsForm.textBox_Impact;
                    _presenter._Model._bMCSettings.ObjectName = _settingsForm.textBox_Object;
                    _presenter._Model._bMCSettings.Password = _settingsForm.textBox_Password;
                    _presenter._Model._bMCSettings.Port = _settingsForm.textBox_Port != "" ? int.Parse(_settingsForm.textBox_Port) : 0;
                    _presenter._Model._bMCSettings.ServerAddress = _settingsForm.textBox_Address;
                    _presenter._Model._bMCSettings.Urgency = _settingsForm.textBox_Urgency;
                    _presenter._Model._bMCSettings.UserName = _settingsForm.textBox_UserName;
                    _presenter._Model._bMCSettings.WorkGroup = _settingsForm.textBox_WorkGroup;
                    _presenter._Model._bMCSettings.Service = _settingsForm.textBox_Service;
                    _presenter._Model._bMCSettings.ShortDescription = _settingsForm.textBox_ShortDescription;
                    _presenter._Model._bMCSettings.Timeout = int.Parse(_settingsForm.textBox_Timeout);
                    _presenter._Model._bMCSettings.FirstName = _settingsForm.textBox_FirstName;
                    _presenter._Model._bMCSettings.LastName = _settingsForm.textBox_LastName;
                    _presenter._Model._bMCSettings.MiddleName = _settingsForm.textBox_MiddleName;

                    XmlSerializer xml = new XmlSerializer(typeof(BMCSettings));
                    using (var fStream = new FileStream("BMCData.xml", FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        xml.Serialize(fStream, _presenter._Model._bMCSettings);
                    }

                    String plainText = _settingsForm.textBox_Password;
                    String entropyText = Environment.UserName;
                    byte[] plainBytes = Encoding.Unicode.GetBytes(plainText);
                    byte[] entropyBytes = Encoding.Unicode.GetBytes(entropyText);
                    byte[] encryptedBytes = ProtectedData.Protect(plainBytes, entropyBytes, DataProtectionScope.CurrentUser);
                    WriteBytesToFile(encryptedBytes);
                }
            }
        }

        static void WriteBytesToFile(byte[] bytes)
        {
            string path = "ProtocolTester.dat";

            // Delete the file if it exists
            if (File.Exists(path))
                File.Delete(path);
            
            using (FileStream fs = File.Create(path))
            {
                fs.Write(bytes, 0, bytes.Length);
            }
        }

        private void _settingsForm_FormLoad(object sender, EventArgs e)
        {
            _settingsForm.checkBoxAutoStartState = _settings.AutoStartExchange;

            if (_presenter._Model._bMCSettings != null)
            {
                _settingsForm.checkBox_FunctionActivated = _presenter._Model._bMCSettings.FunctionActivated;
                _settingsForm.textBox_Address = _presenter._Model._bMCSettings.ServerAddress;
                _settingsForm.textBox_Impact = _presenter._Model._bMCSettings.Impact;
                _settingsForm.textBox_Object = _presenter._Model._bMCSettings.ObjectName;
                _settingsForm.textBox_Password = _presenter._Model._bMCSettings.Password;
                _settingsForm.textBox_Port = _presenter._Model._bMCSettings.Port.ToString();
                _settingsForm.textBox_Service = _presenter._Model._bMCSettings.Service;
                _settingsForm.textBox_ShortDescription = _presenter._Model._bMCSettings.ShortDescription;
                _settingsForm.textBox_Timeout = _presenter._Model._bMCSettings.Timeout.ToString();
                _settingsForm.textBox_Urgency = _presenter._Model._bMCSettings.Urgency;
                _settingsForm.textBox_UserName = _presenter._Model._bMCSettings.UserName;
                _settingsForm.textBox_WorkGroup = _presenter._Model._bMCSettings.WorkGroup;
                _settingsForm.textBox_FirstName = _presenter._Model._bMCSettings.FirstName;
                _settingsForm.textBox_LastName = _presenter._Model._bMCSettings.LastName;
                _settingsForm.textBox_MiddleName = _presenter._Model._bMCSettings.MiddleName;

                var val =_presenter._Model._bMCSettings.ServiceTable.Select(v => v.Value).Where(v2=>v2.StateSelect).ToArray();

                if (val.Length > 0)
                {
                    _settingsForm.textBoxService.Text = val[0].ServiceName;
                    _settingsForm.textBoxService.Tag = val[0];
                }

                var val2 = _presenter._Model._bMCSettings.WorkGroupTable.Select(v => v.Value).Where(v2 => v2.StateSelect).ToArray();

                if (val2.Length > 0)
                {
                    _settingsForm.textBoxWorkGroup.Text = val2[0].WorkGroupName;
                    _settingsForm.textBoxWorkGroup.Tag = val2[0];
                }
            }
        }

        private void _settingsForm_checkBoxAutoStartStateChange(object sender, EventArgs e)
        {
            _settings.AutoStartExchange = ((CheckBox)sender).CheckState == CheckState.Checked ? true : false;
        }

        void _settingsForm_GetPathButtonsClick(object sender, EventArgs e)
        {
            _settings.SettingsPath = mSettingsForm.GetPath(sender);
            _settingsForm.TextBoxPath = _settings.SettingsPath;
        }
    }
}
