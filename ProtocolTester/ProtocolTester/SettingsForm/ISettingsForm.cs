﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProtocolTester.SettingsFormNamespace
{
    public interface ISettingsForm
    {
        //СВОЙСТВА//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        string TextBoxPath { set; }
        bool checkBoxAutoStartState { get; set; }

        string textBox_Object { get; set; }
        string textBox_ShortDescription { get; set; }
        string textBox_Service { get; set; }
        string textBox_WorkGroup { get; set; }
        string textBox_Timeout { get; set; }
        string textBox_FirstName { get; set; }
        string textBox_LastName { get; set; }
        string textBox_MiddleName { get; set; }
        int textBox_Impact { get; set; }
        int textBox_Urgency { get; set; }
        string textBox_UserName { get; set; }
        string textBox_Password { get; set; }
        string textBox_Address { get; set; }
        string textBox_Port { get; set; }
        bool checkBox_FunctionActivated { get; set; }
        TextBox textBoxService { get; set; }
        TextBox textBoxWorkGroup { get; set; }

        //СОБЫТИЯ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event EventHandler<EventArgs> GetPathButtonsClick;
        event EventHandler<EventArgs> checkBoxAutoStartStateChange;
        event EventHandler<EventArgs> FormLoad;
        event EventHandler<EventArgs> ButtonsClick;

        //МЕТОДЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    }
}
