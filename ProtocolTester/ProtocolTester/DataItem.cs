﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProtocolTester
{
    [Serializable]
    public class DataItem
    {
        public string Com { get; set; }
        public int Variable { get; set; }
        public string Value { get; set; }
        public string TimeStamp { get; set; }
        public string OldValue { get; set; }
        public string OldTimeStamp { get; set; }
        public string Quality { get; set; }
        public int Function { get; set; }
        public DataType DataType_ { get; set; }
        public int Group { get; set; }
        public bool Arhiving { get; set; }
        public bool OpcTag { get; set; }
        public string OpcTagName { get; set; }
        public bool OPCAccess { get; set; }
        public bool NeedSend { get; set; }

        public DataItem(string portName, int variable, DataType dataType, int function)
        {
            Com = portName;
            Variable = variable;
            DataType_ = dataType;
            Function = function;
        }

        public DataItem()
        {

        }

        public enum DataType
        {
            INT, REAL, BOOL
        }
    }
}
