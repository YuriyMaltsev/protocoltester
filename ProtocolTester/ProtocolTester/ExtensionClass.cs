﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace ProtocolTester
{
    public static class ExtensionClass
    {
        /// <summary>  
        /// метод расширения парсит строку 
        /// </summary>  
        public static Parity GetParity(this string strParity)
        {
            switch (strParity)
            {
                case "Even":
                    return Parity.Even;
                case "Mark":
                    return Parity.Mark;
                case "None":
                    return Parity.None;
                case "Odd":
                    return Parity.Odd;
                case "Space":
                    return Parity.Space;
                default:
                    return Parity.None;
            }
        }

        public static StopBits GetStopBits(this string strStopBits)
        {
            switch (strStopBits)
            {
                case "None":
                    return StopBits.None;
                case "One":
                    return StopBits.One;
                case "OnePointFive":
                    return StopBits.OnePointFive;
                case "Two":
                    return StopBits.Two;
                default:
                    return StopBits.None;
            }
        }
    }
}
