﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Drawing;

namespace ProtocolTester
{
    interface IScanForm
    {
        //СВОЙСТВА//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        List<int> DataBits { get; }
        List<Parity> _Parity { get; }
        List<StopBits> _StopBits { get; }
        List<int> Speed { get; }
        List<int> Address { get; }
        string PortName { get; }
        int ProgressBar1Value { get; set; }
        int ProgressBar1Max { get; set; }
        decimal numericUpDownAddressEndMaximum { get; set; }
        Color buttonStartScanColor { get; set; }
        string ProgressBar1Text { set; }
        int WriteReadTimeout { get; set; }
        decimal ScanFunction { get; }

        //СОБЫТИЯ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event EventHandler<EventArgs> ViewButtonsClick;//событие клика по кнопкам формы
        event EventHandler<EventArgs> WriteReadTimeoutChange;//событие изменения таймаута

        //МЕТОДЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void ChangeMode(Model.Mode mode);//Смена режима
        void ShowForm();//Метод запуска формы
        int GetStatus();//Метод проверяет необходимые настройки и дает добро на запуск сканирования
        void AddNewItemInListViewScanResult(string[] item);
        void RemoveAllFromListViewScanResult();
        void listViewNowScanUpdate(string[] item);
        void ListBoxLogInsertLine(string log);
    }
}
