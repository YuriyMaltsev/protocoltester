﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Diagnostics;
using System.Threading;
using System.Collections;

namespace ProtocolTester
{
    class MScanForm
    {
        ConnectionLine485 connectionLine485 = new ConnectionLine485();

        public event EventHandler<EventArgs> AddNewItemInListView;
        public event EventHandler<EventArgs> UpdateProgressBarValue;
        public event EventHandler<EventArgs> UpdateProgressBarMaximum;
        public event EventHandler<EventArgs> UpdateProgressBarText;
        public event EventHandler<EventArgs> UpdateListViewNowScan;
        public event EventHandler<EventArgs> InsertLog;
        

        List<string[]> scanResult;

        Hashtable scanResultHash;

        bool autoModeIndicator;

        private List<int> _dataBits;
        private List<int> _speeds;
        private List<Parity> _paritys;
        private List<StopBits> _stopBits;
        private List<int> _addressList;
        private string portName;
        decimal _scanFunction;

        public int WriteReadTimeout
        {
            get;
            set;
        }

        public Hashtable ScanResultHash
        {
            get { return scanResultHash; }
        }

        public ConnectionLine485 _ConnectionLine485
        {
            get { return connectionLine485; }
            set { connectionLine485 = value; }
        }

        public string PortName
        {
            get { return portName; }
            set { portName = value; }
        }

        public bool AutoModeIndicator
        {
            get { return autoModeIndicator; }
            set { autoModeIndicator = value; }
        }

        public List<int> DataBits
        {
            get { return _dataBits; }
        }

        public List<int> Speeds
        {
            get { return _speeds; }
        }

        public List<Parity> Paritys
        {
            get { return _paritys; }
        }

        public List<StopBits> _StopBits
        {
            get { return _stopBits; }
        }

        public List<int> _AddressList
        {
            get { return _addressList; }
            set { _addressList = value; }
        }

        public MScanForm()
        {

        }

        public void MScanFormInit(List<int> dataBits, List<int> speeds, List<Parity> paritys, List<StopBits> stopBits, List<int> addressList, string portName, decimal scanFunction = 0)
        {
            _dataBits = dataBits;
            _speeds = speeds;
            _paritys = paritys;
            _stopBits = stopBits;
            _addressList = addressList;
            this.portName = portName;
            _scanFunction = scanFunction;
        }


        public void StartScan(object sender)
        {
            string status = "";

               
            
            connectionLine485.Port.DataReceived += new SerialDataReceivedEventHandler(sp_DataReceived);
                scanResult = new List<string[]>();
                scanResultHash = new Hashtable();

            

            int progressBarMaximum = (_addressList.Count * _dataBits.Count * _stopBits.Count * _paritys.Count * _speeds.Count);
            int iterationCount = 0;

            if (UpdateProgressBarMaximum != null)
                UpdateProgressBarMaximum(progressBarMaximum, EventArgs.Empty);

            

            for (int i1 = 0; i1 < _addressList.Count; i1++)//перебор устройств
            {
                for (int i2 = 0; i2 < _dataBits.Count; i2++)//
                {
                    for (int i3 = 0; i3 < _stopBits.Count; i3++)//
                    {
                        for (int i4 = 0; i4 < _paritys.Count; i4++)//
                        {
                            for (int i5 = 0; i5 < _speeds.Count; i5++)//
                            {
                                try
                                {
                                    if (!autoModeIndicator)
                                    {
                                        autoModeIndicator = false;
                                        connectionLine485.Port.DataReceived -= sp_DataReceived;
                                        if (connectionLine485.Port.IsOpen)
                                            connectionLine485.Port.Close();
                                        return;
                                    }

                                    OpenComPort(connectionLine485.Port, _dataBits[i2], _speeds[i5], _paritys[i4], _stopBits[i3], portName, _addressList[i1].ToString());

                                    if (UpdateListViewNowScan != null)
                                        UpdateListViewNowScan(new string[] { connectionLine485.Port.BaudRate.ToString(), connectionLine485.Port.DataBits.ToString(), connectionLine485.Port.Parity.ToString(), connectionLine485.Port.StopBits.ToString(), (int.Parse(connectionLine485.Port.NewLine) - (connectionLine485.Mode == Model.Mode.Carel ? 48 : 0)).ToString() }, EventArgs.Empty);


                                    if (connectionLine485.Mode == Model.Mode.Carel)
                                    {
                                        CarelUnit carelUnit = new CarelUnit(connectionLine485.Port, connectionLine485.Port.PortName, _addressList[i1]);
                                        status = carelUnit._Carel.SendRequest(connectionLine485.Port, Convert.ToByte(_addressList[i1]), false, sender);

                                        status = DateTime.Now.ToString("HH:mm:ss") + ": " + status + ". DataBits:" + connectionLine485.Port.DataBits.ToString() + ";StopBits:" + connectionLine485.Port.StopBits.ToString() +
                                            ";Parity:" + connectionLine485.Port.Parity.ToString() + ";Speed:" + connectionLine485.Port.BaudRate.ToString() + ";Address:" + (int.Parse(connectionLine485.Port.NewLine) - 48).ToString();


                                        # region  использовать если не подписываться на событие чтения из порта.
                                        /*
                                        //используется если не подписываться на событие чтения из порта.
                                        byte[] response = new byte[] { };
                                        carelUnit._Carel.GetResponse(connectionLine485.Port, ref response);

                                        if (response.Length > 2)
                                            if (response[0] == 2)//& int.Parse(((SerialPort)sender).NewLine) == response[1])
                                            {
                                                string[] temp = new string[] { connectionLine485.Port.PortName, (int.Parse(connectionLine485.Port.NewLine) - 48).ToString(), connectionLine485.Port.DataBits.ToString(), connectionLine485.Port.Parity.ToString(), connectionLine485.Port.StopBits.ToString(), connectionLine485.Port.BaudRate.ToString() };
                                                if (!scanResultHash.ContainsKey((temp[0] + temp[1] + temp[2] + temp[3] + temp[4] + temp[5]).GetHashCode()))
                                                    scanResultHash.Add((temp[0] + temp[1] + temp[2] + temp[3] + temp[4] + temp[5]).GetHashCode(), temp);

                                                if (AddNewItemInListView != null)
                                                    AddNewItemInListView(temp, EventArgs.Empty);
                                            }
                                        */
                                        # endregion
                                    }
                                    else
                                    {
                                        if (connectionLine485.Mode == Model.Mode.ModBusRTU)
                                        {
                                            ModBusRTUUnit modBusRTUUnit = new ModBusRTUUnit(connectionLine485.Port, connectionLine485.Port.PortName, _addressList[i1]);

                                            short[] values = new short[] {};

                                            switch ((int)_scanFunction)
                                            {
                                                case 1:
                                                    status = ModBusRTU.SendFc1(modBusRTUUnit, Convert.ToByte(_addressList[i1]), 0, 1, WriteReadTimeout, ref values, false);
                                                    break;
                                                case 2:
                                                    status = ModBusRTU.SendFc2(modBusRTUUnit, Convert.ToByte(_addressList[i1]), 0, 1, WriteReadTimeout, ref values, false);
                                                    break;
                                                case 3:
                                                    status = ModBusRTU.SendFc3(modBusRTUUnit, Convert.ToByte(_addressList[i1]), 0, 1, WriteReadTimeout, ref values, false);
                                                    break;

                                                case 4:
                                                    status = ModBusRTU.SendFc4(modBusRTUUnit, Convert.ToByte(_addressList[i1]), 0, 1, WriteReadTimeout, ref values, false);
                                                    break;
                                            }
                                            
                                            status = DateTime.Now.ToString("HH:mm:ss") + ": " + status + ". DataBits:" + connectionLine485.Port.DataBits.ToString() + ";StopBits:" + connectionLine485.Port.StopBits.ToString() +
                                            ";Parity:" + connectionLine485.Port.Parity.ToString() + ";Speed:" + connectionLine485.Port.BaudRate.ToString() + ";Address:" + _addressList[i1].ToString();
                                        }
                                    }

                                    if (UpdateProgressBarValue != null)
                                    {
                                        UpdateProgressBarValue(iterationCount, EventArgs.Empty);
                                        UpdateProgressBarText(scanResult.Count.ToString(), EventArgs.Empty);
                                        InsertLog(status, EventArgs.Empty);
                                    }

                                    iterationCount++;
                                    Thread.Sleep(WriteReadTimeout);
                                }
                                catch (Exception ex)
                                {
                                    InsertLog(DateTime.Now + ": " + ex.Message, EventArgs.Empty);
                                }
                            }
                        }
                    }
                }
                Thread.Sleep(WriteReadTimeout);
            }

            connectionLine485.Port.DataReceived -= sp_DataReceived;
            if (connectionLine485.Port.IsOpen)
                connectionLine485.Port.Close();

        }

        private void sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                byte[] response = new byte[((SerialPort)sender).BytesToRead];
                //GetResponseBool((SerialPort)sender,ref response);
                ((SerialPort)sender).Read(response, 0, response.Length);

                if (connectionLine485.Mode == Model.Mode.Carel)
                {
                    if (response.Length > 0)
                    {
                        if (response[0] == 2 | response[0] == 0)//& int.Parse(((SerialPort)sender).NewLine) == response[1])
                        {
                            string[] temp = new string[] { ((SerialPort)sender).PortName, (int.Parse(((SerialPort)sender).NewLine) - 48).ToString(), ((SerialPort)sender).DataBits.ToString(), ((SerialPort)sender).Parity.ToString(), ((SerialPort)sender).StopBits.ToString(), ((SerialPort)sender).BaudRate.ToString() };
                            if (!scanResultHash.ContainsKey((temp[0] + temp[1] + temp[2] + temp[3] + temp[4] + temp[5]).GetHashCode()))
                                scanResultHash.Add((temp[0] + temp[1] + temp[2] + temp[3] + temp[4] + temp[5]).GetHashCode(), temp);

                            if (AddNewItemInListView != null)
                                AddNewItemInListView(temp, EventArgs.Empty);
                        }
                    }
                }
                else
                {
                    if (connectionLine485.Mode == Model.Mode.ModBusRTU)
                    {
                        if ((response.Length > 6 & _scanFunction == 3 | _scanFunction == 4) | (response.Length > 5 & _scanFunction == 1 | _scanFunction == 2))
                        {
                            string[] temp = new string[] { ((SerialPort)sender).PortName, (response[0]).ToString(), ((SerialPort)sender).DataBits.ToString(), ((SerialPort)sender).Parity.ToString(), ((SerialPort)sender).StopBits.ToString(), ((SerialPort)sender).BaudRate.ToString() };
                            if (!scanResultHash.ContainsKey((temp[0] + temp[1] + temp[2] + temp[3] + temp[4] + temp[5]).GetHashCode()))
                                scanResultHash.Add((temp[0] + temp[1] + temp[2] + temp[3] + temp[4] + temp[5]).GetHashCode(), temp);

                            if (AddNewItemInListView != null)
                                AddNewItemInListView(temp, EventArgs.Empty);
                        }
                        
                    }
                }

                if (UpdateProgressBarValue != null)
                {
                    string status = DateTime.Now.ToString("HH:mm:ss") + ": DataBits:" + ((SerialPort)sender).DataBits.ToString() + ";StopBits:" + ((SerialPort)sender).StopBits.ToString() +
                                        ";Parity:" + ((SerialPort)sender).Parity.ToString() + ";Speed:" + ((SerialPort)sender).BaudRate.ToString() + ";Address:" + (int.Parse(((SerialPort)sender).NewLine) - 48).ToString() + ": response ";
                    foreach (byte b in response)
                        status = status + b.ToString() + "|";
                    InsertLog(status, EventArgs.Empty);
                }

                scanResult.Add(new string[] { ((SerialPort)sender).PortName, (int.Parse(((SerialPort)sender).NewLine) - 48).ToString(), ((SerialPort)sender).DataBits.ToString(), ((SerialPort)sender).Parity.ToString(), ((SerialPort)sender).StopBits.ToString(), ((SerialPort)sender).BaudRate.ToString() });
                
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError("(sp_DataReceived.1)" + ex.Message, Model.GetProgrammDir() + "\\Logs\\Scan.txt");
            }
        }

        public void OpenComPort(SerialPort port, int dataBits, int baudRate, Parity parity, StopBits stopBits, string _portName, string address)
        {
            
            if (port == null)
            {
                 port = new SerialPort(_portName);
            }

            try
            {
                if (port.PortName != _portName)
                {
                    if (port.IsOpen)
                    {
                        port.Close();
                    }

                    port.PortName = _portName;
                }

                port.BaudRate = baudRate;
                port.DataBits = dataBits;
                port.Parity = parity;
                port.StopBits = stopBits;
                port.NewLine = address;


                if (!port.IsOpen)
                    port.Open();
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError("(OpenComPort.1)" + ex.Message, Model.GetProgrammDir() + "\\Logs\\Scan.txt");
            }
        }

        public static int SetBit(int dw, int nBitNumber, int nBitValue)
        {
            //dw - целое, в котором задаем бит
            //nBitNumber - номер бита, который задаем (0..31)
            int dwMask = 1 << nBitNumber;// 0000100000....

            //nBitValue (0|1)
            if (nBitValue == 0)
            {//задаем 0
                dwMask = ~dwMask;// 1111011111....
                dw = dw & dwMask; // 0 & x = 0
            }
            else
            {//задаем 1
                dw = dw | dwMask;// 1 | x = 1
            }

            return dw;
        }

        public static int GetBit(int dw, int nBitNumber)
        {
            //dw - целое в котором узнаем бит
            //nBitNumber - номер бита, который узнаем (0..31)
            int dwMask = 1 << nBitNumber;// 0000100000....
            dw = dwMask & dw;
            if (dw != 0)
                return 1;
            return 0;
        }
    }
}
