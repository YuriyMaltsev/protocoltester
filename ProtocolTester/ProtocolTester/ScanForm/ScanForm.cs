﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Xml.Serialization;

namespace ProtocolTester
{
    public partial class ScanForm : Form, IScanForm
    {
        public event EventHandler<EventArgs> ViewButtonsClick;
        public event EventHandler<EventArgs> WriteReadTimeoutChange;

        Model.Mode _mode;
        ScanSettings scanSettings = new ScanSettings();

        public void ListBoxLogInsertLine(string log)
        {
            listBoxLog.Invoke(new Action(() => listBoxLog.Items.Insert(0, log)));
        }

        public void listViewNowScanUpdate(string[] item)
        {
            for (int i = 0; i < listViewNowScan.Items.Count; i++)
            {
                ListViewItem lvi = new ListViewItem();

                if (listViewNowScan.InvokeRequired)
                {
                    listViewNowScan.Invoke(new Action(() =>
                    {
                        lvi.Text = listViewNowScan.Items[i].Text;
                        lvi.SubItems.Add(item[i]);
                        listViewNowScan.Items[i] = lvi;
                    }));
                }
            }
        }

        public decimal ScanFunction
        {
            get { return numericUpDownScanFunction.Value; }
        }

        public int WriteReadTimeout
        {
            get
            {
                return int.Parse(textBoxWRTimeout.Text);
            }
            set
            {
                if (WriteReadTimeoutChange != null)
                    WriteReadTimeoutChange(value, EventArgs.Empty);
                textBoxWRTimeout.Text = value.ToString();
            }
        }

        public Color buttonStartScanColor
        {
            get
            {
                return buttonStartScan.BackColor;
            }
            set
            {
                buttonStartScan.BackColor = value;
            }
        }

        public decimal numericUpDownAddressEndMaximum
        {
            get
            {
                return numericUpDownAddressEnd.Maximum;
            }
            set
            {
                numericUpDownAddressEnd.Maximum = value;
            }
        }

        public int ProgressBar1Max
        {
            get
            {
                return progressBar1.Maximum;
            }
            set
            {
                progressBar1.Invoke(new Action(()=> 
                    {
                        progressBar1.Maximum = value;
                    }));
                
            }
        }

        public string ProgressBar1Text
        {
            set
            {
                progressBar1.Invoke(new Action(() =>
                {
                    progressBar1.Refresh();
                    progressBar1.CreateGraphics().DrawString("Ответов - " + value, new Font("Arial",
                        (float)10.25, FontStyle.Bold),
                        Brushes.White, new PointF(progressBar1.Width / 2 - 40, progressBar1.Height / 2 - 7));
                    
                }));
            }
        }

        public int ProgressBar1Value
        {
            get
            {
                return progressBar1.Value;
            }
            set
            {
                progressBar1.Invoke(new Action(() =>
                {
                    progressBar1.Value = value;
                }));
                
            }
        }


        public List<int> DataBits
        {
            get 
            {
                List<int> dataBits = new List<int>();
                if (checkBoxDataBits7.Checked)
                    dataBits.Add(7);
                if (checkBoxDataBits8.Checked)
                    dataBits.Add(8);
                return dataBits; 
            }
        }

        public List<Parity> _Parity
        {
            get
            {
                List<Parity> parity = new List<Parity>();
                if (checkBoxParityOdd.Checked)
                    parity.Add(Parity.Odd);
                if (checkBoxParityEven.Checked)
                    parity.Add(Parity.Even);
                if (checkBoxParityNone.Checked)
                    parity.Add(Parity.None);
                return parity;
            }
        }

        public List<StopBits> _StopBits
        {
            get
            {
                List<StopBits> stopBits = new List<StopBits>();
                if (checkBoxStopBits1.Checked)
                    stopBits.Add(StopBits.One);
                if (checkBoxStopBits2.Checked)
                    stopBits.Add(StopBits.Two);
                return stopBits;
            }
        }

        public List<int> Speed
        {
            get
            {
                List<int> speed = new List<int>();
                if (checkBoxSpeed300.Checked)
                    speed.Add(300);
                if (checkBoxSpeed600.Checked)
                    speed.Add(600);
                if (checkBoxSpeed1200.Checked)
                    speed.Add(1200);
                if (checkBoxSpeed2400.Checked)
                    speed.Add(2400);
                if (checkBoxSpeed4800.Checked)
                    speed.Add(4800);
                if (checkBoxSpeed7200.Checked)
                    speed.Add(7200);
                if (checkBoxSpeed9600.Checked)
                    speed.Add(9600);
                if (checkBoxSpeed14400.Checked)
                    speed.Add(14400);
                if (checkBoxSpeed19200.Checked)
                    speed.Add(19200);
                if (checkBoxSpeed38400.Checked)
                    speed.Add(38400);
                if (checkBoxSpeed56000.Checked)
                    speed.Add(56000);
                if (checkBoxSpeed57600.Checked)
                    speed.Add(57600);
                if (checkBoxSpeed115200.Checked)
                    speed.Add(115200);
                if (checkBoxSpeed128000.Checked)
                    speed.Add(128000);
                if (checkBoxSpeed256000.Checked)
                        speed.Add(256000);
                return speed;
            }
        }

        public List<int> Address
        {
            get
            {
                List<int> address = new List<int>();
                for (int i = (int)numericUpDownAddressStart.Value; i <= numericUpDownAddressEnd.Value; i++)
                {
                    address.Add(_mode == Model.Mode.Carel ? i + 48 : i);
                }
                return address;
            }
        }

        public string PortName
        {
            get { return comboBoxCOMPort.Text; }
        }

        public ScanForm()
        {
            InitializeComponent();
            groupBox1.MouseMove += new MouseEventHandler(groupBox1_MouseMove);
        }

        public void ShowForm()
        {
            Show();
        }

        private void buttons_Click(object sender, EventArgs e)
        {
            if (ViewButtonsClick != null)
                ViewButtonsClick(sender, e);
        }

        public void ChangeMode(Model.Mode mode)
        {
            _mode = mode;
            foreach (var control in groupBoxMode.Controls)
            {
                ((Button)control).BackColor = Color.FromName("Control");
            }

            switch (mode)
            {
                case Model.Mode.Carel:
                    buttonCarel.BackColor = Color.LightGreen;
                    numericUpDownScanFunction.Enabled = false;
                    break;
                case Model.Mode.ModBusRTU:
                    buttonModBusRTU.BackColor = Color.LightGreen;
                    numericUpDownScanFunction.Enabled = true;
                    break;
                case Model.Mode.ModBusASCII:
                    buttonModBusASCII.BackColor = Color.LightGreen;
                    numericUpDownScanFunction.Enabled = false;
                    break;
                case Model.Mode.ModBusTCP:
                    buttonModBusTCP.BackColor = Color.LightGreen;
                    numericUpDownScanFunction.Enabled = false;
                    break;
            }

            scanSettings.mode = mode.ToString();
        }


        public int GetStatus()
        {
            int status = 0;
            status = comboBoxCOMPort.Text != "" ? MScanForm.SetBit(status, 0, 0) : MScanForm.SetBit(status, 0, 1);
            status = checkBoxDataBits7.Checked | checkBoxDataBits8.Checked ? MScanForm.SetBit(status, 1, 0) : MScanForm.SetBit(status, 1, 1);
            status = checkBoxSpeed300.Checked | checkBoxSpeed600.Checked | 
                checkBoxSpeed1200.Checked | checkBoxSpeed2400.Checked |
                checkBoxSpeed4800.Checked | checkBoxSpeed7200.Checked |
                checkBoxSpeed9600.Checked | checkBoxSpeed14400.Checked |
                checkBoxSpeed19200.Checked | checkBoxSpeed38400.Checked |
                checkBoxSpeed56000.Checked | checkBoxSpeed57600.Checked |
                checkBoxSpeed115200.Checked | checkBoxSpeed128000.Checked |
                checkBoxSpeed256000.Checked ? MScanForm.SetBit(status, 2, 0) : MScanForm.SetBit(status, 2, 1);
            status = checkBoxParityNone.Checked |checkBoxParityOdd.Checked | checkBoxParityEven.Checked ? MScanForm.SetBit(status, 3, 0) : MScanForm.SetBit(status, 3, 1);
            status = checkBoxStopBits1.Checked | checkBoxStopBits2.Checked ? MScanForm.SetBit(status, 4, 0) : MScanForm.SetBit(status, 4, 1);
            status = numericUpDownAddressEnd.Value >= numericUpDownAddressStart.Value ? MScanForm.SetBit(status, 5, 0) : MScanForm.SetBit(status, 5, 1);

            if (status > 0)
            {
                string msg = "Не выбраны или заданы не верно обязательные параметры:";
                msg = MScanForm.GetBit(status, 0) == 0 ? msg : msg + "\nCOM Port.";
                msg = MScanForm.GetBit(status, 1) == 0 ? msg : msg + "\nData Bits.";
                msg = MScanForm.GetBit(status, 2) == 0 ? msg : msg + "\nSpeed.";
                msg = MScanForm.GetBit(status, 3) == 0 ? msg : msg + "\nParity.";
                msg = MScanForm.GetBit(status, 4) == 0 ? msg : msg + "\nStop Bits.";
                msg = MScanForm.GetBit(status, 5) == 0 ? msg : msg + "\nAddress.";

                System.Windows.Forms.MessageBox.Show(msg);
            }

            return status;
        }

        private void ScanForm_Load(object sender, EventArgs e)
        {
            

            var ports = SerialPort.GetPortNames();

            foreach (string port in ports)
            {
                comboBoxCOMPort.Items.Add(port);
            }

            try
            {
                

                XmlSerializer serializer = new XmlSerializer(typeof(ScanSettings));

                if (File.Exists(Model.GetProgrammDir() + "\\ScanSettings.xml"))
                {
                    StreamReader reader = new StreamReader(Model.GetProgrammDir() + "\\ScanSettings.xml");
                    scanSettings = (ScanSettings)serializer.Deserialize(reader);
                    reader.Close();

                    comboBoxCOMPort.SelectedIndex = scanSettings.ComPortComboBoxIndex;
                    checkBoxDataBits7.Checked = scanSettings.checkBoxDataBits7State;
                    checkBoxDataBits8.Checked = scanSettings.checkBoxDataBits8State;
                    checkBoxParityEven.Checked = scanSettings.checkBoxParityEven;
                    checkBoxParityNone.Checked = scanSettings.checkBoxParityNone;
                    checkBoxParityOdd.Checked = scanSettings.checkBoxParityOdd;
                    checkBoxSpeed115200.Checked = scanSettings.checkBoxSpeed115200;
                    checkBoxSpeed1200.Checked = scanSettings.checkBoxSpeed1200;
                    checkBoxSpeed128000.Checked = scanSettings.checkBoxSpeed128000;
                    checkBoxSpeed14400.Checked = scanSettings.checkBoxSpeed14400;
                    checkBoxSpeed19200.Checked = scanSettings.checkBoxSpeed19200;
                    checkBoxSpeed2400.Checked = scanSettings.checkBoxSpeed2400;
                    checkBoxSpeed256000.Checked = scanSettings.checkBoxSpeed256000;
                    checkBoxSpeed300.Checked = scanSettings.checkBoxSpeed300;
                    checkBoxSpeed38400.Checked = scanSettings.checkBoxSpeed38400;
                    checkBoxSpeed4800.Checked = scanSettings.checkBoxSpeed4800;
                    checkBoxSpeed56000.Checked = scanSettings.checkBoxSpeed56000;
                    checkBoxSpeed57600.Checked = scanSettings.checkBoxSpeed57600;
                    checkBoxSpeed600.Checked = scanSettings.checkBoxSpeed600;
                    checkBoxSpeed7200.Checked = scanSettings.checkBoxSpeed7200;
                    checkBoxSpeed9600.Checked = scanSettings.checkBoxSpeed9600;
                    checkBoxStopBits1.Checked = scanSettings.checkBoxStopBits1;
                    checkBoxStopBits2.Checked = scanSettings.checkBoxStopBits2;
                    numericUpDownAddressEnd.Value = scanSettings.numericUpDownAddressEnd;
                    numericUpDownAddressStart.Value = scanSettings.numericUpDownAddressStart;
                    numericUpDownScanFunction.Value = scanSettings.numericUpDownScanFunction;
                    WriteReadTimeout = int.Parse(scanSettings.WRTimeout);
                    

                    switch (scanSettings.mode)
                    {
                        case "ModBusRTU":
                            _mode = Model.Mode.ModBusRTU;
                            buttonModBusRTU.PerformClick();
                            break;
                        case "Carel":
                            _mode = Model.Mode.Carel;
                            buttonCarel.PerformClick();
                            break;
                        case "ModBusASCII":
                            _mode = Model.Mode.ModBusASCII;
                            buttonModBusASCII.PerformClick();
                            break;
                        case "ModBusTCP":
                            _mode = Model.Mode.ModBusTCP;
                            buttonModBusTCP.PerformClick();
                            break;

                    }

                }
            }
            catch
            {

            }
        }

        

        private void ScanForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            scanSettings.ComPortComboBoxIndex = comboBoxCOMPort.SelectedIndex;
            scanSettings.checkBoxDataBits7State = checkBoxDataBits7.Checked;
            scanSettings.checkBoxDataBits8State = checkBoxDataBits8.Checked;
            scanSettings.checkBoxParityEven = checkBoxParityEven.Checked;
            scanSettings.checkBoxParityNone = checkBoxParityNone.Checked;
            scanSettings.checkBoxParityOdd = checkBoxParityOdd.Checked;
            scanSettings.checkBoxSpeed115200 = checkBoxSpeed115200.Checked;
            scanSettings.checkBoxSpeed1200 = checkBoxSpeed1200.Checked;
            scanSettings.checkBoxSpeed128000 = checkBoxSpeed128000.Checked;
            scanSettings.checkBoxSpeed14400 = checkBoxSpeed14400.Checked;
            scanSettings.checkBoxSpeed19200 = checkBoxSpeed19200.Checked;
            scanSettings.checkBoxSpeed2400 = checkBoxSpeed2400.Checked;
            scanSettings.checkBoxSpeed256000 = checkBoxSpeed256000.Checked;
            scanSettings.checkBoxSpeed300 = checkBoxSpeed300.Checked;
            scanSettings.checkBoxSpeed38400 = checkBoxSpeed38400.Checked;
            scanSettings.checkBoxSpeed4800 = checkBoxSpeed4800.Checked;
            scanSettings.checkBoxSpeed56000 = checkBoxSpeed56000.Checked;
            scanSettings.checkBoxSpeed57600 = checkBoxSpeed57600.Checked;
            scanSettings.checkBoxSpeed600 = checkBoxSpeed600.Checked;
            scanSettings.checkBoxSpeed7200 = checkBoxSpeed7200.Checked;
            scanSettings.checkBoxSpeed9600 = checkBoxSpeed9600.Checked;
            scanSettings.checkBoxStopBits1 = checkBoxStopBits1.Checked;
            scanSettings.checkBoxStopBits2 = checkBoxStopBits2.Checked;
            scanSettings.numericUpDownAddressEnd = numericUpDownAddressEnd.Value;
            scanSettings.numericUpDownAddressStart = numericUpDownAddressStart.Value;
            scanSettings.numericUpDownScanFunction = numericUpDownScanFunction.Value;
            scanSettings.WRTimeout = textBoxWRTimeout.Text;
            

            try
            {
                XmlSerializer xml = new XmlSerializer(typeof(ScanSettings));
                using (var fStream = new FileStream(Model.GetProgrammDir() + "\\ScanSettings.xml", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    xml.Serialize(fStream, scanSettings);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ошибка при сохранении конфигурации.\n" + ex.Message);
            }
        }



        public void AddNewItemInListViewScanResult(string[] item)
        {
            bool flag = true;

            //Проверка на дубли
            if (listViewScanResult.InvokeRequired)
                listViewScanResult.Invoke(new Action(() =>
                    {
                        foreach (var v in listViewScanResult.Items)
                        {
                            if (((ListViewItem)v).SubItems[0].Text == item[0] &
                                ((ListViewItem)v).SubItems[1].Text == item[1] &
                                ((ListViewItem)v).SubItems[2].Text == item[2] &
                                ((ListViewItem)v).SubItems[3].Text == item[3] &
                                ((ListViewItem)v).SubItems[4].Text == item[4] &
                                ((ListViewItem)v).SubItems[5].Text == item[5])
                            {
                                flag = false;
                                break;
                            }
                        }
                    }));
            else
            {
                foreach (var v in listViewScanResult.Items)
                {
                    if (((ListViewItem)v).SubItems[0].Text == item[0] &
                        ((ListViewItem)v).SubItems[1].Text == item[1] &
                        ((ListViewItem)v).SubItems[2].Text == item[2] &
                        ((ListViewItem)v).SubItems[3].Text == item[3] &
                        ((ListViewItem)v).SubItems[4].Text == item[4] &
                        ((ListViewItem)v).SubItems[5].Text == item[5])
                    {
                        flag = false;
                        break;
                    }
                }
            }

            if (flag)
                if (listViewScanResult.InvokeRequired)
                    listViewScanResult.Invoke(new Action(() => listViewScanResult.Items.Add(new ListViewItem(item))));
                else
                    listViewScanResult.Items.Add(new ListViewItem(item));
        }

        public void RemoveAllFromListViewScanResult()
        {
            if (listViewScanResult.InvokeRequired)
                listViewScanResult.Invoke(new Action(() => listViewScanResult.Items.Clear()));
            else
                listViewScanResult.Items.Clear();
        }

        private void textBoxWRTimeout_Leave(object sender, EventArgs e)
        {
            WriteReadTimeout = int.Parse(textBoxWRTimeout.Text);
        }

        private void очиститьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ToolStripDropDownItem)sender).Text == "Очистить лог")
                listBoxLog.Items.Clear();
            else
                if (((ToolStripDropDownItem)sender).Text == "Очистить список")
                    listViewScanResult.Items.Clear();
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            if (((ContextMenuStrip)sender).SourceControl == listBoxLog)
            {
                contextMenuStrip1.Items[0].Text = "Очистить лог";
            }
            else
                if (((ContextMenuStrip)sender).SourceControl ==  listViewScanResult)
                {
                    contextMenuStrip1.Items[0].Text = "Очистить список";
                }
        }

        private void listViewScanResult_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetData(typeof(ListView)) == null)
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void listViewScanResult_DragDrop(object sender, DragEventArgs e)
        {
            List<string[]> selected = GetSelection(DataBits, Speed, _Parity, _StopBits, Address, comboBoxCOMPort.Text);

            foreach (string[] line in selected)
            {
                bool flag = true;

                foreach (var v in listViewScanResult.Items)
                {
                    if (((ListViewItem)v).SubItems[0].Text == line[0] &
                        ((ListViewItem)v).SubItems[1].Text == line[1] &
                        ((ListViewItem)v).SubItems[2].Text == line[2] &
                        ((ListViewItem)v).SubItems[3].Text == line[3] &
                        ((ListViewItem)v).SubItems[4].Text == line[4] &
                        ((ListViewItem)v).SubItems[5].Text == line[5])
                    {
                        flag = false;
                        break;
                    }
                }

                if (flag)
                    listViewScanResult.Items.Add(new ListViewItem(line));
            }
        }

        void groupBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                ((GroupBox)sender).DoDragDrop(GetSelection(DataBits, Speed, _Parity, _StopBits, Address, comboBoxCOMPort.Text), DragDropEffects.All);
            }
        }

        private List<string[]> GetSelection(List<int> dataBits, List<int> speeds, List<Parity> paritys, List<StopBits> stopBits, List<int> addressList, string portName)
        {
            List<string[]> resList = new List<string[]>();

            for (int i = 0; i < addressList.Count; i++)
            {
                for (int i2 = 0; i2 < dataBits.Count; i2++)//
                {
                    for (int i3 = 0; i3 < stopBits.Count; i3++)//
                    {
                        for (int i4 = 0; i4 < paritys.Count; i4++)//
                        {
                            for (int i5 = 0; i5 < speeds.Count; i5++)//
                            {
                                resList.Add(new string[] { PortName, (_mode == Model.Mode.Carel ? addressList[i] - 48 : addressList[i]).ToString(), dataBits[i2].ToString(), paritys[i4].ToString(), stopBits[i3].ToString(), speeds[i5].ToString(), _mode.ToString(), "0" });
                            }
                        }
                    }
                }
            }

            return resList;
        }

        private void listViewScanResult_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                List<string[]> array = new List<string[]>();
                foreach (ListViewItem item in ((ListView)sender).SelectedItems)
                {
                    string[] tempArr = new string[8];
                    int index = 0;
                    foreach (var sitem in item.SubItems)
                    {
                        tempArr[index] = ((ListViewItem.ListViewSubItem)sitem).Text;
                        index++;
                    }
                    tempArr[6] = _mode.ToString();
                    tempArr[7] = "0";

                    array.Add(tempArr);
                }

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    ((ListView)sender).DoDragDrop(array, DragDropEffects.All);
                }
            }
        }


    }
}
