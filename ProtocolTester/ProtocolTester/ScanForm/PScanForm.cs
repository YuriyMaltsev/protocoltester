﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Drawing;
using System.Collections;

namespace ProtocolTester
{
    class PScanForm
    {
        IScanForm _scanForm;
        Presenter _presenter;
        MScanForm mScanForm;
        

        public event EventHandler<EventArgs> ViewButtonsClick;

        public PScanForm(ScanForm scanForm, Presenter presenter)
        {
            _scanForm = scanForm;
            _presenter = presenter;
            mScanForm = new MScanForm();
            mScanForm._ConnectionLine485.ChangeMode += new EventHandler<ExtendedEventArgs>(_presenter_ChangeMode);
            _scanForm.ViewButtonsClick +=new EventHandler<EventArgs>(_scanForm_ViewButtonsClick);
            mScanForm.AddNewItemInListView += new EventHandler<EventArgs>(mScanForm_AddNewItemInListView);
            mScanForm.UpdateProgressBarValue += new EventHandler<EventArgs>(mScanForm_UpdateProgressBarValue);
            mScanForm.UpdateProgressBarMaximum += new EventHandler<EventArgs>(mScanForm_UpdateProgressBarMaximum);
            mScanForm.UpdateProgressBarText += new EventHandler<EventArgs>(mScanForm_UpdateProgressBarText);
            mScanForm.UpdateListViewNowScan += new EventHandler<EventArgs>(mScanForm_UpdateListViewNowScan);
            mScanForm.InsertLog += new EventHandler<EventArgs>(mScanForm_InsertLog);
            _scanForm.WriteReadTimeoutChange += new EventHandler<EventArgs>(_scanForm_WriteReadTimeoutChange);
        }

        void mScanForm_InsertLog(object sender, EventArgs e)
        {
            _scanForm.ListBoxLogInsertLine((string)sender);
        }

        void _scanForm_WriteReadTimeoutChange(object sender, EventArgs e)
        {
            mScanForm.WriteReadTimeout = (int)sender;
        }

        void mScanForm_UpdateListViewNowScan(object sender, EventArgs e)
        {
            _scanForm.listViewNowScanUpdate((string[])sender);
        }

        void mScanForm_UpdateProgressBarText(object sender, EventArgs e)
        {
            _scanForm.ProgressBar1Text = (string)sender;
        }

        void mScanForm_UpdateProgressBarMaximum(object sender, EventArgs e)
        {
            _scanForm.ProgressBar1Max = (int)sender;
        }

        void mScanForm_UpdateProgressBarValue(object sender, EventArgs e)
        {
            _scanForm.ProgressBar1Value = (int)sender;
        }

        void mScanForm_AddNewItemInListView(object sender, EventArgs e)
        {
            _scanForm.AddNewItemInListViewScanResult((string[])sender);
        }

        void _presenter_ChangeMode(object sender, ExtendedEventArgs e)
        {
            _scanForm.ChangeMode((Model.Mode)e.Control);
            
        }

        public void ShowForm()
        {
            _scanForm.ShowForm();
        }


        void _scanForm_ViewButtonsClick(object sender, EventArgs e)
        {
            if (((Button)sender).Name == "buttonModBusRTU" | ((Button)sender).Name == "buttonCarel" | ((Button)sender).Name == "buttonModBusASCII" | ((Button)sender).Name == "buttonModBusTCP")
            {
                mScanForm._ConnectionLine485.Mode = ((Button)sender).Name == "buttonModBusRTU" ? Model.Mode.ModBusRTU :
                    ((Button)sender).Name == "buttonCarel" ? Model.Mode.Carel :
                    ((Button)sender).Name == "buttonModBusASCII" ? Model.Mode.ModBusASCII :
                    ((Button)sender).Name == "buttonModBusTCP" ? Model.Mode.ModBusTCP : Model.Mode.ModBusRTU;

                _scanForm.numericUpDownAddressEndMaximum = mScanForm._ConnectionLine485.Mode == Model.Mode.Carel ? 207 : 255;
            }
            else
                if (((Button)sender).Name == "buttonStartScan")
                {
                    if (mScanForm.AutoModeIndicator == false)
                    {
                        if (_scanForm.GetStatus() == 0)
                        {
                            if (mScanForm._ConnectionLine485.Mode == Model.Mode.Carel | mScanForm._ConnectionLine485.Mode == Model.Mode.ModBusRTU)
                            {
                                mScanForm.MScanFormInit(_scanForm.DataBits, _scanForm.Speed, _scanForm._Parity, _scanForm._StopBits, _scanForm.Address, _scanForm.PortName, _scanForm.ScanFunction);
                                mScanForm.AutoModeIndicator = true;

                                _scanForm.buttonStartScanColor = Color.LightGreen;

                                Task task = Task.Factory.StartNew(() =>
                                {
                                    mScanForm.StartScan(sender);
                                    mScanForm.AutoModeIndicator = false;
                                    _scanForm.buttonStartScanColor = Color.FromName("Control");
                                    _scanForm.ProgressBar1Value = 0;
                                    mScanForm.AutoModeIndicator = false;

                                    foreach (var v in mScanForm.ScanResultHash)
                                    {
                                        mScanForm_AddNewItemInListView(((DictionaryEntry)v).Value, EventArgs.Empty);
                                    }
                                });
                            }
                        }
                    }
                    else
                    {
                        mScanForm.AutoModeIndicator = false;
                    }
                }
        }

    }
}
