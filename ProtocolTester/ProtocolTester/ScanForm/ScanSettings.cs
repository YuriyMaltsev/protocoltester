﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProtocolTester
{
    [Serializable]
    public class ScanSettings
    {
        public int ComPortComboBoxIndex { get; set; }
        public decimal numericUpDownAddressStart { get; set; }
        public decimal numericUpDownAddressEnd { get; set; }
        public decimal numericUpDownScanFunction { get; set; }
        public bool checkBoxDataBits7State { get; set; }
        public bool checkBoxDataBits8State { get; set; }
        public bool checkBoxParityNone { get; set; }
        public bool checkBoxParityOdd { get; set; }
        public bool checkBoxParityEven { get; set; }
        public bool checkBoxStopBits1 { get; set; }
        public bool checkBoxStopBits2 { get; set; }
        public bool checkBoxSpeed300 { get; set; }
        public bool checkBoxSpeed600 { get; set; }
        public bool checkBoxSpeed1200 { get; set; }
        public bool checkBoxSpeed2400 { get; set; }
        public bool checkBoxSpeed4800 { get; set; }
        public bool checkBoxSpeed7200 { get; set; }
        public bool checkBoxSpeed9600 { get; set; }
        public bool checkBoxSpeed14400 { get; set; }
        public bool checkBoxSpeed19200 { get; set; }
        public bool checkBoxSpeed38400 { get; set; }
        public bool checkBoxSpeed56000 { get; set; }
        public bool checkBoxSpeed57600 { get; set; }
        public bool checkBoxSpeed115200 { get; set; }
        public bool checkBoxSpeed128000 { get; set; }
        public bool checkBoxSpeed256000 { get; set; }
        public string mode { get; set; }
        public string WRTimeout { get; set; }
    }
}
