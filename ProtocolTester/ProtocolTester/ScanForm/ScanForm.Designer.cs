﻿namespace ProtocolTester
{
    partial class ScanForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Speed");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Data Bits");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("Parity");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Stop Bits");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("Address");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScanForm));
            this.groupBoxMode = new System.Windows.Forms.GroupBox();
            this.buttonModBusTCP = new System.Windows.Forms.Button();
            this.buttonModBusASCII = new System.Windows.Forms.Button();
            this.buttonCarel = new System.Windows.Forms.Button();
            this.buttonModBusRTU = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxWRTimeout = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownScanFunction = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonStartScan = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.listViewNowScan = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownAddressEnd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownAddressStart = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.checkBoxStopBits2 = new System.Windows.Forms.CheckBox();
            this.checkBoxStopBits1 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkBoxParityEven = new System.Windows.Forms.CheckBox();
            this.checkBoxParityOdd = new System.Windows.Forms.CheckBox();
            this.checkBoxParityNone = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxDataBits8 = new System.Windows.Forms.CheckBox();
            this.checkBoxDataBits7 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxSpeed256000 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed128000 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed115200 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed57600 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed56000 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed38400 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed19200 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed14400 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed9600 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed7200 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed4800 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed2400 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed1200 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed600 = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed300 = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.comboBoxCOMPort = new System.Windows.Forms.ComboBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.listViewScanResult = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.очиститьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBoxMode.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScanFunction)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAddressEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAddressStart)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxMode
            // 
            this.groupBoxMode.Controls.Add(this.buttonModBusTCP);
            this.groupBoxMode.Controls.Add(this.buttonModBusASCII);
            this.groupBoxMode.Controls.Add(this.buttonCarel);
            this.groupBoxMode.Controls.Add(this.buttonModBusRTU);
            this.groupBoxMode.Location = new System.Drawing.Point(4, 3);
            this.groupBoxMode.Name = "groupBoxMode";
            this.groupBoxMode.Size = new System.Drawing.Size(384, 58);
            this.groupBoxMode.TabIndex = 2;
            this.groupBoxMode.TabStop = false;
            this.groupBoxMode.Text = "Режим";
            // 
            // buttonModBusTCP
            // 
            this.buttonModBusTCP.Enabled = false;
            this.buttonModBusTCP.Location = new System.Drawing.Point(283, 18);
            this.buttonModBusTCP.Name = "buttonModBusTCP";
            this.buttonModBusTCP.Size = new System.Drawing.Size(90, 30);
            this.buttonModBusTCP.TabIndex = 3;
            this.buttonModBusTCP.Text = "ModBus TCP";
            this.buttonModBusTCP.UseVisualStyleBackColor = true;
            this.buttonModBusTCP.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonModBusASCII
            // 
            this.buttonModBusASCII.Enabled = false;
            this.buttonModBusASCII.Location = new System.Drawing.Point(192, 18);
            this.buttonModBusASCII.Name = "buttonModBusASCII";
            this.buttonModBusASCII.Size = new System.Drawing.Size(90, 30);
            this.buttonModBusASCII.TabIndex = 2;
            this.buttonModBusASCII.Text = "ModBus ASCII";
            this.buttonModBusASCII.UseVisualStyleBackColor = true;
            this.buttonModBusASCII.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonCarel
            // 
            this.buttonCarel.Location = new System.Drawing.Point(101, 18);
            this.buttonCarel.Name = "buttonCarel";
            this.buttonCarel.Size = new System.Drawing.Size(90, 30);
            this.buttonCarel.TabIndex = 1;
            this.buttonCarel.Text = "Carel";
            this.buttonCarel.UseVisualStyleBackColor = true;
            this.buttonCarel.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonModBusRTU
            // 
            this.buttonModBusRTU.Location = new System.Drawing.Point(10, 18);
            this.buttonModBusRTU.Name = "buttonModBusRTU";
            this.buttonModBusRTU.Size = new System.Drawing.Size(90, 30);
            this.buttonModBusRTU.TabIndex = 0;
            this.buttonModBusRTU.Text = "ModBus RTU";
            this.buttonModBusRTU.UseVisualStyleBackColor = true;
            this.buttonModBusRTU.Click += new System.EventHandler(this.buttons_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.textBoxWRTimeout);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.numericUpDownScanFunction);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.buttonStartScan);
            this.groupBox1.Controls.Add(this.groupBox7);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBox8);
            this.groupBox1.Location = new System.Drawing.Point(4, 67);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 318);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры";
            // 
            // textBoxWRTimeout
            // 
            this.textBoxWRTimeout.Location = new System.Drawing.Point(12, 249);
            this.textBoxWRTimeout.Name = "textBoxWRTimeout";
            this.textBoxWRTimeout.Size = new System.Drawing.Size(112, 20);
            this.textBoxWRTimeout.TabIndex = 17;
            this.textBoxWRTimeout.Leave += new System.EventHandler(this.textBoxWRTimeout_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "W/R Timeout:";
            // 
            // numericUpDownScanFunction
            // 
            this.numericUpDownScanFunction.Location = new System.Drawing.Point(87, 209);
            this.numericUpDownScanFunction.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownScanFunction.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownScanFunction.Name = "numericUpDownScanFunction";
            this.numericUpDownScanFunction.Size = new System.Drawing.Size(37, 20);
            this.numericUpDownScanFunction.TabIndex = 24;
            this.numericUpDownScanFunction.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 212);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Scan Function:";
            // 
            // buttonStartScan
            // 
            this.buttonStartScan.Location = new System.Drawing.Point(14, 275);
            this.buttonStartScan.Name = "buttonStartScan";
            this.buttonStartScan.Size = new System.Drawing.Size(110, 37);
            this.buttonStartScan.TabIndex = 25;
            this.buttonStartScan.Text = "Start Scan";
            this.buttonStartScan.UseVisualStyleBackColor = true;
            this.buttonStartScan.Click += new System.EventHandler(this.buttons_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.listViewNowScan);
            this.groupBox7.Location = new System.Drawing.Point(10, 82);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(115, 119);
            this.groupBox7.TabIndex = 31;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Now Scan";
            // 
            // listViewNowScan
            // 
            this.listViewNowScan.AllowDrop = true;
            this.listViewNowScan.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listViewNowScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listViewNowScan.FullRowSelect = true;
            this.listViewNowScan.GridLines = true;
            this.listViewNowScan.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5});
            this.listViewNowScan.Location = new System.Drawing.Point(6, 19);
            this.listViewNowScan.Name = "listViewNowScan";
            this.listViewNowScan.Size = new System.Drawing.Size(102, 95);
            this.listViewNowScan.TabIndex = 22;
            this.listViewNowScan.UseCompatibleStateImageBehavior = false;
            this.listViewNowScan.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Param";
            this.columnHeader1.Width = 51;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Value";
            this.columnHeader2.Width = 44;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.numericUpDownAddressEnd);
            this.groupBox6.Controls.Add(this.numericUpDownAddressStart);
            this.groupBox6.Location = new System.Drawing.Point(133, 238);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(92, 75);
            this.groupBox6.TabIndex = 30;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Address";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "по";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "c";
            // 
            // numericUpDownAddressEnd
            // 
            this.numericUpDownAddressEnd.Location = new System.Drawing.Point(32, 45);
            this.numericUpDownAddressEnd.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownAddressEnd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownAddressEnd.Name = "numericUpDownAddressEnd";
            this.numericUpDownAddressEnd.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownAddressEnd.TabIndex = 22;
            this.numericUpDownAddressEnd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDownAddressStart
            // 
            this.numericUpDownAddressStart.Location = new System.Drawing.Point(32, 19);
            this.numericUpDownAddressStart.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownAddressStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownAddressStart.Name = "numericUpDownAddressStart";
            this.numericUpDownAddressStart.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownAddressStart.TabIndex = 21;
            this.numericUpDownAddressStart.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.checkBoxStopBits2);
            this.groupBox5.Controls.Add(this.checkBoxStopBits1);
            this.groupBox5.Location = new System.Drawing.Point(133, 169);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(92, 57);
            this.groupBox5.TabIndex = 28;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Stop Bits";
            // 
            // checkBoxStopBits2
            // 
            this.checkBoxStopBits2.AutoSize = true;
            this.checkBoxStopBits2.Location = new System.Drawing.Point(16, 37);
            this.checkBoxStopBits2.Name = "checkBoxStopBits2";
            this.checkBoxStopBits2.Size = new System.Drawing.Size(32, 17);
            this.checkBoxStopBits2.TabIndex = 18;
            this.checkBoxStopBits2.Text = "2";
            this.checkBoxStopBits2.UseVisualStyleBackColor = true;
            // 
            // checkBoxStopBits1
            // 
            this.checkBoxStopBits1.AutoSize = true;
            this.checkBoxStopBits1.Location = new System.Drawing.Point(16, 19);
            this.checkBoxStopBits1.Name = "checkBoxStopBits1";
            this.checkBoxStopBits1.Size = new System.Drawing.Size(32, 17);
            this.checkBoxStopBits1.TabIndex = 17;
            this.checkBoxStopBits1.Text = "1";
            this.checkBoxStopBits1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkBoxParityEven);
            this.groupBox4.Controls.Add(this.checkBoxParityOdd);
            this.groupBox4.Controls.Add(this.checkBoxParityNone);
            this.groupBox4.Location = new System.Drawing.Point(133, 82);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(92, 79);
            this.groupBox4.TabIndex = 29;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Parity";
            // 
            // checkBoxParityEven
            // 
            this.checkBoxParityEven.AutoSize = true;
            this.checkBoxParityEven.Location = new System.Drawing.Point(16, 55);
            this.checkBoxParityEven.Name = "checkBoxParityEven";
            this.checkBoxParityEven.Size = new System.Drawing.Size(51, 17);
            this.checkBoxParityEven.TabIndex = 19;
            this.checkBoxParityEven.Text = "Even";
            this.checkBoxParityEven.UseVisualStyleBackColor = true;
            // 
            // checkBoxParityOdd
            // 
            this.checkBoxParityOdd.AutoSize = true;
            this.checkBoxParityOdd.Location = new System.Drawing.Point(16, 37);
            this.checkBoxParityOdd.Name = "checkBoxParityOdd";
            this.checkBoxParityOdd.Size = new System.Drawing.Size(46, 17);
            this.checkBoxParityOdd.TabIndex = 18;
            this.checkBoxParityOdd.Text = "Odd";
            this.checkBoxParityOdd.UseVisualStyleBackColor = true;
            // 
            // checkBoxParityNone
            // 
            this.checkBoxParityNone.AutoSize = true;
            this.checkBoxParityNone.Location = new System.Drawing.Point(16, 19);
            this.checkBoxParityNone.Name = "checkBoxParityNone";
            this.checkBoxParityNone.Size = new System.Drawing.Size(52, 17);
            this.checkBoxParityNone.TabIndex = 17;
            this.checkBoxParityNone.Text = "None";
            this.checkBoxParityNone.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxDataBits8);
            this.groupBox3.Controls.Add(this.checkBoxDataBits7);
            this.groupBox3.Location = new System.Drawing.Point(133, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(92, 57);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data Bits";
            // 
            // checkBoxDataBits8
            // 
            this.checkBoxDataBits8.AutoSize = true;
            this.checkBoxDataBits8.Location = new System.Drawing.Point(16, 37);
            this.checkBoxDataBits8.Name = "checkBoxDataBits8";
            this.checkBoxDataBits8.Size = new System.Drawing.Size(32, 17);
            this.checkBoxDataBits8.TabIndex = 18;
            this.checkBoxDataBits8.Text = "8";
            this.checkBoxDataBits8.UseVisualStyleBackColor = true;
            // 
            // checkBoxDataBits7
            // 
            this.checkBoxDataBits7.AutoSize = true;
            this.checkBoxDataBits7.Location = new System.Drawing.Point(16, 19);
            this.checkBoxDataBits7.Name = "checkBoxDataBits7";
            this.checkBoxDataBits7.Size = new System.Drawing.Size(32, 17);
            this.checkBoxDataBits7.TabIndex = 17;
            this.checkBoxDataBits7.Text = "7";
            this.checkBoxDataBits7.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.checkBoxSpeed256000);
            this.groupBox2.Controls.Add(this.checkBoxSpeed128000);
            this.groupBox2.Controls.Add(this.checkBoxSpeed115200);
            this.groupBox2.Controls.Add(this.checkBoxSpeed57600);
            this.groupBox2.Controls.Add(this.checkBoxSpeed56000);
            this.groupBox2.Controls.Add(this.checkBoxSpeed38400);
            this.groupBox2.Controls.Add(this.checkBoxSpeed19200);
            this.groupBox2.Controls.Add(this.checkBoxSpeed14400);
            this.groupBox2.Controls.Add(this.checkBoxSpeed9600);
            this.groupBox2.Controls.Add(this.checkBoxSpeed7200);
            this.groupBox2.Controls.Add(this.checkBoxSpeed4800);
            this.groupBox2.Controls.Add(this.checkBoxSpeed2400);
            this.groupBox2.Controls.Add(this.checkBoxSpeed1200);
            this.groupBox2.Controls.Add(this.checkBoxSpeed600);
            this.groupBox2.Controls.Add(this.checkBoxSpeed300);
            this.groupBox2.Location = new System.Drawing.Point(231, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(147, 294);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Speed";
            // 
            // checkBoxSpeed256000
            // 
            this.checkBoxSpeed256000.AutoSize = true;
            this.checkBoxSpeed256000.Location = new System.Drawing.Point(13, 271);
            this.checkBoxSpeed256000.Name = "checkBoxSpeed256000";
            this.checkBoxSpeed256000.Size = new System.Drawing.Size(62, 17);
            this.checkBoxSpeed256000.TabIndex = 16;
            this.checkBoxSpeed256000.Text = "256000";
            this.checkBoxSpeed256000.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed128000
            // 
            this.checkBoxSpeed128000.AutoSize = true;
            this.checkBoxSpeed128000.Location = new System.Drawing.Point(13, 253);
            this.checkBoxSpeed128000.Name = "checkBoxSpeed128000";
            this.checkBoxSpeed128000.Size = new System.Drawing.Size(62, 17);
            this.checkBoxSpeed128000.TabIndex = 15;
            this.checkBoxSpeed128000.Text = "128000";
            this.checkBoxSpeed128000.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed115200
            // 
            this.checkBoxSpeed115200.AutoSize = true;
            this.checkBoxSpeed115200.Location = new System.Drawing.Point(13, 235);
            this.checkBoxSpeed115200.Name = "checkBoxSpeed115200";
            this.checkBoxSpeed115200.Size = new System.Drawing.Size(62, 17);
            this.checkBoxSpeed115200.TabIndex = 14;
            this.checkBoxSpeed115200.Text = "115200";
            this.checkBoxSpeed115200.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed57600
            // 
            this.checkBoxSpeed57600.AutoSize = true;
            this.checkBoxSpeed57600.Location = new System.Drawing.Point(13, 217);
            this.checkBoxSpeed57600.Name = "checkBoxSpeed57600";
            this.checkBoxSpeed57600.Size = new System.Drawing.Size(56, 17);
            this.checkBoxSpeed57600.TabIndex = 13;
            this.checkBoxSpeed57600.Text = "57600";
            this.checkBoxSpeed57600.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed56000
            // 
            this.checkBoxSpeed56000.AutoSize = true;
            this.checkBoxSpeed56000.Location = new System.Drawing.Point(13, 199);
            this.checkBoxSpeed56000.Name = "checkBoxSpeed56000";
            this.checkBoxSpeed56000.Size = new System.Drawing.Size(56, 17);
            this.checkBoxSpeed56000.TabIndex = 12;
            this.checkBoxSpeed56000.Text = "56000";
            this.checkBoxSpeed56000.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed38400
            // 
            this.checkBoxSpeed38400.AutoSize = true;
            this.checkBoxSpeed38400.Location = new System.Drawing.Point(13, 181);
            this.checkBoxSpeed38400.Name = "checkBoxSpeed38400";
            this.checkBoxSpeed38400.Size = new System.Drawing.Size(56, 17);
            this.checkBoxSpeed38400.TabIndex = 11;
            this.checkBoxSpeed38400.Text = "38400";
            this.checkBoxSpeed38400.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed19200
            // 
            this.checkBoxSpeed19200.AutoSize = true;
            this.checkBoxSpeed19200.Location = new System.Drawing.Point(13, 163);
            this.checkBoxSpeed19200.Name = "checkBoxSpeed19200";
            this.checkBoxSpeed19200.Size = new System.Drawing.Size(56, 17);
            this.checkBoxSpeed19200.TabIndex = 10;
            this.checkBoxSpeed19200.Text = "19200";
            this.checkBoxSpeed19200.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed14400
            // 
            this.checkBoxSpeed14400.AutoSize = true;
            this.checkBoxSpeed14400.Location = new System.Drawing.Point(13, 145);
            this.checkBoxSpeed14400.Name = "checkBoxSpeed14400";
            this.checkBoxSpeed14400.Size = new System.Drawing.Size(56, 17);
            this.checkBoxSpeed14400.TabIndex = 9;
            this.checkBoxSpeed14400.Text = "14400";
            this.checkBoxSpeed14400.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed9600
            // 
            this.checkBoxSpeed9600.AutoSize = true;
            this.checkBoxSpeed9600.Location = new System.Drawing.Point(13, 127);
            this.checkBoxSpeed9600.Name = "checkBoxSpeed9600";
            this.checkBoxSpeed9600.Size = new System.Drawing.Size(50, 17);
            this.checkBoxSpeed9600.TabIndex = 8;
            this.checkBoxSpeed9600.Text = "9600";
            this.checkBoxSpeed9600.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed7200
            // 
            this.checkBoxSpeed7200.AutoSize = true;
            this.checkBoxSpeed7200.Location = new System.Drawing.Point(13, 109);
            this.checkBoxSpeed7200.Name = "checkBoxSpeed7200";
            this.checkBoxSpeed7200.Size = new System.Drawing.Size(50, 17);
            this.checkBoxSpeed7200.TabIndex = 7;
            this.checkBoxSpeed7200.Text = "7200";
            this.checkBoxSpeed7200.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed4800
            // 
            this.checkBoxSpeed4800.AutoSize = true;
            this.checkBoxSpeed4800.Location = new System.Drawing.Point(13, 91);
            this.checkBoxSpeed4800.Name = "checkBoxSpeed4800";
            this.checkBoxSpeed4800.Size = new System.Drawing.Size(50, 17);
            this.checkBoxSpeed4800.TabIndex = 6;
            this.checkBoxSpeed4800.Text = "4800";
            this.checkBoxSpeed4800.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed2400
            // 
            this.checkBoxSpeed2400.AutoSize = true;
            this.checkBoxSpeed2400.Location = new System.Drawing.Point(13, 73);
            this.checkBoxSpeed2400.Name = "checkBoxSpeed2400";
            this.checkBoxSpeed2400.Size = new System.Drawing.Size(50, 17);
            this.checkBoxSpeed2400.TabIndex = 5;
            this.checkBoxSpeed2400.Text = "2400";
            this.checkBoxSpeed2400.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed1200
            // 
            this.checkBoxSpeed1200.AutoSize = true;
            this.checkBoxSpeed1200.Location = new System.Drawing.Point(13, 55);
            this.checkBoxSpeed1200.Name = "checkBoxSpeed1200";
            this.checkBoxSpeed1200.Size = new System.Drawing.Size(50, 17);
            this.checkBoxSpeed1200.TabIndex = 4;
            this.checkBoxSpeed1200.Text = "1200";
            this.checkBoxSpeed1200.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed600
            // 
            this.checkBoxSpeed600.AutoSize = true;
            this.checkBoxSpeed600.Location = new System.Drawing.Point(13, 37);
            this.checkBoxSpeed600.Name = "checkBoxSpeed600";
            this.checkBoxSpeed600.Size = new System.Drawing.Size(44, 17);
            this.checkBoxSpeed600.TabIndex = 3;
            this.checkBoxSpeed600.Text = "600";
            this.checkBoxSpeed600.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed300
            // 
            this.checkBoxSpeed300.AutoSize = true;
            this.checkBoxSpeed300.Location = new System.Drawing.Point(13, 19);
            this.checkBoxSpeed300.Name = "checkBoxSpeed300";
            this.checkBoxSpeed300.Size = new System.Drawing.Size(44, 17);
            this.checkBoxSpeed300.TabIndex = 2;
            this.checkBoxSpeed300.Text = "300";
            this.checkBoxSpeed300.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.comboBoxCOMPort);
            this.groupBox8.Location = new System.Drawing.Point(10, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(115, 57);
            this.groupBox8.TabIndex = 22;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "COM port";
            // 
            // comboBoxCOMPort
            // 
            this.comboBoxCOMPort.FormattingEnabled = true;
            this.comboBoxCOMPort.Location = new System.Drawing.Point(13, 22);
            this.comboBoxCOMPort.Name = "comboBoxCOMPort";
            this.comboBoxCOMPort.Size = new System.Drawing.Size(89, 21);
            this.comboBoxCOMPort.TabIndex = 1;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.tabControl1);
            this.groupBox9.Location = new System.Drawing.Point(393, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(344, 410);
            this.groupBox9.TabIndex = 4;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Результат сканирования";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 16);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(338, 391);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.listViewScanResult);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(330, 365);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Result";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // listViewScanResult
            // 
            this.listViewScanResult.AllowDrop = true;
            this.listViewScanResult.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.listViewScanResult.ContextMenuStrip = this.contextMenuStrip1;
            this.listViewScanResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewScanResult.FullRowSelect = true;
            this.listViewScanResult.GridLines = true;
            this.listViewScanResult.Location = new System.Drawing.Point(3, 3);
            this.listViewScanResult.Name = "listViewScanResult";
            this.listViewScanResult.Size = new System.Drawing.Size(324, 359);
            this.listViewScanResult.TabIndex = 0;
            this.listViewScanResult.UseCompatibleStateImageBehavior = false;
            this.listViewScanResult.View = System.Windows.Forms.View.Details;
            this.listViewScanResult.DragDrop += new System.Windows.Forms.DragEventHandler(this.listViewScanResult_DragDrop);
            this.listViewScanResult.DragEnter += new System.Windows.Forms.DragEventHandler(this.listViewScanResult_DragEnter);
            this.listViewScanResult.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listViewScanResult_MouseMove);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Com";
            this.columnHeader3.Width = 45;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Address";
            this.columnHeader4.Width = 54;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "DataBits";
            this.columnHeader5.Width = 55;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Parity";
            this.columnHeader6.Width = 48;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "StopBits";
            this.columnHeader7.Width = 55;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Speed";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.очиститьToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(124, 26);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // очиститьToolStripMenuItem
            // 
            this.очиститьToolStripMenuItem.Name = "очиститьToolStripMenuItem";
            this.очиститьToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.очиститьToolStripMenuItem.Text = "Очистить";
            this.очиститьToolStripMenuItem.Click += new System.EventHandler(this.очиститьToolStripMenuItem_Click);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.listBoxLog);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(330, 365);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Log";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // listBoxLog
            // 
            this.listBoxLog.ContextMenuStrip = this.contextMenuStrip1;
            this.listBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(3, 3);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(324, 359);
            this.listBoxLog.TabIndex = 0;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progressBar1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.progressBar1.Location = new System.Drawing.Point(4, 389);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(386, 23);
            this.progressBar1.TabIndex = 33;
            // 
            // ScanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 416);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxMode);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ScanForm";
            this.Text = "Scaner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScanForm_FormClosing);
            this.Load += new System.EventHandler(this.ScanForm_Load);
            this.groupBoxMode.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScanFunction)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAddressEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAddressStart)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxMode;
        private System.Windows.Forms.Button buttonCarel;
        private System.Windows.Forms.Button buttonModBusRTU;
        private System.Windows.Forms.Button buttonModBusTCP;
        private System.Windows.Forms.Button buttonModBusASCII;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numericUpDownScanFunction;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonStartScan;
        private System.Windows.Forms.GroupBox groupBox7;
        public System.Windows.Forms.ListView listViewNowScan;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownAddressEnd;
        private System.Windows.Forms.NumericUpDown numericUpDownAddressStart;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox checkBoxStopBits2;
        private System.Windows.Forms.CheckBox checkBoxStopBits1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkBoxParityEven;
        private System.Windows.Forms.CheckBox checkBoxParityOdd;
        private System.Windows.Forms.CheckBox checkBoxParityNone;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBoxDataBits8;
        private System.Windows.Forms.CheckBox checkBoxDataBits7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxSpeed256000;
        private System.Windows.Forms.CheckBox checkBoxSpeed128000;
        private System.Windows.Forms.CheckBox checkBoxSpeed115200;
        private System.Windows.Forms.CheckBox checkBoxSpeed57600;
        private System.Windows.Forms.CheckBox checkBoxSpeed56000;
        private System.Windows.Forms.CheckBox checkBoxSpeed38400;
        private System.Windows.Forms.CheckBox checkBoxSpeed19200;
        private System.Windows.Forms.CheckBox checkBoxSpeed14400;
        private System.Windows.Forms.CheckBox checkBoxSpeed9600;
        private System.Windows.Forms.CheckBox checkBoxSpeed7200;
        private System.Windows.Forms.CheckBox checkBoxSpeed4800;
        private System.Windows.Forms.CheckBox checkBoxSpeed2400;
        private System.Windows.Forms.CheckBox checkBoxSpeed1200;
        private System.Windows.Forms.CheckBox checkBoxSpeed600;
        private System.Windows.Forms.CheckBox checkBoxSpeed300;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox comboBoxCOMPort;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage5;
        public System.Windows.Forms.ListView listViewScanResult;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.TabPage tabPage6;
        public System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox textBoxWRTimeout;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem очиститьToolStripMenuItem;
    }
}