﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProtocolTester
{
    public class ExtendedEventArgs : EventArgs
    {
        private object control;

        public object Control
        {
            get { return control; }
        }

        public ExtendedEventArgs(object _control)
        {
            control = _control;
        }

    }
}
