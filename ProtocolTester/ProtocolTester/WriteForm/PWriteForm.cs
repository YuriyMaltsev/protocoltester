﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProtocolTester
{
    public class PWriteForm
    {
        IWriteForm _writeForm;
        MWriteForm mWriteForm;

        public PWriteForm(WriteForm writeForm, Presenter presenter)
        {
            _writeForm = writeForm;
            mWriteForm = new MWriteForm(presenter);

            _writeForm.WriteButtonClick +=new EventHandler<EventArgs>(_writeForm_WriteButtonClick);
        }

        public void _writeForm_WriteButtonClick(object sender, EventArgs e)
        {
            
            mWriteForm.NewValue = _writeForm.textBoxNewValueText == "False" ? "0" : _writeForm.textBoxNewValueText == "True" ? "1" : _writeForm.textBoxNewValueText;
            mWriteForm.WriteValue((Device)sender);
        }
    }
}
