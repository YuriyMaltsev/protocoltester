﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProtocolTester
{
    public partial class WriteForm : Form, IWriteForm
    {
        public event EventHandler<EventArgs> WriteButtonClick;

        Device _device;
        
        public string textBoxNewValueText
        {
            get { return textBoxNewValue.Text; }
        }

        public WriteForm(Device device)
        {
            InitializeComponent();
            _device = device;

            string val = _device.DataItems[_device.SelectIndex].Value;
            textBoxNewValue.Text = val == "false" ? "0" :
                val == "true" ? "1" : val;
        }

        private void buttonWrite_Click(object sender, EventArgs e)
        {
            if (WriteButtonClick != null)
                WriteButtonClick(_device, e);
            Close();
        }

        private void WriteForm_Load(object sender, EventArgs e)
        {
            textBoxNewValue.Focus();
            textBoxNewValue.SelectAll();
        }

        private void textBoxNewValue_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                buttonWrite.PerformClick();
            }
        }
    }
}
