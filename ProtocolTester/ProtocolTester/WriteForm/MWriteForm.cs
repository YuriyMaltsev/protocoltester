﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProtocolTester
{
    public class MWriteForm
    {
        Presenter _presenter;
        string newValue = "";

        public string NewValue { set { newValue = value; } }

        public MWriteForm(Presenter presenter)
        {
            _presenter = presenter;
        }

        public void WriteValue(Device _device)
        {
            int address = _device.Address;
            int dataType = _device.DataItems[_device.SelectIndex].DataType_ == DataItem.DataType.REAL ? 65 :
                _device.DataItems[_device.SelectIndex].DataType_ == DataItem.DataType.INT ? 73 : 68;
            int itemAddress = _device is CarelUnit ? _device.DataItems[_device.SelectIndex].Variable + 48 : _device.DataItems[_device.SelectIndex].Variable;

            if (_device is CarelUnit)
            {
                Carel.WriteNewValue(dataType == 73 | dataType == 68 ? short.Parse(newValue.Replace('.', ',').Replace("False", "0").Replace("True", "1")) : (short)(double.Parse(newValue.Replace('.', ',').Replace("False", "0").Replace("True", "1")) * 10),
                    address,
                    _device.DataItems[_device.SelectIndex].DataType_,
                    itemAddress,
                    _device);
            }
            else
            {
                if (_device.DataItems[_device.SelectIndex].DataType_ == DataItem.DataType.BOOL)
                    ModBusRTU.WriteSingleCoil(_device, (ushort)_device.DataItems[_device.SelectIndex].Variable, dataType == 73 | dataType == 68 ? short.Parse(newValue.Replace('.', ',').Replace("False", "0").Replace("True", "1")) : (short)(double.Parse(newValue.Replace('.', ',').Replace("False", "0").Replace("True", "1")) * 10));
                else
                {
                    int res =  dataType == 73 | dataType == 68 ? (int)(double.Parse(newValue.Replace('.', ',').Replace("False", "0").Replace("True", "1"))) : 
                        (int)(double.Parse(newValue.Replace('.', ',').Replace("False", "0").Replace("True", "1")) * 10);

                    ModBusRTU.WriteSingleRegister(_device, (ushort)_device.DataItems[_device.SelectIndex].Variable, res);
                    
                }
            }
        }

    }
}
