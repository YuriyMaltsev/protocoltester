﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProtocolTester.LoggingForm
{
    public partial class LogForm : Form, ILoggingForm
    {
        

        public LogForm()
        {
            InitializeComponent();
            Device.UpdateListViewVirualItemsCount += new EventHandler<EventArgs>(Device_UpdateListViewVirualItemsCount);
        }

        void Device_UpdateListViewVirualItemsCount(object sender, EventArgs e)
        {
            if (listView1.InvokeRequired)
                listView1.Invoke(new Action(() => listView1.VirtualListSize = (int)sender));
            else
                listView1.VirtualListSize = (int)sender;
        }

        private void listView1_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
           
         //   e.DrawDefault = true;
            using (StringFormat sf = new StringFormat())
            {
                //e.Graphics.FillRectangle(new SolidBrush(Color.Blue), e.Bounds);
               // e.Graphics.DrawString(e.SubItem.Text, ((ListView)sender).Font, new SolidBrush(System.Drawing.ColorTranslator.FromHtml(System.Drawing.ColorTranslator.ToHtml(Color.LightGreen))), e.Bounds, sf);
                e.Graphics.DrawString(e.SubItem.Text, ((ListView)sender).Font, new SolidBrush(Color.GreenYellow), e.Bounds, sf);  
            }
            //e
        }

        private void listView1_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            ListViewItem lvi = new ListViewItem(new string[] { Model.LogList[e.ItemIndex] });

            e.Item = lvi;

            
        }

        private void LogForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Device.UpdateListViewVirualItemsCount -= Device_UpdateListViewVirualItemsCount;


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                listView1.Invalidate();
            }
            catch { }
        }
    }
}
