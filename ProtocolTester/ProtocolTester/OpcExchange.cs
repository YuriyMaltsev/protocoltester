﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ProtocolTester
{
    class OpcExchange
    {
        public event EventHandler<EventArgs> ChangeOpcButtonPic;

        private List<object[]> _tagList;//ссылка на список тегов, элементы которого это массивы, 
        //первый элемент это строка описывающяя тег, через пробел индекс, имя, тип тега, модификатор доступа
        //второй элемент значение переменной
        //третий элемент булевый, при изменении значения устанавливается в true, после отсылки устанавливается в false

        public OpcExchange(List<object[]> tagList)
        {
            _tagList = tagList;
        }
        
        /// <summary>
        /// старт цыкла передачи всех изменившихся тегов с шифрованием ssl
        /// </summary>
        public void StartExchangeSSL(string certificatePath)
        {
            IPEndPoint Addr = null; //конечная точка(IP и порт)
            Addr = new IPEndPoint(IPAddress.Any, 43211); //"localhost" = 127.0.0.1

            TcpClient clientSocket;// = new TcpListener(Addr);
            bool breakFlag = false;

            if (Model.Listener == null)
            {
                if (ChangeOpcButtonPic != null)
                    ChangeOpcButtonPic("opcON.png", EventArgs.Empty);
                Model.Listener = new TcpListener(Addr);

                Model.Listener.Start();

                


                while (true)
                {
                    clientSocket = Model.Listener.AcceptTcpClient();

                    X509Certificate2 certificate = new X509Certificate2(certificatePath, "1234");//Environment.CurrentDirectory + "\\server.pfx", "1234");


                    using (SslStream sslStream = new SslStream(clientSocket.GetStream(), false,
                    new RemoteCertificateValidationCallback(ValidateClientCertificate), null))
                    {
                        // Set a client certificate as mandatory
                        sslStream.AuthenticateAsServer(certificate, false, System.Security.Authentication.SslProtocols.Ssl3, true);


                        byte[] answer = new byte[] { };
                        string hashString = "";

                        while (true)
                        {
                            if (Model.Listener == null)
                            {
                                breakFlag = true;
                                clientSocket.Close();
                                break;
                            }

                            try
                            {
                                var request = Encoding.ASCII.GetString(ReadAllBytes(sslStream));

                                 if (request.Substring(0, 1) == "\r")
                                     request = request.Substring(2, request.Length - 2);

                                switch (request)
                                {
                                    case "TAGS\r\n":
                                        hashString = SendTags(sslStream);
                                        sslStream.Write(answer, 0, answer.Length);
                                        break;
                                    case "HASH\r\n":
                                        answer = Encoding.ASCII.GetBytes("HASH " + getHashSha256(hashString) + "\r\n");
                                        sslStream.Write(answer, 0, answer.Length);
                                        break;
                                    case "SUBSCRIBE\r\n":
                                        answer = Encoding.ASCII.GetBytes("SUBSCRIBE_ACK" + "\r\n");
                                        sslStream.Write(answer, 0, answer.Length);
                                        break;
                                    case "VALUES\r\n":
                                        answer = Encoding.ASCII.GetBytes("VALUES " + _tagList.Count + "\r\n");
                                        sslStream.Write(answer, 0, answer.Length);
                                        for (int i = 0; i < _tagList.Count; i++)
                                        {
                                            var val = ((DataItem)_tagList[i][1]).Value == null ? "0" : ((DataItem)_tagList[i][1]).Value;

                                            // var w = ((DataItem)_tagList[i][1]).Value == "False" ? "0" : ((DataItem)_tagList[i][1]).Value == "True" ? "1" : ((DataItem)_tagList[i][1]).Value;
                                            var v = "VALUE " + (i + 1).ToString() + " " + val.Replace(',', '.').Replace("False", "0").Replace("True", "1") + " " +(((Device)_tagList[i][2]).Quality ? "g" : "b"  )+ " " +
                                                    (((DataItem)_tagList[i][1]).TimeStamp != null ? ((DataItem)_tagList[i][1]).TimeStamp : DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss.fff")) + "\r\n";
                                            answer = Encoding.ASCII.GetBytes(v);
                                            sslStream.Write(answer, 0, answer.Length);
                                            ((DataItem)_tagList[i][1]).NeedSend = false;

                                            if (i == _tagList.Count - 2)
                                            {

                                            }
                                        }
                                        answer = Encoding.ASCII.GetBytes("");
                                        break;
                                    case "PING\r\n":
                                        var tempList = _tagList.Where((v) => ((DataItem)v[1]).NeedSend).ToArray();

                                        int c = tempList.Count();

                                        string answerstring = "";
                                        foreach (var v in tempList)
                                        {
                                            string val = ((DataItem)v[1]).Value == null ? "0" : ((DataItem)v[1]).Value.Replace(',', '.').Replace("False", "0").Replace("True", "1");
                                            answerstring = answerstring + (((string)v[0]).Split(' ')[1]).ToString() + " " + val + " " + (((Device)v[2]).Quality ? "g" : "b" ) + " " +
                                                (((DataItem)v[1]).TimeStamp != null ? ((DataItem)v[1]).TimeStamp : DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss.fff")) + "|";
                                            ((DataItem)v[1]).NeedSend = false;
                                        }

                                        answer = Encoding.ASCII.GetBytes(answerstring + "\r\n");
                                        sslStream.Write(answer, 0, answer.Length);

                                        break;
                                    default:
                                        if (request.Length > 7)
                                        {
                                            if (request.Substring(0, 7) == "NEW_VAL")
                                            {
                                                string[] tempArr = request.Split(' ');
                                                if (tempArr.Length > 2)
                                                {
                                                    if (_tagList[int.Parse(tempArr[1]) - 1][2] is CarelUnit)
                                                    {
                                                        Carel.WriteNewValue(((DataItem)_tagList[int.Parse(tempArr[1]) - 1][1]).DataType_ == DataItem.DataType.INT | ((DataItem)_tagList[int.Parse(tempArr[1]) - 1][1]).DataType_ == DataItem.DataType.BOOL ?
                                                            short.Parse(tempArr[2].Substring(tempArr[2].Length - 1, 1) == "\n" ?
                                                                tempArr[2].Substring(0, tempArr[2].Length - 2) : tempArr[2])
                                                                : (short)(double.Parse((tempArr[2].Substring(tempArr[2].Length - 1, 1) == "\n" ?
                                                                tempArr[2].Substring(0, tempArr[2].Length - 2) : tempArr[2]).Replace('.', ',')) * 10),
                                                                ((Device)_tagList[int.Parse(tempArr[1]) - 1][2]).Address,
                                                                ((DataItem)_tagList[int.Parse(tempArr[1]) - 1][1]).DataType_,
                                                                ((DataItem)_tagList[int.Parse(tempArr[1]) - 1][1]).Variable + (((Device)_tagList[int.Parse(tempArr[1]) - 1][2]) is CarelUnit ? 48 : 0),
                                                                ((Device)_tagList[int.Parse(tempArr[1]) - 1][2]));
                                                    }
                                                    else
                                                    {
                                                        if (_tagList[int.Parse(tempArr[1]) - 1][2] is ModBusRTUUnit)
                                                        {
                                                            if (((DataItem)_tagList[int.Parse(tempArr[1]) - 1][1]).DataType_ == DataItem.DataType.BOOL)
                                                            {
                                                                ModBusRTU.WriteSingleCoil((Device)_tagList[int.Parse(tempArr[1]) - 1][2],
                                                                    (ushort)(((DataItem)_tagList[int.Parse(tempArr[1]) - 1][1]).Variable),
                                                                    short.Parse(tempArr[2].Substring(tempArr[2].Length - 1, 1) == "\n" ?
                                                                tempArr[2].Substring(0, tempArr[2].Length - 2) : tempArr[2]));
                                                            }
                                                            else
                                                            {
                                                                int res = ((DataItem)_tagList[int.Parse(tempArr[1]) - 1][1]).DataType_ == DataItem.DataType.REAL ?
                                                                    (int)(double.Parse(tempArr[2].Substring(tempArr[2].Length - 1, 1) == "\n" ?
                                                                tempArr[2].Substring(0, tempArr[2].Length - 2).Replace('.', ',') : tempArr[2].Replace('.', ',')) * 10) :
                                                                    (int)(double.Parse(tempArr[2].Substring(tempArr[2].Length - 1, 1) == "\n" ?
                                                                tempArr[2].Substring(0, tempArr[2].Length - 2).Replace('.', ',') : tempArr[2].Replace('.', ',')));
                                                                
                                                                    ModBusRTU.WriteSingleRegister((Device)_tagList[int.Parse(tempArr[1]) - 1][2],
                                                                        (ushort)(((DataItem)_tagList[int.Parse(tempArr[1]) - 1][1]).Variable), res);
                                                                
                                                            }
                                                        }
                                                    }

                                                    ((DataItem)_tagList[int.Parse(tempArr[1]) - 1][1]).NeedSend = true;
                                                }
                                            }
                                            else
                                                if (request.Substring(0, 10) == "PAR_CONNTO")
                                                {
                                                    answer = Encoding.ASCII.GetBytes("PAR_CONNTO_ACK" + "\r\n");
                                                    sslStream.Write(answer, 0, answer.Length);
                                                }
                                                else
                                                {
                                                    if (request.Substring(0, 10) == "PAR_SUBSTO")
                                                    {
                                                        answer = Encoding.ASCII.GetBytes("PAR_SUBSTO_ACK" + "\r\n");
                                                        sslStream.Write(answer, 0, answer.Length);
                                                    }
                                                }
                                        }
                                        break;
                                }
                            }
                            catch(Exception ex)
                            {
                                ErrorEventLog.LogError("StartExchangeSSL.1 - " + ex.Message, Model.GetProgrammDir() + "\\Logs\\OPC.txt");
                                // breakFlag = true;
                                clientSocket.Close();
                                break;
                            }
                        }
                    }
                    if (breakFlag)
                    {
                        if (ChangeOpcButtonPic != null)
                            ChangeOpcButtonPic("opcOFF.png", EventArgs.Empty);
                        System.Threading.Thread.Sleep(1000);
                        break;
                        
                    }
                }
            }

            else
            {
                Model.Listener.Stop();
                Model.Listener = null;

            }
        }

        public static string getHashSha256(string text)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        private string SendTags(Socket socket, IPEndPoint Addr)
        {
            byte[] answer = Encoding.ASCII.GetBytes("TAGS " + _tagList.Count + "\r\n");
            socket.SendTo(answer, SocketFlags.None, Addr);

            string hashstr = "";
            foreach (var v in _tagList)
            {
                answer = Encoding.ASCII.GetBytes(v[0] + "\r\n");
                socket.SendTo(answer, SocketFlags.None, Addr);
                hashstr = hashstr + ((string)v[0]).Substring(4, ((string)v[0]).Length - 4) + Environment.NewLine;
            }

            return hashstr;
        }

        private string SendTags(SslStream sslStream)
        {
            byte[] answer = Encoding.ASCII.GetBytes("TAGS " + _tagList.Count + "\r\n");
            sslStream.Write(answer, 0, answer.Length);

            string hashstr = "";
            foreach (var v in _tagList)
            {
                answer = Encoding.ASCII.GetBytes(v[0] + "\r\n");
                sslStream.Write(answer, 0, answer.Length);
                hashstr = hashstr + ((string)v[0]).Substring(4, ((string)v[0]).Length - 4) + Environment.NewLine;
            }

            return hashstr;
        }

        private byte[] ReadAllBytes(SslStream sslStream)
        {
            byte[] arr = new byte[] { };

            List<byte> bl = new List<byte>();
            while (sslStream.CanRead)
            {
                bl.Add((byte)sslStream.ReadByte());
                if (bl.Count > 2)
                    if (bl[bl.Count - 1] == 10 & bl[bl.Count - 2] == 13)
                        break;
            }

            arr = new byte[bl.Count];
            for (int i = 0; i < bl.Count; i++)
                arr[i] = bl[i];

            return arr;
        }

        public static bool ValidateClientCertificate(object sender, X509Certificate certificate,
            X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            // Accept all certificates
            return true;
        }

        /// <summary>
        /// Возвращаее строку состоящую из цифр, удаляя все остальное из входной строки
        /// </summary>
        /// <param name="text">входная произвольная строка</param>
        /// <returns></returns>
        public static string ReturnOnlyNums(string text)
        {
            string res = "";
            string pattern = @"(\d)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(text);

            while (match.Success)
            {
                res = res + match.Groups[1].Value;
                match = match.NextMatch();
            }
            return res;

        }
    }
}
