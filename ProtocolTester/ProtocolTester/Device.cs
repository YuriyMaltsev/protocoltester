﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Diagnostics;
using System.Drawing;
using System.Xml.Serialization;

namespace ProtocolTester
{
    //Сборник класснов описывающих устройства

    /// <summary>
    /// Базовый класс всех устройств
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(CarelUnit))]
    [XmlInclude(typeof(ModBusRTUUnit))]
    [XmlType("device")]
    public class Device
    {
        public static event EventHandler<EventArgs> InvalidateListViewDevice;

        public static event EventHandler<EventArgs> UpdateListViewVirualItemsCount;

        [XmlIgnore]
        public string OpcName { get; set; }
        [XmlIgnore]
        public BMCRemedy bMCRemedy { get; set; }

        private ulong crcError;
        private ulong unitAnswerError;
        private ulong notAnswer;
        private ulong emptyAnswer;
        private ulong writeError;
        private ulong comPortError;
        private ulong readError;
        private ulong goodAnswer;
        private ulong allRequest;
        private ulong incorrectAnswer;
        private Color color = Color.White;
        private bool quality = true;

        [XmlIgnore]
        public bool Quality { get { return quality; } set { quality = value; }  }
        [XmlIgnore]
        public int SelectIndex { get; set; }
        [XmlIgnore]
        public Color _Color
        {
            get { return color; }
        }


        /// <summary>
        /// Не корректный ответ
        /// </summary>
        [XmlIgnore]
        public ulong IncorrectAnswer
        {
            get { return incorrectAnswer; }
            set
            {
                incorrectAnswer = value == 0 ? value : (incorrectAnswer + 1 > 18446744073709551610 ? 0 : incorrectAnswer + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; ConnectionLine485.IncorrectAnswer++;
        //        if (color != Color.MediumSlateBlue)
                {
                    color = Color.MediumSlateBlue;
                    Device.InvalidateListViewDevice(new object[] { this, GetGoodQualityPercent() }, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Всего запросов
        /// </summary>
        [XmlIgnore]
        public ulong AllRequest
        {
            get { return allRequest; }
        }

        /// <summary>
        /// Ошибки чтения из порта
        /// </summary>
        [XmlIgnore]
        public ulong GoodAnswer
        {
            get { return goodAnswer; }
            set
            {
                goodAnswer = value == 0 ? value : (goodAnswer + 1 > 18446744073709551610 ? 0 : goodAnswer + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; ConnectionLine485.GoodAnswer++;
         //       if (color != Color.Green)
                {
                    color = Color.Green;
                    Device.InvalidateListViewDevice(new object[] { this, GetGoodQualityPercent() }, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Ошибки чтения из порта
        /// </summary>
        [XmlIgnore]
        public ulong ReadError
        {
            get { return readError; }
            set
            {
                readError = value == 0 ? value : (readError + 1 > 18446744073709551610 ? 0 : readError + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; ConnectionLine485.ReadError++;
         //       if (color != Color.Orange)
                {
                    color = Color.Orange;
                    Device.InvalidateListViewDevice(new object[] { this, GetGoodQualityPercent() }, EventArgs.Empty);
                } 
            }
        }

        /// <summary>
        /// Ошибки ком порта (порт не открыт, порт занят)
        /// </summary>
        [XmlIgnore]
        public ulong СomPortError
        {
            get { return comPortError; }
            set
            {
                comPortError = value == 0 ? value : (comPortError + 1 > 18446744073709551610 ? 0 : comPortError + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; ConnectionLine485.СomPortError++;
         //       if (color != Color.Black)
                {
                    color = Color.Black;
                    Device.InvalidateListViewDevice(new object[] { this, GetGoodQualityPercent() }, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Ошибки записи в порт
        /// </summary>
        [XmlIgnore]
        public ulong WriteError
        {
            get { return writeError; }
            set
            {
                writeError = value == 0 ? value : (writeError + 1 > 18446744073709551610 ? 0 : writeError + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; ConnectionLine485.WriteError++;
         //       if (color != Color.IndianRed)
                {
                    color = Color.IndianRed;
                    Device.InvalidateListViewDevice(new object[] { this, GetGoodQualityPercent() }, EventArgs.Empty);
                } 
            }
        }

        /// <summary>
        /// С момента последнего запроса изменения не произошли
        /// </summary>
        [XmlIgnore]
        public ulong EmptyAnswer
        {
            get { return emptyAnswer; }
            set
            {
                emptyAnswer = value == 0 ? value : (emptyAnswer + 1 > 18446744073709551610 ? 0 : emptyAnswer + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; ConnectionLine485.EmptyAnswer++;
          //      if (color != Color.Yellow)
                {
                    color = Color.Yellow;
                    Device.InvalidateListViewDevice(new object[] { this, GetGoodQualityPercent() }, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Нет ответа от устройства
        /// </summary>
        [XmlIgnore]
        public ulong NotAnswer
        {
            get { return notAnswer; }
            set
            {
                notAnswer = value == 0 ? value : (notAnswer + 1 > 18446744073709551610 ? 0 : notAnswer + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; ConnectionLine485.NotAnswer++;
              //  if (color != Color.Gray)
                {
                    color = Color.Gray;
                    Device.InvalidateListViewDevice(new object[] { this, GetGoodQualityPercent() }, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Ошибки ответа контроллера
        /// </summary>
        [XmlIgnore]
        public ulong UnitAnswerError
        {
            get { return unitAnswerError; }
            set
            {
                unitAnswerError = value == 0 ? value : (unitAnswerError + 1 > 18446744073709551610 ? 0 : unitAnswerError + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; ConnectionLine485.UnitAnswerError++;
        //        if (color != Color.OrangeRed)
                {
                    color = Color.OrangeRed;
                    Device.InvalidateListViewDevice(new object[] { this, GetGoodQualityPercent() }, EventArgs.Empty);
                } 
            }
        }

        /// <summary>
        /// Ошибки контрольной суммы
        /// </summary>
        [XmlIgnore]
        public ulong CrcError
        {
            get { return crcError; }
            set
            {
                crcError = value == 0 ? value : (crcError + 1 > 18446744073709551610 ? 0 : crcError + 1); allRequest = allRequest > 18446744073709551610 ? 0 : allRequest + 1; ConnectionLine485.CrcError++; 
        //        if (color != Color.Red)
                {
                    color = Color.Red;
                    Device.InvalidateListViewDevice(new object[] { this, GetGoodQualityPercent() }, EventArgs.Empty);
                }
            }
        }
        
        private ulong GetGoodQualityPercent()
        {
            ulong res = 0;

            res = (goodAnswer + emptyAnswer) * 100 / allRequest;

            return res;
        }

        [XmlIgnore]
        Stopwatch timer = new Stopwatch();

        public int Address { get; set; }//адрес устройства в сети

        //[XmlIgnore]
        public List<DataItem> DataItems { get; set; }//список переменных

        [XmlIgnore]
        public int NotAnswerCountForQuality { get; set; }

        [XmlIgnore]
        private SerialPort port;

        [XmlIgnore]
        public SerialPort SerialPort { get { return port; } set { port = value; } }

        [XmlIgnore]
        public Stopwatch Timer { get { return timer; } set { timer = value; } }

        public void ResetStatistic()
        {
            crcError = 0;
            unitAnswerError = 0;
            notAnswer = 0;
            emptyAnswer = 0;
            writeError = 0;
            comPortError = 0;
            readError = 0;
            goodAnswer = 0;
            allRequest = 0;
            incorrectAnswer = 0;
        }

        public void AddLog(string msgString, byte[] msgByte)
        {
            lock (Model.LogList)
            {
                for (int i = Model.LogList.Count; i > 1000; i--)
                {
                    //   if (Model.LogList.Count > 1000)
                    Model.LogList.RemoveAt(Model.LogList.Count - 1);
                }

                string resString = "";

                for (int i = 0; i < msgByte.Length; i++)
                {
                    resString = resString + msgByte[i] + "|";
                }

                resString = DateTime.Now + ": " + msgString + " " + resString;


                Model.LogList.Insert(0, resString);

                try
                {
                    if (UpdateListViewVirualItemsCount != null)
                        UpdateListViewVirualItemsCount(Model.LogList.Count, EventArgs.Empty);
                }
                catch(Exception ex)
                {
                    ErrorEventLog.LogError("AddLog.1 - " + ex.Message, Model.GetProgrammDir() + "\\Logs\\AddLog.txt");
                }
            }
        }

        public Device()
        {

        }

    }

    [Serializable]
    public class ModBusRTUUnit : Device
   {
       private ModBusRTU modBusRTU;

       [XmlIgnore]
       public ModBusRTU _ModBusRTU
       {
           get { return modBusRTU; }
       }
        
       public ModBusRTUUnit(SerialPort _serialPort, string portName, int address)
       {
           modBusRTU = new ModBusRTU();
           this.Address = address;
           DataItems = new List<DataItem>();
           SerialPort = _serialPort;
       }

        public ModBusRTUUnit()
        {

        }
    }

    /// <summary>
    /// Класс содержит поля одного контроллера
    /// </summary>
    [Serializable]
    public class CarelUnit : Device
   {
       private Carel carel;

       [XmlIgnore]
       public Carel _Carel
       {
           get { return carel; }
       }

       public CarelUnit(SerialPort _serialPort, string portName, int address)
       {

           SerialPort = _serialPort;
           carel = new Carel(_serialPort);
           DataItems = new List<DataItem>();

           for (int i = 0; i < 621; i++)
           {
               DataItems.Add(new DataItem(portName,
                    i + 1 < 208 ? i + 1 :
                        i + 1 > 207 & i + 1 < 415 ? i + 1 - 207 :
                            i + 1 - 414,
                    i + 1 < 208 ? DataItem.DataType.REAL :
                        i + 1 > 207 & i + 1 < 415 ? DataItem.DataType.INT : 
                            DataItem.DataType.BOOL, 0));
           }

           this.Address = address;
       }

        public CarelUnit()
        {

        }

    }
}
