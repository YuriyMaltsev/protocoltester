﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProtocolTester
{
    [Serializable]
    public class Settings
    {
        public decimal ReadWriteTimeout { get; set; }
        public decimal UpdateInterval { get; set; }

        public string SettingsPath { get; set; }//путь к вар файлам
        
        public bool AutoStartExchange { get; set; }
    }
}
