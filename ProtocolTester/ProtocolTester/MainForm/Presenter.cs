﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using ProtocolTester.SettingsFormNamespace;
using ProtocolTester.LoggingForm;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace ProtocolTester
{
    [Serializable]
    public class Presenter
    {
        [NonSerialized]
        private IView _mainForm;
        private Model model = new Model();
        private Model.Mode mode = new Model.Mode();

        public event EventHandler<ExtendedEventArgs> ChangeMode;
        
        /// <summary>
        /// Режим в котором работает программа
        /// </summary>
        public Model.Mode Mode
        { 
            get { return mode; }
            set 
            {
                if (ChangeMode != null)
                    ChangeMode(null, new ExtendedEventArgs(value));
                mode = value; 
            }
        }

        public Model _Model
        {
            get { return model; }
            set { model = value; }
        }


        public Presenter(MainForm mainForm)
        {
            _mainForm = mainForm;
            _mainForm.ScanerShowMenuClick +=new EventHandler<EventArgs>(_mainForm_ScanerShowMenuClick);
            _mainForm.NewItemAddingInListView += new EventHandler<EventArgs>(_mainForm_NewItemAddingInListView);
            _mainForm.ViewButtonsClick += new EventHandler<EventArgs>(_mainForm_ViewButtonsClick);
            _mainForm.DeleteDeviceFromListView += new EventHandler<EventArgs>(_mainForm_DeleteDeviceFromListView);
            _mainForm.TimeoutOrIntervalChange += new EventHandler<EventArgs>(_mainForm_TimeoutOrIntervalChange);
            _mainForm.ProgramStart += new EventHandler<EventArgs>(_mainForm_ProgramStart);
            _mainForm.listViewItemsRetrieveVirtualItem += new EventHandler<RetrieveVirtualItemEventArgs>(_mainForm_listViewItemsRetrieveVirtualItem);
            _mainForm.listViewDeviceSelectIndexChange += new EventHandler<EventArgs>(_mainForm_listViewDeviceSelectIndexChange);
            _mainForm.listViewDeviceUpdateColorForItemPutInTag += new EventHandler<DrawListViewSubItemEventArgs>(_mainForm_listViewDeviceUpdateColorForItemPutInTag);
            _mainForm.ResetStatistic += new EventHandler<EventArgs>(_mainForm_ResetStatistic);
            _mainForm.contextMenuStripListViewItems_ItemClick += new EventHandler<EventArgs>(_mainForm_contextMenuStripListViewItems_ItemClick);
            _mainForm.ListViewMouseDoubleClick += new EventHandler<MouseEventArgs>(_mainForm_ListViewMouseDoubleClick);
            _mainForm.ListViewItemColumnClick += new EventHandler<ColumnClickEventArgs>(_mainForm_ListViewItemColumnClick);
            _mainForm.ButtonSettingsClick += new EventHandler<EventArgs>(_mainForm_ButtonSettingsClick);
            _mainForm.UpdateSettingsRef += new EventHandler<EventArgs>(_mainForm_UpdateSettingsRef);
            _mainForm.OpcButtonsClick += new EventHandler<EventArgs>(_mainForm_OpcButtonsClick);
            _mainForm.LogButtonClick += new EventHandler<EventArgs>(_mainForm_LogButtonClick);
            _mainForm.ListViewItemsContextMenuOpening += _mainForm_ListViewItemsContextMenuOpening;
            _mainForm.ListViewItemsContextMenuDeleteGroup += _mainForm_ListViewItemsContextMenuDeleteGroup;
            _mainForm.Serializable += _mainForm_Serializable;
            _mainForm.DeSerializable += _mainForm_DeSerializable;

            
        }

        private void AcceptModelEvents()
        {
            model.ChangeListViewItemsVirtualItemsCount += new EventHandler<EventArgs>(model_ChangeListViewItemsVirtualItemsCount);
            model.RefreshListViewEvent += new EventHandler<EventArgs>(model_RefreshListViewEvent);
            model.UpdateStatistic += new EventHandler<EventArgs>(model_UpdateStatistic);
            model.ChangeButtonColor += new EventHandler<EventArgs>(model_ChangeButtonColor);
            model.ChangeOpcButtonPic += new EventHandler<EventArgs>(model_ChangeOpcButtonPic);
            model.UpdateDataForModBus += Model_UpdateDataForModBus;
            model.UpdateTimerState += Model_UpdateTimerState;
        }

        private void _mainForm_DeSerializable(object sender, EventArgs e)
        {
            bool flag = true;
            if (File.Exists("ConnectionLine485.xml"))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(Model));
                using (FileStream fs = new FileStream("ConnectionLine485.xml", FileMode.Open))
                {
                    model = (Model)formatter.Deserialize(fs);
                    AcceptModelEvents();
                    flag = false;
                }


                foreach(var line in model._ConnectionLine485List)
                {
                    if (line.BaudRate != 0 & line.PortName != "")
                    {
                        line.Port = new System.IO.Ports.SerialPort(line.PortName, line.BaudRate, line._Parity, line.DataBits, line._StopBits);

                        foreach (var device in line._Device)
                        {
                            device.SerialPort = line.Port;
                            _mainForm.InsertItemInListViewDevice(new string[] { line.PortName, device is CarelUnit ? (device.Address - 48).ToString() : device.Address.ToString(), line.DataBits.ToString(), line._Parity.ToString(), line._StopBits.ToString(), line.BaudRate.ToString(), line.Mode.ToString(), "0" });
                            model.UpdateListViewItemVurtualItemsCount();
                        }
                    }
                }

            }
            
            if (flag)
                AcceptModelEvents();
        }

        private void _mainForm_Serializable(object sender, EventArgs e)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Model));

            using (FileStream fs = new FileStream("ConnectionLine485.xml", FileMode.Create))
            {
                formatter.Serialize(fs, model);
            }



        }

        private void Model_UpdateTimerState(object sender, EventArgs e)
        {
            _mainForm.OnOffUpdateTimer((bool)sender);
        }

        private void _mainForm_ListViewItemsContextMenuDeleteGroup(object sender, EventArgs e)
        {
            if ((int)sender != -1)
            {
                Device dev = model.GetDataItemFromIndex((int)sender);

                if (dev != null)
                {
                    int groupIndex = dev.DataItems[dev.SelectIndex].Group;

                    dev.DataItems.RemoveAll((v) => v.Group == groupIndex);

                    model.UpdateListViewItemVurtualItemsCount();
                }
            }
        }

        private void _mainForm_ListViewItemsContextMenuOpening(object sender, ExtendedEventArgs e)
        {
            Device dev;
            if ((int)e.Control != -1)
            {
                dev = model.GetDataItemFromIndex((int)e.Control);


                if (dev != null)
                {
                    if (dev is ModBusRTUUnit)
                        ((ContextMenuStrip)sender).Items[1].Enabled = true;
                    else
                        ((ContextMenuStrip)sender).Items[1].Enabled = false;
                }

                ((ContextMenuStrip)sender).Items[0].Enabled = true;
            }
            else
            {
                ((ContextMenuStrip)sender).Items[0].Enabled = false;
                ((ContextMenuStrip)sender).Items[1].Enabled = false;
            }
        }

        private void Model_UpdateDataForModBus(object sender, EventArgs e)
        {
            model.ModBusFunction = _mainForm.comboBoxFunctionSelectedFunction;
            model.ModbusStartAddress = _mainForm.textBoxStartItemIndex;
            model.ModbusCount = _mainForm.comboBoxCountItems;
            model.ModBusDataType = _mainForm.comboBoxDataType;
        }

        void _mainForm_LogButtonClick(object sender, EventArgs e)
        {
            LogForm lf = new LogForm();
            PLoggingForm pLoggingForm = new PLoggingForm(lf, this);

            lf.StartPosition = FormStartPosition.Manual;
            lf.Show();   
        }

        void model_ChangeOpcButtonPic(object sender, EventArgs e)
        {
            _mainForm.ChangeButtonPic(sender);
        }

        void _mainForm_OpcButtonsClick(object sender, EventArgs e)
        {
            
            if (((ToolStripButton)sender).Name != "toolStripButton2")
                model.RunOPC();
            else
            {
                if (model.bMCRemedy == null)
                    model.bMCRemedy = new BMCRemedy(model._bMCSettings.BMCServer, model._bMCSettings);

                model._bMCSettings.Description = "Нет связи с контроллером, адресс 7.";
                if (model._bMCSettings.FunctionActivated)
                {
                    Task carelTask = Task.Factory.StartNew(() =>
                                        {
                                            System.Threading.Thread.Sleep(model._bMCSettings.Timeout * 1000);

                                            //model._bMCSettings.BMCServer = model._bMCSettings.BMCServer ?? new BMC.ARSystem.Server();
                                            model.bMCRemedy._BMCServer = model.bMCRemedy._BMCServer ?? new BMC.ARSystem.Server();

                                            lock (model.bMCRemedy._BMCServer)
                                            {
                                                //model.bMCRemedy.CreateIncidentBMC(model.bMCRemedy, unit);
                                            }
                                        });
                }
            }
        }

        void _mainForm_UpdateSettingsRef(object sender, EventArgs e)
        {
            model._Settings = ((Settings)sender);

            XmlSerializer serializer = new XmlSerializer(typeof(BMCSettings));
            if (File.Exists("BMCData.xml"))
            {
                StreamReader reader = new StreamReader("BMCData.xml");
                model._bMCSettings = (BMCSettings)serializer.Deserialize(reader);
                reader.Close();

                byte[] entropyBytes = Encoding.Unicode.GetBytes(Environment.UserName);

                // Read the secret data from the file
                byte[] encryptedData = ReadBytesFromFile();
                // TODO: Decrypt the data
                try
                {
                    byte[] decryptedData = ProtectedData.Unprotect(encryptedData, entropyBytes, DataProtectionScope.CurrentUser);
                    model._bMCSettings.Password = Encoding.Unicode.GetString(decryptedData, 0, decryptedData.Length);
                }
                catch (CryptographicException ce)
                {
                    ErrorEventLog.LogError("CryptographicException: " + ce.Message, Model.GetProgrammDir() + "\\Logs\\Serialization.txt");
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError("Exception: " + ex.Message, Model.GetProgrammDir() + "\\Logs\\Serialization.txt");
                }
            }
            else
            {
                model._bMCSettings = new BMCSettings();
            }
        }

        // Reads an array of bytes from a file
        static byte[] ReadBytesFromFile()
        {
            string path = "ProtocolTester.dat";

            // Delete the file if it exists
            if (!File.Exists(path))
            {
                return null;
            }

            //Create the file and write the bytes. Use of 'using' will close
            // the stream at the end of the block
            int nRead = 0;
            byte[] b = new byte[1024];
            using (FileStream fs = File.OpenRead(path))
            {

                nRead = fs.Read(b, 0, b.Length);

            }

            if (nRead > 0)
            {

                byte[] b2 = new byte[nRead];
                Array.Copy(b, b2, nRead);
                return b2;

            }
            else

                return null;

        }

        void _mainForm_ButtonSettingsClick(object sender, EventArgs e)
        {
            SettingsForm sf = new SettingsForm(((Settings)sender).SettingsPath);
            PSettingsForm pSettingsForm = new PSettingsForm(((Settings)sender), sf, this);
            sf.StartPosition = FormStartPosition.CenterScreen;
            sf.ShowDialog();
        }

        void _mainForm_ListViewItemColumnClick(object sender, ColumnClickEventArgs e)
        {
            model.ListViewItemColumnClick(sender, e);
        }

        void _mainForm_ListViewMouseDoubleClick(object sender, MouseEventArgs e)
        {
            model.DoubleClickListViewEvent(sender, e);
        }

        void _mainForm_contextMenuStripListViewItems_ItemClick(object sender, EventArgs e)
        {
            if (_mainForm.ListViewItemsSelectedItemIndex != null)
            {
                Device d = model.GetDataItemFromIndex((int)_mainForm.ListViewItemsSelectedItemIndex);

                if (d != null)
                {
                    WriteForm wf = new WriteForm(d);
                    PWriteForm pWriteForm = new PWriteForm(wf, this);
                    wf.Location = Model.GetCursorPosition();
                    wf.Location = new Point(wf.Location.X - 100, wf.Location.Y - 35);
                    wf.StartPosition = FormStartPosition.Manual;
                    wf.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Не удалось определить контроллер в тором требуется изменить параметр.");
                }
            }
            else
            {
                MessageBox.Show("Не удалось определить изменяемый параметр.");
            }
        }

        void model_ChangeButtonColor(object sender, EventArgs e)
        {
            _mainForm.ChangeButtonColor(model.AutoModeIndicator ? Color.LightGreen : Color.FromName("Control"), "buttonStartScan");
        }

        void _mainForm_ResetStatistic(object sender, EventArgs e)
        {
            model.ResetStatistic();
        }


        /// <summary>
        /// Получение цвета для лист вью айтема из соответствующего экземпляра девайс. Используется только при добавлении устройства.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _mainForm_listViewDeviceUpdateColorForItemPutInTag(object sender, DrawListViewSubItemEventArgs e)
        {
            var device = model.GetDeviceFromListViewItem(e.Item);

            if (e.Item.ForeColor != device._Color)
            {
                e.Item.ForeColor = device._Color;
                _mainForm.RefreshListView("listViewDevice");
            }
        }

        void _mainForm_listViewDeviceSelectIndexChange(object sender, EventArgs e)
        {
            List<Device> dev = sender != null ? model.GetDeviceFromListViewItems((ListView.SelectedListViewItemCollection)sender) : null;
            _mainForm.UpdateSelectedDevice(dev);
            _Model.SelectedDeviceInDeviceListView = dev;
        }

        void model_UpdateStatistic(object sender, EventArgs e)
        {
            _mainForm.UpdateStatistic((Device)sender);
        }

        void model_RefreshListViewEvent(object sender, EventArgs e)
        {
            _mainForm.RefreshListView("listViewItem");
        }

        void model_ChangeListViewItemsVirtualItemsCount(object sender, EventArgs e)
        {
            _mainForm.ListViewItemsVirtualListSize = (int)sender;
        }

        void _mainForm_listViewItemsRetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            model.ListViewRetrieveVirtualItem(sender, e);
        }

        void _mainForm_ProgramStart(object sender, EventArgs e)
        {
            MainForm.ProgrammVersion = model.GetProgramVersion();
        }

        void _mainForm_TimeoutOrIntervalChange(object sender, EventArgs e)
        {
            if (((NumericUpDown)sender).Name == "numericUpDownRWTimeout")
            {
                model.ReadWriteTimeout = ((NumericUpDown)sender).Value;
                _mainForm.WriteReadTimeout = model.ReadWriteTimeout;
            }
            else
                if (((NumericUpDown)sender).Name == "numericUpDownUpdateInterval")
                {
                    model.UpdateInterval = ((NumericUpDown)sender).Value;
                    _mainForm.UpdateInterval = model.UpdateInterval;
                }
        }

        void _mainForm_DeleteDeviceFromListView(object sender, EventArgs e)
        {
            model.DeleteDeviceFromLine((ListViewItem)sender);
        }

        void _mainForm_ViewButtonsClick(object sender, EventArgs e)
        {
            model.ViewButtonsClick(sender, e);
        }

        void _mainForm_NewItemAddingInListView(object sender, EventArgs e)
        {
            model.UpdateDevices((string[])sender);
        }

        void _mainForm_ScanerShowMenuClick(object sender, EventArgs e)
        {
            ScanForm scanForm = new ScanForm();
            PScanForm pScanForm = new PScanForm(scanForm, this);
            scanForm.StartPosition = FormStartPosition.CenterScreen;
            pScanForm.ShowForm();
        }

        public void SaveBMCSettingsRef(BMCSettings bMCSettings)
        {
            model._bMCSettings = bMCSettings;
        }

    }
}
