﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;
using System.Diagnostics;


namespace ProtocolTester
{
    public partial class MainForm : Form, IView
    {
        public static string ProgrammVersion { get;set; }
        Settings settings;
        
        public MainForm()
        {
            InitializeComponent();

            Device.InvalidateListViewDevice += new EventHandler<EventArgs>(Device_InvalidateListViewDevice);
        }

        void Device_InvalidateListViewDevice(object sender, EventArgs e)
        {
            if (ConnectionLine485.AllRequest > 1000000000000)
                resetToolStripMenuItem_Click(sender, e);

            if (listViewDevice.InvokeRequired)
                listViewDevice.Invoke(new Action(() =>
                {
                    string address = ((object[])sender)[0] is CarelUnit ? (((Device)(((object[])sender)[0])).Address - 48).ToString() : ((Device)(((object[])sender)[0])).Address.ToString();

                    for (int i = 0; i < listViewDevice.Items.Count; i++)
                    {
                        if (listViewDevice.Items[i].SubItems[1].Text == address)
                        {
                            listViewDevice.Items[i].SubItems[7].Text = ((object[])sender)[1].ToString();
                            break;
                        }
                    }

                    listViewDevice.Refresh();
                }));
            else
                listViewDevice.Refresh();
        }

        private Device _selectedDevice;

        public event EventHandler<EventArgs> ViewButtonsClick;
        public event EventHandler<EventArgs> ScanerShowMenuClick;
        public event EventHandler<EventArgs> NewItemAddingInListView;
        public event EventHandler<EventArgs> DeleteDeviceFromListView;
        public event EventHandler<EventArgs> TimeoutOrIntervalChange;
        public event EventHandler<EventArgs> ProgramStart;
        public event EventHandler<RetrieveVirtualItemEventArgs> listViewItemsRetrieveVirtualItem;
        public event EventHandler<EventArgs> listViewDeviceSelectIndexChange;
        public event EventHandler<DrawListViewSubItemEventArgs> listViewDeviceUpdateColorForItemPutInTag;
        public event EventHandler<EventArgs> ResetStatistic;
        public event EventHandler<EventArgs> contextMenuStripListViewItems_ItemClick;
        public event EventHandler<MouseEventArgs> ListViewMouseDoubleClick;
        public event EventHandler<ColumnClickEventArgs> ListViewItemColumnClick;
        public event EventHandler<EventArgs> ButtonSettingsClick;
        public event EventHandler<EventArgs> UpdateSettingsRef;
        public event EventHandler<EventArgs> OpcButtonsClick;
        public event EventHandler<EventArgs> LogButtonClick;
        public event EventHandler<ExtendedEventArgs> ListViewItemsContextMenuOpening;
        public event EventHandler<EventArgs> ListViewItemsContextMenuDeleteGroup;
        public event EventHandler<EventArgs> Serializable;
        public event EventHandler<EventArgs> DeSerializable;

        public void ChangeButtonColor(Color color, object sender)
        {
            if ((string)sender == "buttonStartScan")
            {
                buttonStartScan.BackColor = color;
            }
        }

        public int? ListViewItemsSelectedItemIndex
        {
            get
            {
                return listViewItems.SelectedIndices.Count > 0 ? (int?)listViewItems.SelectedIndices[0] : null;
            }
        }

        public void RefreshListView(string listViewName)
        {
            if (listViewName == "listViewItem")
            {
                if (listViewItems.InvokeRequired)
                    listViewItems.Invoke(new Action(() => listViewItems.Refresh()));
                else
                    listViewItems.Refresh();
            }
            else
                if (listViewName == "listViewDevice")
                {
                    if (listViewDevice.InvokeRequired)
                        listViewDevice.Invoke(new Action(() => listViewDevice.Refresh()));
                    else
                        listViewDevice.Refresh();
                }
        }

        public int ListViewItemsVirtualListSize
        {
            get { return listViewItems.VirtualListSize; }
            set { listViewItems.VirtualListSize = value; }
        }

        public decimal UpdateInterval
        {
            get { return numericUpDownUpdateInterval.Value; }
            set { settings.UpdateInterval = value; }
        }

        public  decimal WriteReadTimeout
        {
            get { return numericUpDownRWTimeout.Value; }
            set { settings.ReadWriteTimeout = value; }
        }

        public int comboBoxFunctionSelectedFunction
        {
            get
            {
                return comboBoxFunction.Text != "" ? int.Parse(comboBoxFunction.Text) : 0;
            }
        }

        public int textBoxStartItemIndex
        {
            get
            {
                return textBoxStart.Text != "" ? int.Parse(textBoxStart.Text) : 0;
            }
        }

        public int comboBoxCountItems
        {
            get
            {
                return comboBoxCount.Text != "" ? int.Parse(comboBoxCount.Text) : 0;
            }
        }

        DataItem.DataType IView.comboBoxDataType
        {
            get
            {
                return comboBoxDataType.SelectedItem.ToString() == "BOOL" ? DataItem.DataType.BOOL :
                            comboBoxDataType.SelectedItem.ToString() == "INT" ? DataItem.DataType.INT :
                                DataItem.DataType.REAL;
            }
        }

        public ListViewItem listViewDeviceSelectedItem
        {
            get
            {
                return listViewDevice.SelectedItems.Count > 0 ? listViewDevice.SelectedItems[0] : null;
            }
        }

        private void Buttons_Click(object sender, EventArgs e)
        {
            
            if (listViewDevice.Items.Count > 0)
                if (ViewButtonsClick != null)
                    ViewButtonsClick(sender, e);
        }

        private void сканерToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ScanerShowMenuClick != null)
                ScanerShowMenuClick(sender, e);
        }

        private void listViewScanResult_DragDrop(object sender, DragEventArgs e)
        {
            var c = e.Data.GetFormats();
            if (c.Length > 0)
            {
                List<string[]> dragData = (List<string[]>)e.Data.GetData(c[0]);
                string message = "";
              
                foreach (string[] line in dragData)
                {
                    bool flag = true;

                    foreach (var v in listViewDevice.Items)
                    {
                        if (((ListViewItem)v).SubItems[0].Text == line[0] &
                            ((ListViewItem)v).SubItems[1].Text == line[1] &
                            ((ListViewItem)v).SubItems[2].Text == line[2] &
                            ((ListViewItem)v).SubItems[3].Text == line[3] &
                            ((ListViewItem)v).SubItems[4].Text == line[4] &
                            ((ListViewItem)v).SubItems[5].Text == line[5] &
                            ((ListViewItem)v).SubItems[6].Text == line[6] &
                            ((ListViewItem)v).SubItems[7].Text == line[7])
                        {
                            message = "Устройство уже добавлено.";
                            flag = false;
                        }

                        if (((ListViewItem)v).SubItems[0].Text == line[0] & ((ListViewItem)v).SubItems[6].Text != line[6])
                        {
                            message = "Использование разных протоколов на одной линии запрещено.";
                            flag = false;
                        }
                    }

                    if (flag)
                    {
                        listViewDevice.Items.Add(new ListViewItem(line));
                        if (NewItemAddingInListView != null)
                        {
                            NewItemAddingInListView(line, EventArgs.Empty);
                        }
                    }
                    else
                    {
                        MessageBox.Show(message);
                        break;
                    }
                }
            }
        }

        private void listViewScanResult_DragEnter(object sender, DragEventArgs e)
        {
            var v = e.Data.GetData(e.Data.GetFormats()[0]);
            var t = v.GetType();

            if (v is ListView.SelectedListViewItemCollection)
            {
                e.Effect = DragDropEffects.None;
            }
            else

            if (e.Data.GetData(typeof(ListView)) == null)
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void listViewDevice_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                ((ListView)sender).DoDragDrop(((ListView)sender).SelectedItems, DragDropEffects.All);
            }
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            var v = e.Data.GetData(e.Data.GetFormats()[0]);
            var t = v.GetType();

            if (v is ListView.SelectedListViewItemCollection)
            {
                e.Effect = DragDropEffects.Move;

                foreach (ListViewItem item in listViewDevice.SelectedItems)
                {
                    listViewDevice.Items.Remove(item);
                    if (DeleteDeviceFromListView != null)
                        DeleteDeviceFromListView(item, EventArgs.Empty);
                }
            }
        }

        private void TimeoutOrIntervelChange(object sender, EventArgs e)
        {
            if (TimeoutOrIntervalChange != null)
            {
                TimeoutOrIntervalChange(sender, e);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            comboBoxFunction.SelectedIndex = 2;
            comboBoxCount.SelectedIndex = 49;
            comboBoxDataType.SelectedIndex = 2;
            if (ProgramStart != null)
                ProgramStart(null, EventArgs.Empty);

            this.Text = this.Text + " v." + ProgrammVersion;

            listViewItems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            columnHeaderValuePort, columnHeaderValueAddress, columnHeaderValueVariable, columnHeaderValueGroup, columnHeaderValueFunction, columnHeaderValueValue, columnHeaderValueTimeStamp, columnHeaderValueOldValue, columnHeaderValueOldTimeStamp, columnHeaderValueQuality, columnHeaderValueType, columnHeaderValueOPC, columnHeaderValueOPCName, columnHeaderValueOPCAccess});

            try
            {
                try
                {
                    if (DeSerializable != null)
                        DeSerializable(sender, e);
                }
                catch (Exception ex)
                {
                    string s = ex.Message + Environment.NewLine + ex.InnerException;
                    MessageBox.Show(s);
                    ErrorEventLog.LogError("MainForm_Load.1 - " + s, Model.GetProgrammDir() + "\\Logs\\Serialization.txt");
                }

                settings = new Settings();

                XmlSerializer serializer = new XmlSerializer(typeof(Settings));

                if (File.Exists(Model.GetProgrammDir() + "\\Settings.xml"))
                {
                    StreamReader reader = new StreamReader(Model.GetProgrammDir() + "\\Settings.xml");
                    settings = (Settings)serializer.Deserialize(reader);
                    reader.Close();


                }
                
                if (UpdateSettingsRef != null)
                    UpdateSettingsRef(settings, e);

                numericUpDownRWTimeout.Value = settings.ReadWriteTimeout;
                numericUpDownUpdateInterval.Value = settings.UpdateInterval;

                TimeoutOrIntervelChange(numericUpDownRWTimeout, EventArgs.Empty);
                TimeoutOrIntervelChange(numericUpDownUpdateInterval, EventArgs.Empty);

                if (settings.AutoStartExchange)
                {
                    buttonStartScan.PerformClick();
                    toolStripButtonOnOffOPC.PerformClick();
                }

            }
            catch(Exception ex)
            {
                ErrorEventLog.LogError("MainForm_Load.1 - " + ex.Message, Model.GetProgrammDir() + "\\Logs\\Serialization.txt");
            }

            chartPie.ChartAreas[0].Area3DStyle.Enable3D = true;
            chartPie.Series[0]["PieLabelStyle"] = "Outside";
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                XmlSerializer xml = new XmlSerializer(typeof(Settings));
                using (var fStream = new FileStream(Model.GetProgrammDir() + "\\Settings.xml", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    xml.Serialize(fStream, settings);
                }

                if (Serializable != null)
                    Serializable(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ошибка при сохранении конфигурации.\n" + ex.Message);
            }
        }

        private void listViewItems_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            if (e.Header.Width == 0)
            {
                e.Header.Text = "";
            }
            else
            {
                using (StringFormat sf = new StringFormat())
                {
                    switch (e.Header.TextAlign)
                    {
                        case HorizontalAlignment.Center:
                            sf.Alignment = StringAlignment.Center;
                            break;
                        case HorizontalAlignment.Left:
                            sf.Alignment = StringAlignment.Center;
                            break;
                    }

                    Rectangle rct = e.Bounds;
                    rct.Height = rct.Height;

                    e.Graphics.FillRectangle(Brushes.LightGray, rct);
                    e.Graphics.DrawRectangle(new Pen(Color.Gray, 1), rct);

                    using (Font headerFont = new Font("Helvetica", 9))
                    {
                        e.Graphics.DrawString(e.Header.Text, headerFont,
                            Brushes.Black, rct, sf);
                    }
                }
                return;
            }
        }

        private void listViewItems_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            if (listViewItemsRetrieveVirtualItem != null)
                listViewItemsRetrieveVirtualItem(sender, e); 
        }

        private void listViewItems_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            listViewItems.Invalidate();
        }


        public void UpdateStatistic(Device device)
        {
            Device selectedDevice = _selectedDevice;

            try
            {
                int numberOfPointsInChart = 100;
                int numberOfPointsAfterRemoval = numberOfPointsInChart - 1;

                double reqTime = device.Timer != null ? Math.Round((double)device.Timer.ElapsedMilliseconds / 1000, 3) : Math.Round((double)0, 3);


                if (chartLine.InvokeRequired)
                {
                    chartLine.Invoke(new Action(() =>
                        {
                            try
                            {
                                
                                if (chartLine.Series[chartLine.Series.Count - 1].Points.Count > 10)
                                {
                                    double min = 9999, max = 0, avr = 0;
                                    foreach (var v in chartLine.Series[chartLine.Series.Count - 1].Points)
                                    {
                                        if (v.YValues[0] > max)
                                            max = v.YValues[0];

                                        if (v.YValues[0] < min)
                                            min = v.YValues[0];

                                        avr = avr + v.YValues[0];
                                    }

                                    if (reqTime > max)
                                        max = reqTime;
                                    if (reqTime < min)
                                        min = reqTime;

                                    if (max != 0 & min != 0)
                                    {
                                        chartLine.ChartAreas["ChartArea1"].AxisY.Maximum = max;
                                        chartLine.ChartAreas["ChartArea1"].AxisY.Minimum = min;
                                    }
                                     
                                }


                                
                                chartLine.Series[chartLine.Series.Count - 1].Points.AddXY(ConnectionLine485.AllRequest, reqTime);

                            }
                            catch(Exception ex)
                            {
                                ErrorEventLog.LogError("UpdateStatistic.1 - " + ex.Message, Model.GetProgrammDir() + "\\Logs\\UpdateStatistic.txt");
                            }
                        }));
                }
                else
                    chartLine.Series[chartLine.Series.Count - 1].Points.AddXY(ConnectionLine485.AllRequest, reqTime);


                chartPie.Invoke(new Action(() =>
                {
                    {
                        
                        double[] yValues;
                        string[] xValues;

                        if (selectedDevice != null)
                        {
                            yValues = new double[] { selectedDevice.GoodAnswer, selectedDevice.CrcError, selectedDevice.EmptyAnswer, selectedDevice.NotAnswer, selectedDevice.ReadError, selectedDevice.UnitAnswerError, selectedDevice.WriteError, selectedDevice.СomPortError, selectedDevice.IncorrectAnswer };
                            xValues = new string[] { "Good req: " + selectedDevice.GoodAnswer.ToString(), 
                                         "CRC error: " + selectedDevice.CrcError.ToString(),
                                         "Empty answer: " + selectedDevice.EmptyAnswer.ToString(),
                                         "No answer: " + selectedDevice.NotAnswer.ToString(),
                                         "Read error: " + selectedDevice.ReadError.ToString(),
                                         "Error answer: " + selectedDevice.UnitAnswerError.ToString(),
                                         "Write error: " + selectedDevice.WriteError.ToString(),
                                         "Port error: " + selectedDevice.СomPortError.ToString(),
                                         "Incorrect answer: " + selectedDevice.IncorrectAnswer.ToString()};
                        }
                        else
                        {
                            yValues = new double[] { ConnectionLine485.GoodAnswer, ConnectionLine485.CrcError, ConnectionLine485.EmptyAnswer, ConnectionLine485.NotAnswer, ConnectionLine485.ReadError, ConnectionLine485.UnitAnswerError, ConnectionLine485.WriteError, ConnectionLine485.СomPortError, ConnectionLine485.IncorrectAnswer };
                            xValues = new string[] { "Good req: " + ConnectionLine485.GoodAnswer.ToString(), 
                                         "CRC error: " + ConnectionLine485.CrcError.ToString(),
                                         "Empty answer: " + ConnectionLine485.EmptyAnswer.ToString(),
                                         "No answer: " + ConnectionLine485.NotAnswer.ToString(),
                                         "Read error: " + ConnectionLine485.ReadError.ToString(),
                                         "Error answer: " + ConnectionLine485.UnitAnswerError.ToString(),
                                         "Write error: " + ConnectionLine485.WriteError.ToString(),
                                         "Port error: " + ConnectionLine485.СomPortError.ToString(),
                                         "Incorrect answer: " + ConnectionLine485.IncorrectAnswer.ToString()};
                        }


                        chartPie.Series[0].Points.DataBindXY(xValues, yValues);

                        
                            chartPie.Series[0].Points[0].Color = Color.Green;
                            chartPie.Series[0].Points[0].Name = "Good req";
                            chartPie.Series[0].Points[0].ToolTip = "Корректно выполненные операции.";

                            chartPie.Series[0].Points[1].Color = Color.Red;
                            chartPie.Series[0].Points[1].Name = "CRC error";
                            chartPie.Series[0].Points[1].ToolTip = "Ошибка расчета контрольной суммы.";

                            chartPie.Series[0].Points[2].Color = Color.Yellow;
                            chartPie.Series[0].Points[2].Name = "Empty answer";
                            chartPie.Series[0].Points[2].ToolTip = "Пустой ответ. С момента последнего запроса изменения не происходили (код 0).";

                            chartPie.Series[0].Points[3].Color = Color.Gray;
                            chartPie.Series[0].Points[3].Name = "No answer";
                            chartPie.Series[0].Points[3].ToolTip = "Устройство не отвечает, не доступно.";

                            chartPie.Series[0].Points[4].Color = Color.Orange;
                            chartPie.Series[0].Points[4].Name = "Read error";
                            chartPie.Series[0].Points[4].ToolTip = "Ошибка чтения из порта.";

                            chartPie.Series[0].Points[5].Color = Color.OrangeRed;
                            chartPie.Series[0].Points[5].Name = "Error answer";
                            chartPie.Series[0].Points[5].ToolTip = "Устройство не корректно обработало полученные данные и ответило ошибкой (код 7).";

                            chartPie.Series[0].Points[6].Color = Color.IndianRed;
                            chartPie.Series[0].Points[6].Name = "Write error";
                            chartPie.Series[0].Points[6].ToolTip = "Ошибка записи в порт.";

                            chartPie.Series[0].Points[7].Color = Color.Black;
                            chartPie.Series[0].Points[7].Name = "Port error";
                            chartPie.Series[0].Points[7].ToolTip = "Порт не открыт или к нему отсутствует доступ.";

                            chartPie.Series[0].Points[8].Color = Color.MediumSlateBlue;
                            chartPie.Series[0].Points[8].Name = "Incorrect answer";
                            chartPie.Series[0].Points[8].ToolTip = "Не корректный ответ от устройства. Возможно в сети есть устройство работающее по другому протоколу.";

                        chartPie.Series[0].ChartType = SeriesChartType.Doughnut;
                        chartPie.Series[1].ChartType = SeriesChartType.Doughnut;
                        chartPie.Series[2].ChartType = SeriesChartType.Doughnut;
                        chartPie.Series[3].ChartType = SeriesChartType.Doughnut;
                        chartPie.Series[4].ChartType = SeriesChartType.Doughnut;
                        chartPie.Series[5].ChartType = SeriesChartType.Doughnut;
                        chartPie.Series[6].ChartType = SeriesChartType.Doughnut;
                        chartPie.Series[7].ChartType = SeriesChartType.Doughnut;
                    }

                    // Keep a constant number of points by removing them from the left
                    if (ConnectionLine485.AllRequest > 100)
                    while (chartLine.Series[chartLine.Series.Count - 1].Points.Count > numberOfPointsInChart)
                    {
                        // Remove data points on the left side
                        while (chartLine.Series[chartLine.Series.Count - 1].Points.Count > numberOfPointsAfterRemoval)
                        {
                            chartLine.Series[chartLine.Series.Count - 1].Points.RemoveAt(0);
                        }

                        // Adjust X axis scale
                        chartLine.ChartAreas["ChartArea1"].AxisX.Minimum = ConnectionLine485.AllRequest - (ulong)numberOfPointsInChart;
                        chartLine.ChartAreas["ChartArea1"].AxisX.Maximum = ConnectionLine485.AllRequest;// chartLine.ChartAreas["ChartArea1"].AxisX.Minimum + numberOfPointsInChart;
                    }

                    chartLine.Invalidate();
                }));
 
            }
            catch(Exception ex)
            {
                ErrorEventLog.LogError("UpdateStatistic.2 - " + ex.Message, Model.GetProgrammDir() + "\\Logs\\UpdateStatistic.txt");
            }
            device.Timer.Stop();
            device.Timer.Reset();
        }


        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (ViewButtonsClick != null)
                ViewButtonsClick(sender, e);
        }

        public void UpdateSelectedDevice(List<Device> devices)
        {
            if (devices != null)
            {
                foreach (Device device in devices)
                {
                    _selectedDevice = device;

                    if (device is ModBusRTUUnit)
                    {
                        groupBox11.Enabled = true;
                        groupBox12.Enabled = true;
                        groupBox13.Enabled = true;
                        buttonAdd.Enabled = true;
                    }
                    else
                    {
                        groupBox11.Enabled = false;
                        groupBox12.Enabled = false;
                        groupBox13.Enabled = false;
                        buttonAdd.Enabled = false;
                    }
                }
            }
            else
            {
                _selectedDevice = null;
            }
        }

        private void listViewDevice_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (((ListView)sender).SelectedItems.Count > 0)
            {
                if (listViewDeviceSelectIndexChange != null)
                    listViewDeviceSelectIndexChange(((ListView)sender).SelectedItems, e);
            }
            else
            {
                if (listViewDeviceSelectIndexChange != null)
                    listViewDeviceSelectIndexChange(null, e);
            }
        }

        private void listViewDevice_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            if (listViewDeviceUpdateColorForItemPutInTag != null)
                listViewDeviceUpdateColorForItemPutInTag(sender, e);

            using (StringFormat sf = new StringFormat())
            {
                if (e.Item.Selected)
                {
                    e.Graphics.FillRectangle(new SolidBrush(Color.Blue), e.Bounds);
                    e.Graphics.DrawString(e.SubItem.Text, ((ListView)sender).Font, new SolidBrush(System.Drawing.ColorTranslator.FromHtml(System.Drawing.ColorTranslator.ToHtml(Color.White))), e.Bounds, sf);
                }
                else
                {
                    e.Graphics.FillRectangle(new SolidBrush((Color)e.Item.ForeColor), e.Bounds);
                   if (e.Item.ForeColor == Color.Black)
                       e.Graphics.DrawString(e.SubItem.Text, ((ListView)sender).Font, new SolidBrush(Color.White), e.Bounds, sf);  
                    else
                    e.Graphics.DrawString(e.SubItem.Text,
                                ((ListView)sender).Font, new SolidBrush(Color.Black), e.Bounds, sf );
                }
            }
        }

        private void listViewDevice_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ResetStatistic != null)
            {
                System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
                chartArea1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
                chartArea1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
                chartArea1.BackSecondaryColor = System.Drawing.Color.White;
                chartArea1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
                chartArea1.Name = "ChartArea1";
                System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
                legend1.BackColor = System.Drawing.Color.Transparent;
                legend1.Enabled = false;
                legend1.IsTextAutoFit = false;
                legend1.Name = "Legend1";
                Series series1 = new Series();
                series1.ChartArea = "ChartArea1";
                series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                series1.Legend = "Legend1";
                series1.Name = "Series1";

                if (chartLine.InvokeRequired)
                    chartLine.Invoke(new Action(() =>
                    {
                        chartLine.ChartAreas[0] = chartArea1;
                        chartLine.Legends[0] = legend1;
                        chartLine.Series[0] = series1;
                    }));
                else
                {
                    chartLine.ChartAreas[0] = chartArea1;
                    chartLine.Legends[0] = legend1;
                    chartLine.Series[0] = series1;
                }
                

                ResetStatistic(sender, e);

                double[] yValues;
                string[] xValues;

                yValues = new double[] { ConnectionLine485.GoodAnswer, ConnectionLine485.CrcError, ConnectionLine485.EmptyAnswer, ConnectionLine485.NotAnswer, ConnectionLine485.ReadError, ConnectionLine485.UnitAnswerError, ConnectionLine485.WriteError, ConnectionLine485.СomPortError, ConnectionLine485.IncorrectAnswer };
                xValues = new string[] { "Good req: " + ConnectionLine485.GoodAnswer.ToString(), 
                                         "CRC error: " + ConnectionLine485.CrcError.ToString(),
                                         "Empty answer: " + ConnectionLine485.EmptyAnswer.ToString(),
                                         "No answer: " + ConnectionLine485.NotAnswer.ToString(),
                                         "Read error: " + ConnectionLine485.ReadError.ToString(),
                                         "Error answer: " + ConnectionLine485.UnitAnswerError.ToString(),
                                         "Write error: " + ConnectionLine485.WriteError.ToString(),
                                         "Port error: " + ConnectionLine485.СomPortError.ToString(),
                                         "Incorrect answer: " + ConnectionLine485.IncorrectAnswer.ToString()};

                if (chartPie.InvokeRequired)
                    chartPie.Invoke(new Action(() => chartPie.Series[0].Points.DataBindXY(xValues, yValues)));
                else
                    chartPie.Series[0].Points.DataBindXY(xValues, yValues);
            }
        }

        private void записатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contextMenuStripListViewItems_ItemClick != null)
                contextMenuStripListViewItems_ItemClick(sender, e);
        }

        private void listViewItems_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (ListViewMouseDoubleClick != null)
                ListViewMouseDoubleClick(sender, e);
        }

        private void listViewItems_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (ListViewItemColumnClick != null)
                ListViewItemColumnClick(sender, e);
        }

        private void toolStripButtonSettings_Click(object sender, EventArgs e)
        {
            if (ButtonSettingsClick != null)
                ButtonSettingsClick(settings, e);
        }

        private void toolStripButtonOnOffOPC_Click(object sender, EventArgs e)
        {
            if (listViewDevice.Items.Count > 0)
                toolStripButtonOnOffOPC.Image = Properties.Resources.opcStartStop;

            if (OpcButtonsClick != null)
                OpcButtonsClick(sender, e);
        }

        public void ChangeButtonPic(object sender)
        {
            toolStripButtonOnOffOPC.Image = ((string)sender) == "opcON.png" ? Properties.Resources.opcON : Properties.Resources.opcOFF;
        }

        private void toolStripButtonLog_Click(object sender, EventArgs e)
        {
            if (LogButtonClick != null)
                LogButtonClick(sender, e);
        }

        private void contextMenuStripListViewItems_Opening(object sender, CancelEventArgs e)
        {
            if (ListViewItemsContextMenuOpening != null)
                ListViewItemsContextMenuOpening(sender, new ExtendedEventArgs(listViewItems.SelectedIndices.Count > 0 ? listViewItems.SelectedIndices[0] : -1));
        }

        private void удалитьГруппуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ListViewItemsContextMenuDeleteGroup != null)
                ListViewItemsContextMenuDeleteGroup(listViewItems.SelectedIndices.Count > 0 ? listViewItems.SelectedIndices[0] : -1, e);
        }

        public void OnOffUpdateTimer(bool onOff)
        {
            if (onOff)
            {
                timer1.Enabled = true;
                timer1.Start();
            }
            else
            {
                timer1.Stop();
                timer1.Enabled = false;
            }
        }

        public void InsertItemInListViewDevice(string[] device)
        {
            listViewDevice.Items.Add(new ListViewItem(device));
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (OpcButtonsClick != null)
                OpcButtonsClick(sender, e);
        }
    }
}
