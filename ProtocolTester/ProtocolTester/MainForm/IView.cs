﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProtocolTester
{
    public interface IView
    {
        //СВОЙСТВА//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        decimal WriteReadTimeout { get; set; }
        decimal UpdateInterval { get; set; }
        int ListViewItemsVirtualListSize { get; set; }
        int? ListViewItemsSelectedItemIndex { get;}
        ListViewItem listViewDeviceSelectedItem { get; }
        int comboBoxFunctionSelectedFunction { get; }
        int textBoxStartItemIndex { get; }
        int comboBoxCountItems { get; }
        DataItem.DataType comboBoxDataType { get; }

        //СОБЫТИЯ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event EventHandler<EventArgs> LogButtonClick;
        event EventHandler<EventArgs> ViewButtonsClick;
        event EventHandler<EventArgs> OpcButtonsClick;
        event EventHandler<EventArgs> ScanerShowMenuClick;
        event EventHandler<EventArgs> NewItemAddingInListView;
        event EventHandler<EventArgs> DeleteDeviceFromListView;
        event EventHandler<EventArgs> TimeoutOrIntervalChange;
        event EventHandler<EventArgs> ProgramStart;
        event EventHandler<RetrieveVirtualItemEventArgs> listViewItemsRetrieveVirtualItem;
        event EventHandler<EventArgs> listViewDeviceSelectIndexChange;
        event EventHandler<DrawListViewSubItemEventArgs> listViewDeviceUpdateColorForItemPutInTag;
        event EventHandler<EventArgs> ResetStatistic;
        event EventHandler<EventArgs> contextMenuStripListViewItems_ItemClick;
        event EventHandler<MouseEventArgs> ListViewMouseDoubleClick;
        event EventHandler<ColumnClickEventArgs> ListViewItemColumnClick;
        event EventHandler<EventArgs> ButtonSettingsClick;
        event EventHandler<EventArgs> UpdateSettingsRef;
        event EventHandler<ExtendedEventArgs> ListViewItemsContextMenuOpening;
        event EventHandler<EventArgs> ListViewItemsContextMenuDeleteGroup;
        event EventHandler<EventArgs> Serializable;
        event EventHandler<EventArgs> DeSerializable;

        //МЕТОДЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void RefreshListView(string listViewName);
        void ChangeButtonColor(System.Drawing.Color color, object sender);//Смена цвета кнопки старта опроса
        void UpdateStatistic(Device device);
        void UpdateSelectedDevice(List<Device> device);
        void ChangeButtonPic(object sender);
        void OnOffUpdateTimer(bool onOff);
        void InsertItemInListViewDevice(string[] device);
    }
}
