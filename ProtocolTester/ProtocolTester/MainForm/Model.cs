﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Reflection;
using System.IO.Ports;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections;
using System.Net.Sockets;
using System.Threading;
using System.Xml.Serialization;

namespace ProtocolTester
{
    [Serializable]
    public class Model
    {

        private List<Device> selectedDeviceInDeviceListView;

        [XmlIgnore]
        public List<Device> SelectedDeviceInDeviceListView
        {
            set { selectedDeviceInDeviceListView = value; }
        }

        [XmlIgnore]
        public BMCSettings _bMCSettings { get; set; }

        [XmlIgnore]
        public BMCRemedy bMCRemedy { get; set; }

        [XmlIgnore]
        public int ModBusFunction { get; set; }
        [XmlIgnore]
        public int ModbusStartAddress { get; set; }
        [XmlIgnore]
        public int ModbusCount { get; set; }
        [XmlIgnore]
        public DataItem.DataType ModBusDataType { get; set; }

        public event EventHandler<EventArgs> ChangeListViewItemsVirtualItemsCount;
        public event EventHandler<EventArgs> RefreshListViewEvent;
        public event EventHandler<EventArgs> UpdateStatistic;
        public event EventHandler<EventArgs> ChangeButtonColor;
        public event EventHandler<EventArgs> ChangeOpcButtonPic;
        public event EventHandler<EventArgs> UpdateDataForModBus;
        public event EventHandler<EventArgs> UpdateTimerState;

        [XmlIgnore]
        public Settings _Settings { get; set; }

        
        List<ConnectionLine485> connectionLine485List = new List<ConnectionLine485>();

        [XmlIgnore]
        public static List<string> LogList { get; set; }

        private static TcpListener listener;

        public Model()
        {
            LogList = new List<string>();
        }

        [XmlIgnore]
        public static TcpListener Listener
        {
            get { return listener; }
            set
            {
                listener = value;
            }
        }

        [XmlIgnore]
        List<object[]> tagList = new List<object[]>();


        public decimal ReadWriteTimeout { get; set; }

        public decimal UpdateInterval { get; set; }
        private bool autoModeIndicator;

        [XmlIgnore]
        public bool AutoModeIndicator
        {
            get { return autoModeIndicator; }
            set { autoModeIndicator = value; }
        }

        [XmlArray("LineCollection"), XmlArrayItem("Line")]
        public List<ConnectionLine485> _ConnectionLine485List
        {
            get { return connectionLine485List; }
            set { connectionLine485List = value; }
        }

        public enum Mode
        {
            ModBusRTU,
            Carel,
            ModBusASCII,
            ModBusTCP
        }

       
        public void ListViewItemColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (((ListView)sender).Columns[e.Column].Text == "OPC")
            {
                var res = MessageBox.Show(((ListView)sender), "Вы действительно хотите установить/сбросить все значения?", "Atention", MessageBoxButtons.YesNo);                
                if (res == DialogResult.Yes)
                {
                    var temp = connectionLine485List.SelectMany((v1) => v1._Device.SelectMany((v2) => v2.DataItems.Select((v3) => v3))).ToArray();

                    bool state = false;

                    if (temp.Length > 0)
                        state = temp[0].OpcTag ? false : true;

                    foreach (DataItem item in temp)
                    {
                        item.OpcTag = state;
                    }

                }

              
            }
        }

        public void DoubleClickListViewEvent(object sender, MouseEventArgs e)
        {
            string selColumnName = "";//имя выбранного столбца
            int columnIndex = 0;
            int selItemIndex = 0;

            int x = 0;//координата начала столбца от левого края
            int x2 = 0;//ширина столбца
            int y = 0;//координата верхнего левого края выбранной ячейки
            //int y2 = 14;//высота строки
            int selSubitem = 0;

            int[] lWith = new int[((ListView)sender).Columns.Count];
            y = e.Y - 10 + ((ListView)sender).Location.Y;

            int allWith = 0;
            for (int i = 0; i < lWith.Length; i++)
            {
                for (int j = 0; j < ((ListView)sender).Columns.Count; j++)
                {
                    if (((ListView)sender).Columns[j].DisplayIndex == i)
                    {
                        selColumnName = ((ListView)sender).Columns[j].Text;
                        columnIndex = j;
                        selItemIndex = ((ListView)sender).SelectedIndices[0];
                        lWith[i] = ((ListView)sender).Columns[j].Width;
                        allWith = allWith + lWith[i];
                    }
                    if (e.X < allWith)
                    {
                        x = allWith - lWith[i];
                        x2 = lWith[i];
                        selSubitem = j;
                        break;
                    }
                }

                if (e.X < allWith)
                {
                    break;
                }
            }

            for (int i = 0; i < ((ListView)sender).SelectedIndices.Count; i++ )
            {
                Device dev = GetDataItemFromIndex(((ListView)sender).SelectedIndices[i]);
                
                if (dev != null)
                {
                    if (selColumnName == "OPC")
                        dev.DataItems[dev.SelectIndex].OpcTag = dev.DataItems[dev.SelectIndex].OpcTag ? false : true;
                        //item.OpcTag = item.OpcTag ? false : true;
                }
            }
        }
        
        public void UpdateDevices(string[] deviceParam)
        {
            bool flag = true;
            foreach (ConnectionLine485 line in _ConnectionLine485List)
            {
                if (line.Port.PortName == deviceParam[0])
                {
                    switch (deviceParam[6])
                    {
                        case "ModBusRTU":
                            line._Device.Add(new ModBusRTUUnit(line.Port, line.Port.PortName, int.Parse(deviceParam[1])));
                            line.Mode = Mode.ModBusRTU;
                            break;
                        case "Carel":
                            line._Device.Add(new CarelUnit(line.Port, deviceParam[0], int.Parse(deviceParam[1]) + 48));
                            line.Mode = Mode.Carel;
                            break;
                    }
                    flag = false;
                }
            }

            if (flag)
            {
                Mode m = Mode.ModBusRTU;
                Device device = new Device();
                switch (deviceParam[6])
                {
                    case "ModBusRTU":
                        break;
                    case "Carel":
                        m = Mode.Carel;
                        break;
                }

                _ConnectionLine485List.Add(new ConnectionLine485(deviceParam[0], int.Parse(deviceParam[5]), deviceParam[3].GetParity(), int.Parse(deviceParam[2]), deviceParam[4].GetStopBits(), int.Parse(deviceParam[1]), m));

            }

            UpdateListViewItemVurtualItemsCount();
        }

        public void UpdateListViewItemVurtualItemsCount()
        {
            int virtualItemsCount = 0;

            foreach (ConnectionLine485 line in connectionLine485List)
            {
                foreach (var dev in line._Device)
                {
                    virtualItemsCount = virtualItemsCount + dev.DataItems.Count;
                }
            }

            if (ChangeListViewItemsVirtualItemsCount != null)
                ChangeListViewItemsVirtualItemsCount(virtualItemsCount, EventArgs.Empty);
        }

        public void ResetStatistic()
        {
            foreach (var line in connectionLine485List)
            {
                foreach (var dev in line._Device)
                {
                    dev.ResetStatistic();
                }
            }

            ConnectionLine485.ResetStatistic();

        }

        public void ViewButtonsClick(object sender, EventArgs e)
        {
            if (((Button)sender).Name == "buttonStartScan")
            {

                if (autoModeIndicator == false)
                {
                    autoModeIndicator = true;
                    if (ChangeButtonColor != null)
                        ChangeButtonColor("buttonStartScan", e);

                    if (UpdateTimerState != null)
                        UpdateTimerState(true, EventArgs.Empty);

                   // foreach (ConnectionLine485 line in connectionLine485List)
                    Parallel.ForEach(connectionLine485List, (line) =>
                    {
                        //  lock (line)

                        switch (line.Mode)
                        {
                            case Mode.Carel:
                                Task carelTask = Task.Factory.StartNew(() =>
                                    {
                                            //lock (line)
                                            //   Parallel.ForEach(line._Device, (dev) =>
                                            {
                                            foreach (CarelUnit dev in line._Device)
                                            {
                                                    //   lock (dev)
                                                    {
                                                    dev.Timer.Start();
                                                    Carel.GetAllCarelValues((CarelUnit)dev, (int)ReadWriteTimeout, RefreshListViewItems, UpdateStatisticMethod);


                                                    if (autoModeIndicator == false)
                                                    {
                                                        if (line.Port.IsOpen)
                                                            line.Port.Close();
                                                            // break;
                                                        }
                                                }
                                            }
                                        }
                                            //);

                                            if (!line.Port.IsOpen)
                                            autoModeIndicator = false;


                                        while (true)
                                        {
                                            if (autoModeIndicator == false)
                                            {
                                                if (line.Port.IsOpen)
                                                    line.Port.Close();
                                                break;
                                            }
                                            try
                                            {
                                                //Parallel.ForEach(line._Device, (dev) =>
                                                foreach (CarelUnit dev in line._Device)
                                                {
                                                    if (!dev.Timer.IsRunning)
                                                    {
                                                        dev.Timer.Start();
                                                        var res = Carel.GetNewCarelValues((CarelUnit)dev, (int)ReadWriteTimeout, line._Device, _bMCSettings);
                                                        UpdateStatisticMethod(res);
                                                    }
                                                    else
                                                    {
                                                        if (dev.Timer.ElapsedMilliseconds > 30000)
                                                        {
                                                            dev.Timer.Restart();
                                                        }
                                                    }

                                                    if (autoModeIndicator == false)
                                                    {
                                                        break;
                                                    }
                                                }
                                                Thread.Sleep((int)UpdateInterval);
                                            }
                                            catch (Exception ex)
                                            {
                                                if (ex.Message == "Коллекция была изменена; невозможно выполнить операцию перечисления.")
                                                    Carel.GetAllCarelValues((CarelUnit)line._Device[line._Device.Count - 1], (int)ReadWriteTimeout, RefreshListViewItems, UpdateStatisticMethod);
                                            }
                                        }

                                        foreach (CarelUnit dev in line._Device)
                                        {
                                            dev.Quality = false;
                                            for (int i = 0; i < dev.DataItems.Count; i++)
                                            {
                                                dev.DataItems[i].Quality = "Bad";
                                                dev.DataItems[i].NeedSend = true;
                                            }
                                        }

                                        if (ChangeButtonColor != null)
                                            ChangeButtonColor("buttonStartScan", e);

                                    });

                                break;
                            case Mode.ModBusRTU:
                                Task RTUTask = Task.Factory.StartNew(() =>
                                {
                                    while (true)
                                    {
                                        if (autoModeIndicator == false)
                                        {
                                            if (line.Port.IsOpen)
                                                line.Port.Close();
                                            break;
                                        }

                                        try
                                        {
                                            foreach (ModBusRTUUnit dev in line._Device)
                                            {
                                                if (dev.DataItems.Count > 0)
                                                {
                                                    dev.Timer.Start();
                                                    ModBusRTU.UpdateValues(dev, (int)ReadWriteTimeout);
                                                    UpdateStatisticMethod(dev);
                                                }

                                                if (autoModeIndicator == false)
                                                {
                                                    break;
                                                }
                                            }
                                            Thread.Sleep((int)UpdateInterval);
                                        }
                                        catch(Exception ex)
                                        {
                                            ErrorEventLog.LogError("ViewButtonsClick.2 - " + ex.Message, Model.GetProgrammDir() + "\\Logs\\ModBusTRU.txt");
                                            autoModeIndicator = false;
                                        }
                                    }

                                    foreach (ModBusRTUUnit dev in line._Device)
                                    {
                                        dev.Quality = false;
                                        for (int i = 0; i < dev.DataItems.Count; i++)
                                        {
                                            dev.DataItems[i].Quality = "Bad";
                                            dev.DataItems[i].NeedSend = true;
                                        }
                                    }

                                    autoModeIndicator = false;
                                    if (ChangeButtonColor != null)
                                        ChangeButtonColor("buttonStartScan", e);
                                });
                                break;
                        }
                    
                    });
                }
                else
                {
                    autoModeIndicator = false;
                    if (UpdateTimerState != null)
                        UpdateTimerState(false, EventArgs.Empty);
                }
            }
            else
            {
                if (((Button)sender).Name == "buttonAdd")
                {
                    if (selectedDeviceInDeviceListView != null)
                    {
                        var devices = selectedDeviceInDeviceListView;

                        foreach (Device device in devices)
                        {
                            int indexGroup = MaxIndex(device.DataItems);
                            indexGroup++;
                            if (UpdateDataForModBus != null)
                            {
                                UpdateDataForModBus(sender, e);

                                if (ModBusFunction != 0 & ModbusCount != 0)
                                {
                                    int startAddress = ModbusStartAddress;
                                    for (int i = 0; i < ModbusCount; i++)
                                    {
                                        DataItem di = new DataItem(device.SerialPort.PortName, startAddress, ModBusDataType, ModBusFunction);
                                        di.Group = indexGroup;
                                        startAddress++;
                                        device.DataItems.Add(di);
                                    }
                                }
                            }

                            UpdateListViewItemVurtualItemsCount();
                        }
                    }
                }
            }
        }

        private int MaxIndex(List<DataItem> items)
        {
            int res = 0;

            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Group > res)
                    res = items[i].Group;
            }

            return res;
        }

        public static string GetProgrammDir()
        {
            string res = Assembly.GetExecutingAssembly().Location;

            for (int i = res.Length - 1; i > 0; i--)
            {
                if (res[i] != '\\')
                {
                    res = res.Substring(0, i);
                }
                else
                {
                    res = res.Substring(0, i);
                    break;
                }
            }

            return res;
        }

        public void RunOPC()
        {
            List<string[]> varFileLines = new List<string[]> { };
            List<string[]> varFileUnits = new List<string[]> { };
            List<object[]> varFileItems = new List<object[]>();

            List<string[]> varFilesVal = new List<string[]>();

            tagList = new List<object[]>();

            if (_Settings != null)
            {
                if (_Settings.SettingsPath != null)
                    if (_Settings.SettingsPath != "")
                    {
                        if (File.Exists(_Settings.SettingsPath + "\\DRIVER.INI"))
                        {
                            string[] lines = File.ReadAllLines(_Settings.SettingsPath + "\\DRIVER.INI");

                            foreach (string s in lines)
                            {
                                if (s.Length > 5)
                                {
                                    if (s.Substring(0, 4) == "Line")
                                    {
                                        string[] res = new string[2];
                                        res[0] = s.Substring(4, s.Length - 4).Split('=')[0];
                                        res[1] = s.Substring(4, s.Length - 4).Split('=')[1].Split(',')[0];

                                        varFileLines.Add(res);
                                    }
                                }
                            }

                            if (File.Exists(_Settings.SettingsPath + "\\nd_0.ncf"))
                            {
                                lines = File.ReadAllLines(_Settings.SettingsPath + "\\nd_0.ncf");

                                bool flag = false;
                                foreach (string s in lines)
                                {
                                    if (flag)
                                    {
                                        //s.Replace("\"", "");
                                        if (s.Length > 1)
                                            varFileUnits.Add(s.Replace("\"", "").Split(','));
                                    }
                                    else
                                    {
                                        if (s == "[Units]")
                                            flag = true;
                                    }
                                }

                                foreach (string[] sArr in varFileUnits)
                                {
                                    if (sArr.Length == 7)
                                        if (File.Exists(_Settings.SettingsPath + "\\" + sArr[2] + ".var"))
                                        {
                                            lines = File.ReadAllLines(_Settings.SettingsPath + "\\" + sArr[2] + ".var");

                                            flag = false;
                                            foreach (string s in lines)
                                            {
                                                if (flag)
                                                {
                                                    if (s == "")
                                                        break;

                                                    varFileItems.Add(new object[] { sArr[2] + ".var", s.Replace("\"", "").Split(';') });
                                                }
                                                else
                                                {
                                                    if (s == "[VarList]")
                                                        flag = true;
                                                }
                                            }
                                        }
                                }
                            }
                        }
                    }
            }

            var devicesOnPort = connectionLine485List.SelectMany((v1) => v1._Device.Where((v3) =>
            {
                foreach (string[] s in varFileLines)
                {
                    if (v3.SerialPort.PortName == "COM" + s[1])
                        return true;
                }
                return false;
            }).Select((v2) => v2)).ToArray();

            var devicesInVar = devicesOnPort.Where((v) =>
            {
                foreach (string[] dev in varFileUnits)
                {
                    string com = "";
                    foreach (string[] line in varFileLines)
                        if (line[0] == dev[0])
                            com = "COM" + line[1];
                    if (com != "")
                        if (dev[1] == (v is CarelUnit ? v.Address - 48 : v.Address).ToString() & v.SerialPort.PortName == com)
                        {
                            ((Device)v).OpcName = dev[2];//присвоение имени устройству
                            return true;
                        }
                }
                return false;
            }).ToArray();

            int index = 1;

            var tags = devicesInVar.SelectMany((v) => v.DataItems.Where((v2) =>
            {
                foreach (var items in varFileItems)
                {
                    if (v.OpcName == ((string)items[0]).Substring(0, ((string)((object[])items)[0]).Length - 4))
                    {
                        if ((v2.DataType_ == DataItem.DataType.INT ? v is ModBusRTUUnit ? v2.Variable - 208 : v2.Variable : v2.Variable).ToString() == ((string[])((object[])items)[1])[2])
                        {
                            int valType = v2.DataType_ == DataItem.DataType.REAL ? 1 : v2.DataType_ == DataItem.DataType.INT ? 2 : 3;
                            int varType = 0;

                            switch (((string[])((object[])items)[1])[1])
                            {
                                case "1":
                                    varType = 3;
                                    break;
                                case "2":
                                    varType = 1;
                                    break;
                                case "3":
                                    varType = 2;
                                    break;
                                case "4":
                                    varType = 3;
                                    break;
                                case "5":
                                    varType = 2;
                                    break;
                            }



                            if (valType == varType)
                            {
                                v2.OpcTagName = ((string[])((object[])items)[1])[0];
                                v2.OPCAccess = ((string[])((object[])items)[1])[3] == "2" | ((string[])((object[])items)[1])[3] == "3" ? true : false;
                                v2.OpcTag = true;
                                v2.NeedSend = true;

                                tagList.Add(new object[] {"TAG " + index.ToString() + " " + v.OpcName + "\\" + v2.OpcTagName + " " + (v2.DataType_ == DataItem.DataType.BOOL ? "b" : v2.DataType_ == DataItem.DataType.INT ? "i4" : "r8") + " " + (v2.OPCAccess ? "rw" : "r"),
                                                                      v2,
                                                                      v
                                                                      });
                                index++;

                                return true;
                            }
                        }
                    }

                }
                return false;
            })).ToArray();



            if (tagList.Count > 0)
            {
                OpcExchange opc = new OpcExchange(tagList);
                opc.ChangeOpcButtonPic += new EventHandler<EventArgs>(opc_ChangeOpcButtonPic);
                Task runOPC = Task.Factory.StartNew(() =>
                {
                    opc.StartExchangeSSL(GetProgrammDir() + "\\server.pfx");
                });
            }
        }

        void opc_ChangeOpcButtonPic(object sender, EventArgs e)
        {
            if (ChangeOpcButtonPic != null)
                ChangeOpcButtonPic(sender, e);
        }

        public void UpdateStatisticMethod(Device device)
        {
            if (UpdateStatistic != null)
                UpdateStatistic(device, EventArgs.Empty);
        }

        public Device GetDeviceFromListViewItem(ListViewItem lvi)
        {
            for (int i = 0; i < connectionLine485List.Count; i++)
            {

                if (connectionLine485List[i].Port.PortName == lvi.SubItems[0].Text)
                {
                    for (int j = 0; j < connectionLine485List[i]._Device.Count; j++)
                    {
                        if (connectionLine485List[i]._Device[j].Address.ToString() == (lvi.SubItems[6].Text == "Carel" ? (int.Parse(lvi.SubItems[1].Text) + 48).ToString() : lvi.SubItems[1].Text))
                        {
                            return connectionLine485List[i]._Device[j];
                        }
                    }
                }
            }

            return null;
        }

        public List<Device> GetDeviceFromListViewItems(ListView.SelectedListViewItemCollection lvis)
        {
            List<Device> devs = new List<Device>();
            for (int i = 0; i < connectionLine485List.Count; i++)
            {
                for (int i2 = 0; i2 < lvis.Count; i2++)
                {
                    if (connectionLine485List[i].Port.PortName == lvis[i2].SubItems[0].Text)
                    {
                        for (int j = 0; j < connectionLine485List[i]._Device.Count; j++)
                        {
                            if (connectionLine485List[i]._Device[j].Address.ToString() == (lvis[i2].SubItems[6].Text == "Carel" ? (int.Parse(lvis[i2].SubItems[1].Text) + 48).ToString() : lvis[i2].SubItems[1].Text))
                            {
                                devs.Add(connectionLine485List[i]._Device[j]);
                            }
                        }
                    }
                }
            }

            return devs.Count > 0 ? devs : null;
        }

        public void RefreshListViewItems()
        {
            if (RefreshListViewEvent != null)
                RefreshListViewEvent(null, EventArgs.Empty);
        }

        public void ListViewRetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            DataItem di;
            int decrement = 0, decrement2 = 0;

            try
            {
                bool breakFlag = false;
                foreach (ConnectionLine485 line in connectionLine485List)
                {
                    foreach (var dev in line._Device)
                    {
                        decrement = decrement + dev.DataItems.Count;

                        if (decrement > e.ItemIndex)
                        {
                            di = dev.DataItems[e.ItemIndex - decrement2];
                            string t = di.DataType_.ToString();
                            ListViewItem lvi = new ListViewItem(new string[] { di.Com, line.Mode == Mode.Carel ? (dev.Address - 48).ToString() : dev.Address.ToString(), di.Variable.ToString(), di.Group.ToString(), di.Function.ToString(), di.Value, di.TimeStamp, di.OldValue, di.OldTimeStamp, (dev is CarelUnit ? dev.Quality ? "Good" : "Bad" : di.Quality), di.DataType_.ToString(), di.OpcTag.ToString(), di.OpcTagName, di.OPCAccess.ToString() });

                            e.Item = lvi;
                            breakFlag = true;
                            break;
                        }
                        else
                        {
                            decrement2 = decrement2 + dev.DataItems.Count;
                        }
                    }

                    if (breakFlag)
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError("ListViewRetrieveVirtualItem.1 - " + ex.Message, Model.GetProgrammDir() + "\\Logs\\ListViewItems.txt");
            }
        }

        public Device GetDataItemFromIndex(int index)
        {
            Device d = null;
            int decrement = 0, decrement2 = 0;

            try
            {
                bool breakFlag = false;
                foreach (ConnectionLine485 line in connectionLine485List)
                {
                    foreach (var dev in line._Device)
                    {

                        decrement = decrement + dev.DataItems.Count;

                        if (decrement > index)
                        {
                            dev.SelectIndex = index - decrement2;

                            d = dev;
                            breakFlag = true;
                            break;
                        }
                        else
                        {
                            decrement2 = decrement2 + dev.DataItems.Count;
                        }
                    }

                    if (breakFlag)
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError("GetDataItemFromIndex.1 - " + ex.Message, Model.GetProgrammDir() + "\\Logs\\ListViewItems.txt");
            }

            return d;
        }

        public void DeleteDeviceFromLine(ListViewItem lvi)
        {
            for (int i = 0; i < connectionLine485List.Count; i++)
            {
                if (connectionLine485List[i].Port.PortName == lvi.SubItems[0].Text)
                {
                    for (int j = 0; j < connectionLine485List[i]._Device.Count; j++)
                    {
                        if (connectionLine485List[i]._Device[j].Address.ToString() == (lvi.SubItems[6].Text == "Carel" ? (int.Parse(lvi.SubItems[1].Text) +48).ToString() : lvi.SubItems[1].Text))
                        {
                            connectionLine485List[i]._Device.RemoveAt(j);
                            UpdateListViewItemVurtualItemsCount();

                            if (connectionLine485List[i]._Device.Count == 0)
                            {
                                if (connectionLine485List[i].Port.IsOpen)
                                    connectionLine485List[i].Port.Close();

                                connectionLine485List.RemoveAt(i);
                                break;
                            }
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Получение версии программы
        /// </summary>
        /// <returns>Версия программы</returns>
        public string GetProgramVersion()
        {
            Assembly assem = Assembly.GetEntryAssembly();
            AssemblyName assemName = assem.GetName();
            Version ver = assemName.Version;
            return ver.ToString();
        }

        /// <summary>
        /// Struct representing a point.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public static implicit operator Point(POINT point)
            {
                return new Point(point.X, point.Y);
            }
        }

        /// <summary>
        /// Retrieves the cursor's position, in screen coordinates.
        /// </summary>
        /// <see>See MSDN documentation for further information.</see>
        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(out POINT lpPoint);

        public static Point GetCursorPosition()
        {
            POINT lpPoint;
            GetCursorPos(out lpPoint);
            //bool success = User32.GetCursorPos(out lpPoint);
            // if (!success)

            return lpPoint;
        }
    }

    
}
